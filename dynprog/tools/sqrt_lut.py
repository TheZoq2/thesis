import math

for x, y in [(x, round(math.sqrt(x/4096.) * 4096.)) for x in range(0, 4095)]:
    print(f"'h{x:x}: y <= 'h{y:x};")
