#!/bin/python3

import sys

def main():
    if len(sys.argv) != 4:
        print("Usage: populate_graph <graph file> <value file> <output file>")

    graph_file = sys.argv[1]
    value_file = sys.argv[2]
    result_file = sys.argv[3]
    graph = None
    with open(graph_file) as file:
        graph = "\n".join(file.readlines())

    with open(value_file) as file:
        for line in file.readlines():
            split = line.split(": ")
            pattern = "$" + split[0] + "$"
            value = split[1].strip()
            graph = graph.replace(pattern, value)

    with open(result_file, 'w') as file:
        file.write(graph)


main()


