`include "hdl/delay.v"

module before_pipeline(
    clk,
    rst,
    input_valid,
    input_axis0,
    input_axis1,
    input_axis2,
    done_delayed
);
    input clk;
    input rst;
    input input_valid;
    output[`INPUT_SIZE_1 - 1:0] input_axis0;
    output[`INPUT_SIZE_2 - 1:0] input_axis1;
    output reg[`INPUT_SIZE_3 - 1:0] input_axis2;
    output done_delayed;

    wire[`INPUT_LUT_SIZE_1 - 1:0] input_axis_addr0;
    wire[`INPUT_LUT_SIZE_2 - 1:0] input_axis_addr1;
    wire[`INPUT_LUT_SIZE_3 - 1:0] input_axis_addr2;

    wire done;

    ///////////////////////////////////////////////////////////////////////////
    //                      Stage 1
    ///////////////////////////////////////////////////////////////////////////
    CascadedIterator cs
        ( .clk(clk)
        , .rst(rst)
        , .ax0(input_axis_addr0)
        , .ax1(input_axis_addr1)
        , .ax2(input_axis_addr2)
        , .done(done)
        , .count_enable(input_valid)
        );

    `DELAY_STAGE(input_valid, 2, 1)

    ///////////////////////////////////////////////////////////////////////////
    //                      Stage 2
    ///////////////////////////////////////////////////////////////////////////

    input1 i1_lut(.clk(clk), .addr(input_axis_addr0), .result(input_axis0));
    input2 i2_lut(.clk(clk), .addr(input_axis_addr1), .result(input_axis1));

    // One extra depth before model starts for LUTs
    localparam DONE_DELAY_TIME = `sample_model_DEPTH + 1;
    reg[DONE_DELAY_TIME - 1:0] done_delay_fifo;
    always @(posedge clk) begin
        if (rst == 1)
            done_delay_fifo = 0;
        else
            done_delay_fifo = {done_delay_fifo[DONE_DELAY_TIME - 2:0], done};
    end
    wire done_delayed = done_delay_fifo[DONE_DELAY_TIME - 1];

    // Since the second input does not go through a LUT, it must be delayed
    // manually
    always @(posedge clk) begin
        input_axis2 <= input_axis_addr2 << 12;
    end
endmodule
