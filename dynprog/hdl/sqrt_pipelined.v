module sqrt_pipelined#(parameter input_size = 32, parameter output_size = 32)(clk, x, y);
    input clk;
    input[input_size-1:0] x;
    output reg[output_size-1:0] y;

    localparam fractional_bits = 12;

    reg[5:0] prienc_out;
    wire[fractional_bits:0] lut_result;
    wire[31:0] log;

    integer i;
    always @(posedge clk) begin
        prienc_out = 0;
        for(i = 0; i < input_size; i=i+1) begin
            if (x[i]) prienc_out = i-fractional_bits;
        end
    end

    // Amount to shift in order to get x / pow(2, 2*n)
    wire[6:0] amount_to_shift = prienc_out * 2;

    reg[fractional_bits:0] shifted;
    always @(posedge clk) begin
        if ($signed(amount_to_shift) > 0)
            shifted <= x >> amount_to_shift;
        else
            shifted <= x;
    end

    sqrt_lut_pipelined lut(.clk(clk), .x(shifted), .y(lut_result));

    always @(posedge clk) begin
        y <= ($signed(prienc_out) > 0) ? lut_result << prienc_out : lut_result;
    end
endmodule


