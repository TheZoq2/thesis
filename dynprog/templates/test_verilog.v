module model_test();
    `SETUP_TEST
    reg clk;
    reg rst;

    initial begin
        $dumpfile(`VCD_OUTPUT);
        $dumpvars(0, model_test);
        clk = 0;
        input_valid = 0;
        #1;
        forever begin
            #1 clk = ~clk;
        end
    end

    reg input_valid;
    reg[31:0] x1;
    reg[31:0] x2;
    reg[31:0] u1;
    reg[31:0] u2;
    reg[31:0] u3;
    reg[31:0] dt;

    wire[31:0] stage_cost;
    wire[31:0] x1next;
    wire[31:0] x2next;
    wire[31:0] fuel;
    wire[31:0] time_;
    wire[31:0] teng;
    wire[31:0] fbrk;
    wire valid;
    wire failed;

    integer f;

    initial begin
        #20
        input_valid <= 1;

        {{code}}

        `END_TEST
    end

    pt_model uut(
        .clk(clk),
        .rst(rst),
        .input_valid(input_valid),
        .x1(x1),
        .x2(x2),
        .u1(u1),
        .u2(u2),
        .u3(u3),
        .dt(dt),

        .stage_cost(stage_cost),
        .x1next(x1next),
        .x2next(x2next),
        .fuel(fuel),
        .time_(time_),
        .teng(teng),
        .fbrk(fbrk),
        .failed(failed),
        .output_valid(valid)
    );
endmodule
