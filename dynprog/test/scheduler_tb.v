module scheduler_tb();
    `SETUP_TEST
    reg clk;
    reg rst;

    wire[`COST_SIZE-1:0] cost;
    wire[`STATE_SIZE-1:0] cost_read_address;
    reg[`STATE_SIZE_0-1:0] goal_state_0 = 0;
    reg[`STATE_SIZE_1-1:0] goal_state_1 = 0;

    initial begin
        $dumpfile(`VCD_OUTPUT);
        $dumpvars(0, scheduler_tb);
        clk = 0;
        #1;
        forever begin
            #1 clk = ~clk;
        end
    end

    wire[`LAYER_STATE_SIZE - 1:0] goal_address = {goal_state_1, goal_state_1};
    initial begin
        rst = 1;
        @(negedge clk)
        rst = 0;
        `END_TEST
        @(posedge scheduler.setup_done);
        `ASSERT_EQ(scheduler.ram.memory_array[1], 'hffff);
        `ASSERT_EQ(scheduler.ram.memory_array[goal_address], 0);
        @(negedge clk)
        // Wait for the calculations of oner layer to finnish
        @(posedge scheduler.cascading_done)
        // At time one and state 0,0 the expected cost is 1
        `ASSERT_EQ(scheduler.ram.memory_array['h100], 1)
        // Moving in either input should increase the cost by 1
        `ASSERT_EQ(scheduler.ram.memory_array['h101], 2)
        `ASSERT_EQ(scheduler.ram.memory_array['h110], 2)
        // Moving diagonally should increase the cost by 2
        `ASSERT_EQ(scheduler.ram.memory_array['h111], 3)
        `ASSERT_EQ(scheduler.ram.memory_array['h1aa], 'hffff)
        `END_TEST
    end

    Scheduler scheduler (
        .clk(clk),
        .rst(rst),
        .goal_state_0(goal_state_0),
        .goal_state_1(goal_state_1)
    );
endmodule
