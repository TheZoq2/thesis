#include "../src/node.hpp"
#include "../src/codegen.hpp"

#include <iostream>



Rc<Node> sample_fn(Rc<Input<0,100>> a, Rc<Input<0,50>> b) {
    return a + b + b + b;
}

int main() {
    auto result = sample_fn(
        Input<0, 100>::create("a", 0),
        Input<0, 50>::create("b", 0)
    );

#ifndef RUN_CODE
    Codegen codegen({std::make_pair("sum", result)});
    std::cout << join(codegen.generate_module("adder", FixedFixpoint{12}));
#endif
}

