#include <catch2/catch.hpp>

#include "../src/node.hpp"
#include "../src/codegen.hpp"
#include "../src/lookup_table.hpp"
#include "../src/interpolation.hpp"

TEST_CASE("Partial constant tests", "[node]") {
    REQUIRE(Constant::create("", 0)->unique_ancestors() == std::set<Node*>{});
}

TEST_CASE("Multiplication generates correct fixpoint op") {
    auto a = Variable::create("a", 0, 100, 50);
    auto b = Variable::create("b", 0, 100, 50);

    auto result = a * b;

    std::vector<std::string> expected = {
        "reg[26:0] mul_a_b;",
        "wire[39:0] mul_a_b_buf;",
        "assign mul_a_b_buf = $signed(a)*$signed(b);",
        "always @(posedge clk) begin",
        "\tmul_a_b <= mul_a_b_buf[38:12];",
        "end"
    };

    REQUIRE(join(result->get_code(FixedFixpoint{12})) == join(expected));
}
TEST_CASE("Division generates correct fixpoint op") {
    auto a = Variable::create("a", 0, 100, 50);
    auto b = Variable::create("b", 0.25, 100, 50);

    auto result = a / b;

    std::vector<std::string> expected = {
        "reg[21:0] div_a_b;",
        "wire[31:0] div_a_b_buffer;",
        "assign div_a_b_buffer = $signed({a,12'b0})/$signed(b);",
        "always @(posedge clk) begin",
        "\tdiv_a_b <= div_a_b_buffer[21:0];",
        "end"
    };

    REQUIRE(join(result->get_code(FixedFixpoint{12})) == join(expected));
}

TEST_CASE( "Input (partially) works", "[codegen]") {
    auto a = Input<0, 100>::create("a", 0);

    a->set_value(100);

    REQUIRE(a->calculate_value() == ExecutionType(100));
    REQUIRE(a->unique_ancestors() == std::set<Node*>{});
}

TEST_CASE( "Abs works", "[codegen]" ) {
    std::vector<std::string> expected = {
        "reg[19:0] abs_var;",
        "always @(posedge clk) begin",
        "\tif($signed(var) < 0) begin",
        "\t\tabs_var <= -var;",
        "\tend",
        "\telse begin",
        "\t\tabs_var <= var;",
        "\tend",
        "end"
    };

    auto var = Variable::create("var", -100, 50, 0);
    auto result = Abs::create(var);

    assign_delay_slots({result}, 0);
    REQUIRE(join(result->get_code(FixedFixpoint{12})) == join(expected));
    REQUIRE(result->min() == 0);
    REQUIRE(result->max() == 100);
    REQUIRE(result->graph_label_name() == "abs");
    REQUIRE(result->unique_ancestors() == std::set{(Node*)var.ptr()});
}
TEST_CASE( "Sqrt works", "[codegen]" ) {
    auto a = Variable::create("a", 0, 100, 4);

    auto result = Sqrt::create(a);

    REQUIRE(result->min() == 0);
    REQUIRE(result->max() == 10);
    REQUIRE(result->calculate_value() == 2);
    REQUIRE(compare_rcs(result->get_parents(), {a}));
    REQUIRE(result->base_name() == "sqrt_a");
    REQUIRE(result->graph_label_name() == "sqrt");
    REQUIRE(result->unique_ancestors() == std::set{(Node*) a.ptr()});

    std::vector<std::string> expected_code = {
        "wire[16:0] sqrt_a;",
        "sqrt#(.input_size(20), .output_size(17)) sqrt_a_mod(clk, a, sqrt_a);"
    };
    REQUIRE(join(result->get_code(FixedFixpoint{12})) == join(expected_code));
}
TEST_CASE( "Truncate works", "[codegen]" ) {
    auto a = Variable::create("a", -50.3, 100.7, 5.5);
    auto result = Truncate::create(a);

    REQUIRE(result->min() == -50);
    REQUIRE(result->max() == 100);
    REQUIRE(compare_rcs(result->get_parents(), {a}));
    REQUIRE(result->graph_label_name() == "truncate");
    REQUIRE(result->calculate_value() == 5);
    REQUIRE(result->base_name() == "truncate_a");
    REQUIRE(result->unique_ancestors() == std::set{(Node*) a.ptr()});

    std::vector<std::string> expected = {
        "reg[17:0] truncate_a;",
        "always @(posedge clk) begin",
        "\ttruncate_a <= {a[17:10], 10'b0};",
        "end"
    };
    REQUIRE(join(result->get_code(FixedFixpoint{10})) == join(expected));
}



TEST_CASE( "Less than works", "[codegen]" ) {
    auto a = Variable::create("a", 0, 100, 25);
    auto b = Variable::create("b", 0, 100, 50);

    auto result = a < b;

    // Value
    REQUIRE(result->calculate_value() == true);
    // TODO: Codegen
    std::vector<std::string> expected = {
        "reg lt_a_b;",
        "always @(posedge clk) begin",
        "\tlt_a_b <= $signed(a)<$signed(b);",
        "end"
    };

    assign_delay_slots({result}, 0);
    REQUIRE(join(result->get_code(FixedFixpoint{12})) == join(expected));
    REQUIRE(result->graph_label_name() == "<");
    REQUIRE(result->unique_ancestors() == std::set{(Node*) a.ptr(), (Node*)b.ptr()});
}
TEST_CASE( "Less than works for inverted order", "[codegen]" ) {
    auto a = Variable::create("a", 0, 100, 25);
    auto b = Variable::create("b", 0, 100, 50);

    auto result = b < a;

    // Value
    REQUIRE(result->calculate_value() == false);
}
TEST_CASE( "Greater than works", "[codegen]" ) {
    auto a = Variable::create("a", 0, 100, 25);
    auto b = Variable::create("b", 0, 100, 50);

    auto result = a > b;

    // Value
    REQUIRE(result->calculate_value() == false);
    // TODO: Codegen
    std::vector<std::string> expected = {
        "reg gt_a_b;",
        "always @(posedge clk) begin",
        "\tgt_a_b <= $signed(a)>$signed(b);",
        "end"
    };

    assign_delay_slots({result}, 0);
    REQUIRE(join(result->get_code(FixedFixpoint{12})) == join(expected));
    REQUIRE(result->graph_label_name() == ">");

    REQUIRE(result->unique_ancestors() == std::set<Node*>{a.ptr(), b.ptr()});
}
TEST_CASE( "LEq works", "[codegen]" ) {
    auto a = Constant::create("a", 1);
    auto b = Constant::create("b", 2);
    auto c = Constant::create("c", 3);

    REQUIRE((a <= b)->calculate_value());
    REQUIRE((b <= b)->calculate_value());
    REQUIRE_FALSE((c <= b)->calculate_value());

    auto result = a <= b;
    REQUIRE(result->base_name() == "leq_a_b");

    std::vector<std::string> expected_code = {
        "reg leq_a_b;",
        "always @(posedge clk) begin",
        "\tleq_a_b <= $signed(a)<=$signed(b);",
        "end"
    };
    REQUIRE(result->get_code(FixedFixpoint{12}) == expected_code);
    REQUIRE(result->graph_label_name() == "<=");
    REQUIRE(result->unique_ancestors() == std::set<Node*>{a.ptr(), b.ptr()});
}
TEST_CASE( "GEq works", "[codegen]" ) {
    auto a = Constant::create("a", 1);
    auto b = Constant::create("b", 2);
    auto c = Constant::create("c", 3);

    REQUIRE_FALSE((a >= b)->calculate_value());
    REQUIRE((b >= b)->calculate_value());
    REQUIRE((c >= b)->calculate_value());

    auto result = a >= b;
    REQUIRE(result->base_name() == "geq_a_b");

    std::vector<std::string> expected_code = {
        "reg geq_a_b;",
        "always @(posedge clk) begin",
        "\tgeq_a_b <= $signed(a)>=$signed(b);",
        "end"
    };
    REQUIRE(result->get_code(FixedFixpoint{12}) == expected_code);
    REQUIRE(result->graph_label_name() == ">=");
    REQUIRE(result->unique_ancestors() == std::set<Node*>{a.ptr(), b.ptr()});
}

TEST_CASE( "Lookup tables work", "[luts]") {
    auto lut = LookupTable::create("test_lut", {1,2,3});

    auto index = Constant::create("index", 1);
    auto result = LutResult::create(lut, index);

    std::vector<std::string> expected_code = {
        "wire[14:0] lut_test_lut_index;",
        "test_lut#(.input_size(14), .output_size(15)) lut_test_lut_index_lut(.clk(clk), .addr(index), .result(lut_test_lut_index));"
    };

    std::vector<Rc<Node>> expected_children = {index};

    REQUIRE(result->base_name() == "lut_test_lut_index");
    REQUIRE(result->min() == 1);
    REQUIRE(result->max() == 3);
    REQUIRE(result->calculate_value() == 2);
    REQUIRE(compare_rcs(result->get_parents(), expected_children));
    REQUIRE(join(result->get_code(FixedFixpoint{12})) == join(expected_code));
    REQUIRE(result->graph_label_name() == "lut");
    REQUIRE(result->unique_ancestors() == std::set<Node*>{index.ptr()});
}
TEST_CASE( "Lookup tables with non-uniform distribution work", "[luts]") {
    auto lut = LookupTable::create("test_lut", {2, 4, 8}, {1,2,3});

    auto index = Constant::create("index", 2);
    auto result = LutResult::create(lut, index);

    REQUIRE(result->base_name() == "lut_test_lut_index");
    REQUIRE(result->min() == 1);
    REQUIRE(result->max() == 3);
    REQUIRE(result->calculate_value() == 1);
    REQUIRE(result->graph_label_name() == "lut");
}


TEST_CASE( "If-statements work", "[codegen]" ) {
    auto a = Variable::create("a", 25, 100, 60);
    auto b = Variable::create("b", 50, 110, 75);
    auto condition = a < b;
    auto result = _if(condition, a, b);

    std::vector<std::string> expected_code = {
        "reg[19:0] if_lt_a_b_then_a_else_b;",
        "always @(posedge clk) begin",
        "\tif(lt_a_b) begin",
        "\t\tif_lt_a_b_then_a_else_b <= a_1;",
        "\tend",
        "\telse begin",
        "\t\tif_lt_a_b_then_a_else_b <= b_1;",
        "\tend",
        "end"
    };

    REQUIRE(result->min() == 25);
    REQUIRE(result->max() == 110);


    // These assertions were added because something caused if-statements
    // to break when ExecutionType used more than 25 fractional bits
    REQUIRE(a->calculate_value() == ExecutionType(60));
    REQUIRE(b->calculate_value() == ExecutionType(75));
    REQUIRE(condition->calculate_value() == true);

    REQUIRE(result->calculate_value() == ExecutionType(60));

    REQUIRE(result->base_name() == "if_lt_a_b_then_a_else_b");
    REQUIRE(join(result->get_code(FixedFixpoint{12})) == join(expected_code));
    REQUIRE(compare_rcs(result->get_parents(), std::vector<Rc<Node>>{condition, a, b}));
    REQUIRE(result->graph_label_name() == "if");

    result->_override_min(123);
    result->_override_max(225);
    REQUIRE(result->min() == 123);
    REQUIRE(result->max() == 225);
    REQUIRE(result->unique_ancestors() == std::set<Node*>{a.ptr(), b.ptr(), condition.ptr()});
}
TEST_CASE( "If-statements work for false branches", "[codegen]" ) {
    auto a = Variable::create("a", 25, 100, 75);
    auto b = Variable::create("b", 50, 150, 60);
    auto condition = a < b;
    auto result = _if(condition, a, b);

    REQUIRE(result->calculate_value() == 60);
}
TEST_CASE( "If-statements perform sign extension when output sizes are different") {
    auto a = Constant::create("a", 1);
    auto b = Constant::create("b", 100);
    auto result = _if(a < b, a, b);

    std::vector<std::string> expected = {
        "reg[19:0] if_lt_a_b_then_a_else_b;",
        "always @(posedge clk) begin",
        "\tif(lt_a_b) begin",
        "\t\tif_lt_a_b_then_a_else_b <= {{6{a[13]}}, a};",
        "\tend",
        "\telse begin",
        "\t\tif_lt_a_b_then_a_else_b <= b;",
        "\tend",
        "end"
    };

    REQUIRE(join(expected) == join(result->get_code(FixedFixpoint{12})));
}

TEST_CASE( "Boolean constants work", "[codegen]" ) {
    auto var = BoolConst::True;

    std::vector<std::string> expected_code = {"localparam true = 1;"};

    REQUIRE(var->calculate_value() == true);
    REQUIRE(var->base_name() == "true");
    REQUIRE(var->pipeline_buffer_name(2) == "true");
    REQUIRE(var->generate_pipeline_code(FixedFixpoint{12}) == std::vector<std::string>{});
    REQUIRE(var->get_code(FixedFixpoint{12}) == expected_code);

    var = BoolConst::False;
    expected_code = {"localparam false = 0;"};

    REQUIRE(var->calculate_value() == false);
    REQUIRE(var->base_name() == "false");
    REQUIRE(var->pipeline_buffer_name(3) == "false");
    REQUIRE(var->get_code(FixedFixpoint{12}) == expected_code);
    REQUIRE(var->graph_label_name() == "boolconst false");
    REQUIRE(var->unique_ancestors() == std::set<Node*>{});
}

TEST_CASE( "Or works", "[codegen]" ) {
    auto result = BoolConst::True || BoolConst::False;
    auto false_result = BoolConst::False || BoolConst::False;

    // Value
    REQUIRE(result->calculate_value() == true);
    REQUIRE(false_result->calculate_value() == false);
    std::vector<std::string> expected = {
        "reg or_true_false;",
        "always @(posedge clk) begin",
        "\tor_true_false <= true|false;",
        "end"
    };

    REQUIRE(join(result->get_code(FixedFixpoint{12})) == join(expected));
    REQUIRE(result->graph_label_name() == "|");
    REQUIRE(result->unique_ancestors() == std::set<Node*>{
        BoolConst::True.ptr(),
        BoolConst::False.ptr()
    });
}

TEST_CASE( "Not works", "[codegen]") {
    auto t = BoolConst::True;
    auto f = BoolConst::False;
    auto result = !t;

    std::vector<std::string> expected_true = {
        "reg not_true;",
        "always @(posedge clk) begin",
        "\tnot_true <= !true;",
        "end"
    };
    REQUIRE(result->base_name() == "not_true");
    REQUIRE(result->get_code(FixedFixpoint{12}) == expected_true);
    REQUIRE(result->calculate_value() == false);
    REQUIRE(compare_rcs(result->get_parents(), std::vector<Rc<Node>>{t}));

    result = !f;
    std::vector<std::string> expected_false = {
        "reg not_false;",
        "always @(posedge clk) begin",
        "\tnot_false <= !false;",
        "end"
    };
    REQUIRE(result->get_code(FixedFixpoint{12}) == expected_false);
    REQUIRE(result->calculate_value() == true);
    REQUIRE(compare_rcs(result->get_parents(), std::vector<Rc<Node>>{f}));
    REQUIRE(result->graph_label_name() == "!");
    REQUIRE(result->unique_ancestors() == std::set<Node*>{f.ptr()});
}

TEST_CASE( "USub works", "[codegen]" ) {
    auto a = Variable::create("a", -50, 100, 10);
    auto result = -a;

    std::vector<std::string> expected = {
        "reg[19:0] usub_a;",
        "always @(posedge clk) begin",
        "\tusub_a <= -a;",
        "end"
    };

    REQUIRE(compare_rcs(result->get_parents(), {a}));
    REQUIRE(result->get_code(FixedFixpoint{12}) == expected);
    REQUIRE(result->min() == -100);
    REQUIRE(result->max() == 50);
    REQUIRE(result->calculate_value() == -10);
    REQUIRE(result->graph_label_name() == "-");

    a->set_value(-10);
    a->mark_dirty();
    result->mark_dirty();
    REQUIRE(result->calculate_value() == 10);
    REQUIRE(result->unique_ancestors() == std::set<Node*>{a.ptr()});
}
