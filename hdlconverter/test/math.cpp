#include <catch2/catch.hpp>


#include "../src/util.hpp"

#include <cmath>
#include "../MFixedPoint/include/MFixedPoint/FpF.hpp"

#include <iostream>

TEST_CASE( "Log approximation works for 1", "[log]") {
    ExecutionType input = 1;

    REQUIRE(approximate_log2(input) == 0);
}
TEST_CASE( "Log approximation works for 2", "[log]") {
    ExecutionType input = 2;
    REQUIRE(approximate_log2(input) == 1);
}
TEST_CASE( "Log approximation works for 3", "[log]") {
    ExecutionType input = 3;

    auto diff = approximate_log2(input).ToDouble() - log2(input.ToDouble());
    REQUIRE(fabs(diff) < (1./1000.));
}
TEST_CASE( "Log approximation works for 0.5", "[log]") {
    ExecutionType input = 0.5;

    auto diff = approximate_log2(input).ToDouble() - log2(input.ToDouble());
    REQUIRE(fabs(diff) < (1./1000.));
}

TEST_CASE( "Pow2 works for 0", "[pow2]") {
    ExecutionType input = 0;

    REQUIRE(approximate_pow2(input) == 1);
}
TEST_CASE( "Pow2 works for 1", "[pow2]") {
    ExecutionType input = 1;

    REQUIRE(approximate_pow2(input) == 2);
}
TEST_CASE( "Pow2 works for 1.5", "[pow2]") {
    ExecutionType input = 1.5;

    auto diff = approximate_pow2(input).ToDouble() - pow(2, input.ToDouble());
    REQUIRE(fabs(diff) < (1./1000.));
}
TEST_CASE( "Pow2 works for 0.5", "[pow2]") {
    ExecutionType input = 0.5;

    auto diff = approximate_pow2(input).ToDouble() - pow(2, input.ToDouble());
    REQUIRE(fabs(diff) < (1./1000.));
}


/*
  Apparently, the calculation of -3918.9923265993266 * 0.0133 results
  in 2.36 with fixipoint values which is waay off
*/
TEST_CASE( "Fixpoint multiplication of weirdness causing values does not fail") {
    mn::MFixedPoint::FpF<int64_t, int64_t, 20> a = -3918.9923265993266;
    mn::MFixedPoint::FpF<int64_t, int64_t, 20> b = -0.0133;

    REQUIRE(round((double) (a*b)) == 52);
}

TEST_CASE("Fixpoint numbers behave correctly when 64 bit ints overflow") {
    using Fp = mn::MFixedPoint::FpF<__int128_t, __int128_t, 25>;
    Fp a = 75;

    REQUIRE(a == 75);
}
