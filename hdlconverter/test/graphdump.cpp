#include <catch2/catch.hpp>

#include "../src/node.hpp"
#include "../src/graph.hpp"

TEST_CASE( "Graph generation works", "[graph]" ) {
    auto a = Input<0, 100>::create("a", 5);
    auto b = Variable::create("b", 0, 100, 10);
    auto c = Constant::create("c", 15);

    auto result = a + b + c;
    result->override_name("result");

    std::vector<std::string> expected = {
        "add_a_b [label=\"+\\n15.000000\"];",
        "add_a_b -> {a,b};",
        "b [label=\"var b\\n10.000000\"];",
        "b -> {};",
        "a [label=\"in a\\n5.000000\"];",
        "a -> {};",
        "result [label=\"result\\n30.000000\"];",
        "result -> {add_a_b,c};",
        "c [label=\"const c\\n15.000000\"];",
        "c -> {};",
    };

    auto graph = generate_graph(result, [](auto node){return node->display_value();});
    REQUIRE(graph.size() == expected.size());

    for(auto line : expected) {
        REQUIRE(find(graph.begin(), graph.end(), line) != graph.end());
    }
}


TEST_CASE( "Graph label names work", "[graph]") {
    auto a = Constant::create("a", 5);
    auto b = Variable::create("b", 0, 100, 5);
    auto in = Input<0, 100>::create("c", 5);
    REQUIRE(a->graph_label_name() == "const a");
    REQUIRE(b->graph_label_name() == "var b");
    REQUIRE(in->graph_label_name() == "in c");
}

#define BINOP_GRAPH_LABEL_TEST(op, expected) TEST_CASE( "Graph labels work for " expected, "[graph]") { \
    auto a = Constant::create("a", 5); \
    auto b = Constant::create("b", 5); \
    REQUIRE((a op b)->graph_label_name() == expected); \
}

BINOP_GRAPH_LABEL_TEST(+, "+");
BINOP_GRAPH_LABEL_TEST(-, "-");
BINOP_GRAPH_LABEL_TEST(*, "*");
BINOP_GRAPH_LABEL_TEST(/, "/");

