#include <catch2/catch.hpp>

#include "../src/node.hpp"

TEST_CASE( "Constants have correct bounds", "[nodes]") {
    auto a = Constant::create("a", 50);
    REQUIRE(a->min() == 50);
    REQUIRE(a->max() == 50);
}



TEST_CASE( "Variables have correct bounds", "[nodes]") {
    auto a = Variable::create("a", 0, 50, 0);
    REQUIRE(a->min() == 0);
    REQUIRE(a->max() == 50);
}

TEST_CASE( "Addition has correct bounds", "[nodes]") {
    auto a = Variable::create("a", 0, 50, 0);
    auto b = Variable::create("b", 25, 50, 25);

    auto result = a + b;

    REQUIRE(result->min() == 25);
    REQUIRE(result->max() == 100);
}
TEST_CASE( "Subtractions have correct bounds", "[nodes]") {
    auto a = Variable::create("a", 0, 50, 0);
    auto b = Variable::create("b", 25, 50, 25);

    auto result = a - b;

    REQUIRE(result->min() == -50);
    REQUIRE(result->max() == 25);
}
TEST_CASE( "Multiplications have correct bounds", "[nodes]") {
    auto a = Variable::create("a", 0, 50, 0);
    auto b = Variable::create("b", 25, 50, 25);

    auto result = a * b;

    REQUIRE(result->min() == 0);
    REQUIRE(result->max() == 50*50);
}

TEST_CASE( "Simple depth calculations work", "[nodes]") {
    auto a = Variable::create("a", 0, 50, 0);
    auto b = Variable::create("b", 25, 50, 25);
    auto result = a + b;

    REQUIRE(a->depth() == 0);
    REQUIRE(b->depth() == 0);
    REQUIRE(result->depth() == 1);
}

TEST_CASE( "Depth is correct for unbalanced trees", "[nodes]") {
    auto a = Variable::create("a", 0, 50, 0);
    auto b = Variable::create("b", 25, 50, 25);

    auto result_ = a + b;
    auto result = result_ + b;

    REQUIRE(result->depth() == 2);
}

TEST_CASE( "Required pipeline stages is correct", "[nodes]" ) {
    auto a = Variable::create("a", 0, 50, 0);
    auto b = Variable::create("b", 25, 50, 25);

    auto first_addition = a + b;
    auto second_addition = a + first_addition;

    assign_delay_slots({second_addition}, 2);

    REQUIRE(a->get_delay_slots() == 1);
    REQUIRE(b->get_delay_slots() == 0);
    REQUIRE(first_addition->get_delay_slots() == 0);
    REQUIRE(second_addition->get_delay_slots() == 0);
}

TEST_CASE( "Input bounds are correct", "[nodes]" ) {
    auto a = Input<5, 10>::create("a", 5);

    REQUIRE(a->min() == 5);
    REQUIRE(a->max() == 10);
}

TEST_CASE( "Divisions have correct bounds", "[nodes]" ) {
    auto a = Variable::create("a", 5, 10, 5);
    auto b = Variable::create("b", 5, 10, 5);

    auto result = a / b;
    REQUIRE(result->min() == 5 / 10.0);
    REQUIRE(result->max() == 10 / 5.0);
}
