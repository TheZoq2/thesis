#include <catch2/catch.hpp>

#include "../src/node.hpp"
#include "../src/codegen.hpp"
#include "../src/lookup_table.hpp"
#include "../src/interpolation.hpp"
#include "util.hpp"

TEST_CASE( "Constants generate code", "[nodes]" ) {
    auto a = Constant::create("a", 50);
    std::vector<std::string> expected = {"localparam[18:0] a = 204800;"};
    auto code = a->get_code(FixedFixpoint{12});

    REQUIRE(code == expected);
}

TEST_CASE( "Codegen for inputs works", "[nodes]" ) {
    auto a = Input<5, 10>::create("a", 5);

    std::vector<std::string> expected = {
        "input[16:0] a;"
    };
    assign_delay_slots({a}, 0);
    REQUIRE(a->get_code(FixedFixpoint{12}) == expected);
}

TEST_CASE( "Get code works for single layer additions", "[nodes]" ) {
    std::vector<std::string> expected = {
        "reg[19:0] add_a_b;",
        "always @(posedge clk) begin",
        "\tadd_a_b <= $signed(a)+$signed(b);",
        "end"
    };

    auto a = Variable::create("a", 25, 50, 25);
    auto b = Variable::create("b", 0, 50, 0);

    auto result = a + b;

    REQUIRE(result->get_code(FixedFixpoint{12}) == expected);
}
TEST_CASE( "Codegen works for single layer subtractions", "[nodes]" ) {
    std::vector<std::string> expected = {
        "reg[18:0] a;",
        "reg[18:0] b;",
        "reg[18:0] sub_a_b;",
        "always @(posedge clk) begin",
        "\tsub_a_b <= $signed(a)-$signed(b);",
        "end"
    };

    auto a = Variable::create("a", 25, 50, 25);
    auto b = Variable::create("b", 0, 50, 0);

    auto result = a - b;

    Codegen codegen({std::make_pair("out", result)});

    require_same_lines(codegen.generate_code(FixedFixpoint{12}), expected);
}
TEST_CASE( "Codegen works for single layer multiplications", "[nodes]" ) {
    std::vector<std::string> expected = {
        "reg[18:0] a;",
        "reg[18:0] b;",
        "reg[24:0] mul_a_b;",
        "wire[37:0] mul_a_b_buf;",
        "assign mul_a_b_buf = $signed(a)*$signed(b);",
        "always @(posedge clk) begin",
        "\tmul_a_b <= mul_a_b_buf[36:12];",
        "end"
    };

    auto a = Variable::create("a", 25, 50, 25);
    auto b = Variable::create("b", 0, 50, 0);

    auto result = a * b;

    Codegen codegen({std::make_pair("out", result)});

    // REQUIRE(codegen.generate_code() == expected);
    require_same_lines(codegen.generate_code(FixedFixpoint{12}), expected);
}

TEST_CASE( "Codegen works for single layer divisions", "[nodes]" ) {
    std::vector<std::string> expected = {
        "reg[18:0] a;",
        "reg[18:0] b;",
        "reg[19:0] div_a_b;",
        "wire[30:0] div_a_b_buffer;",
        "assign div_a_b_buffer = $signed({a,12'b0})/$signed(b);",
        "always @(posedge clk) begin",
        "\tdiv_a_b <= div_a_b_buffer[19:0];",
        "end"
    };

    auto a = Variable::create("a", 25, 50, 25);
    auto b = Variable::create("b", 0.5, 50, 0);

    auto result = a / b;

    Codegen codegen({std::make_pair("out", result)});

    require_same_lines(codegen.generate_code(FixedFixpoint{12}), expected);
}


TEST_CASE( "Codegen works for single layer additions", "[nodes]" ) {
    std::vector<std::string> expected = {
        "reg[18:0] a;",
        "reg[18:0] b;",
        "reg[19:0] add_a_b;",
        "always @(posedge clk) begin",
        "\tadd_a_b <= $signed(a)+$signed(b);",
        "end"
    };

    auto a = Variable::create("a", 25, 50, 25);
    auto b = Variable::create("b", 0, 50, 0);

    auto result = a + b;

    Codegen codegen({std::make_pair("out", result)});

    require_same_lines(codegen.generate_code(FixedFixpoint{12}), expected);
}

TEST_CASE( "Pipeline registers are used", "[nodes]") {
    auto a = Variable::create("a", 0, 50, 0);
    auto b = Variable::create("b", 0, 50, 0);

    auto result_ = a + b;
    auto result = result_ + a;

    std::vector<std::string> expected = {
        "reg[18:0] a;",
        "reg[18:0] a_1;",
        "always @(posedge clk) begin",
        "\ta_1 <= a;",
        "end",
        "reg[18:0] b;",
        "reg[19:0] add_a_b;",
        "always @(posedge clk) begin",
        "\tadd_a_b <= $signed(a)+$signed(b);",
        "end",
        "reg[20:0] add_add_a_b_a;",
        "always @(posedge clk) begin",
        "\tadd_add_a_b_a <= $signed(add_a_b)+$signed(a_1);",
        "end"
    };


    Codegen codegen({std::make_pair("out", result)});

    require_same_lines(codegen.generate_code(FixedFixpoint{12}), expected);
}

TEST_CASE( "Codegen works when more than 2 vars are added in the same op", "[nodes]" ) {
    auto a = Variable::create("a", 0, 50, 0);
    auto b = Variable::create("b", 0, 50, 0);
    auto c = Variable::create("c", 0, 50, 0);

    auto result = a + b + c;

    std::vector<std::string> expected = {
        "reg[16:0] a;",
        "reg[16:0] b;",
        "reg[17:0] add_a_b;",
        "always @(posedge clk) begin",
        "\tadd_a_b <= $signed(a)+$signed(b);",
        "end",
        "reg[16:0] c;",
        "reg[16:0] c_1;",
        "always @(posedge clk) begin",
        "\tc_1 <= c;",
        "end",
        "reg[18:0] add_add_a_b_c;",
        "always @(posedge clk) begin",
        "\tadd_add_a_b_c <= $signed(add_a_b)+$signed(c_1);",
        "end"
    };

    Codegen codegen({std::make_pair("out", result)});

    require_same_lines(codegen.generate_code(FixedFixpoint{10}), expected);
}

TEST_CASE( "Codegen works for module inputs", "[codegen]" ) {
    auto a = Input<0, 50>::create("a", 0);
    auto b = Input<0, 50>::create("b", 0);
    auto result = a + b;

    Codegen codegen({std::make_pair("result", result)});
    auto code = codegen.generate_module("test", FixedFixpoint{10});

    std::vector<std::string> expected = {
        "module test",
        "\t\t( clk",
        "\t\t, rst",
        "\t\t, input_valid",
        "\t\t, output_valid",
        "\t\t, a",
        "\t\t, b",
        "\t\t, result",
        "\t\t);",
        "\toutput[17:0] result;",
        "\tassign result = add_a_b;",
        "\toutput output_valid;",
        "\tinput clk;",
        "\tinput rst;",
        "\tinput input_valid;",
        "\tinput[16:0] a;",
        "\tinput[16:0] b;",
        "\treg[17:0] add_a_b;",
        "\talways @(posedge clk) begin",
        "\t\tadd_a_b <= $signed(a)+$signed(b);",
        "\tend",
        "\treg pl_validity_buffer;",
        "\talways @(posedge clk) begin",
        "\t\tif(rst == 1) begin",
        "\t\t\tpl_validity_buffer <= 0;",
        "\t\tend",
        "\t\telse begin",
        "\t\t\tpl_validity_buffer <= input_valid;",
        "\t\tend",
        "\tend",
        "\tassign output_valid = pl_validity_buffer;",
        "endmodule",
        "`define test_DEPTH 1"
    };

    require_same_lines(code, expected);
}

TEST_CASE( "Pipeline validity check works", "[codegen]" ) {
    int depth = 10;
    std::vector<std::string> expected = {
        "reg[9:0] pl_validity_buffer;",
        "always @(posedge clk) begin",
        "\tif(rst == 1) begin",
        "\t\tpl_validity_buffer <= 0;",
        "\tend",
        "\telse begin",
        "\t\tpl_validity_buffer <= {pl_validity_buffer[8:0], input_valid};",
        "\tend",
        "end",
        "assign output_valid = pl_validity_buffer[9];"
    };

    auto result = validation_delay("input_valid", "output_valid", depth);

    REQUIRE(join(result) == join(expected));
}

TEST_CASE( "Pipeline validity at depth 1 works", "[codegen]" ) {
    std::vector<std::string> expected = {
        "reg pl_validity_buffer;",
        "always @(posedge clk) begin",
        "\tif(rst == 1) begin",
        "\t\tpl_validity_buffer <= 0;",
        "\tend",
        "\telse begin",
        "\t\tpl_validity_buffer <= input_valid;",
        "\tend",
        "end",
        "assign output_valid = pl_validity_buffer;"
    };

    auto result = validation_delay("input_valid", "output_valid", 1);

    REQUIRE(join(result) == join(expected));
}

TEST_CASE( "Pipeline registers are used in outputs", "[codegen]" ) {
    auto var1 = Input<0, 100>::create("a", 0);
    auto var2 = Input<0, 100>::create("b", 0) + Constant::create("c", 5);

    Codegen codegen({std::make_pair("var1", var1), std::make_pair("var2", var2)});
    auto code = codegen.generate_module("test", FixedFixpoint{10});

    REQUIRE(var1->get_delay_slots() == 1);

    bool found_assignment = false;
    for(auto line : code) {
        if(line.find("assign var1") != std::string::npos) {
            found_assignment = true;
            REQUIRE(line == std::string("\tassign var1 = a_1;"));
        }
    }

    REQUIRE(found_assignment == true);
}

TEST_CASE("Constant pipeline registers are not generated", "[codegen]") {
    auto var = Constant::create("a", 0);

    auto result = var + var + var;

    Codegen codegen({{"result", result}});

    std::vector<std::string> expected = {
        "localparam[10:0] a = 0;",
        "reg[10:0] add_a_a;",
        "always @(posedge clk) begin",
        "\tadd_a_a <= $signed(a)+$signed(a);",
        "end",
        "reg[10:0] add_add_a_a_a;",
        "always @(posedge clk) begin",
        "\tadd_add_a_a_a <= $signed(add_a_a)+$signed(a);",
        "end"
    };
    require_same_lines(codegen.generate_code(FixedFixpoint{10}), expected);
}

TEST_CASE("Pipeline registers for bools are 1 bit", "[codegen]") {
    auto a = Constant::create("a", 1);
    auto b = Constant::create("b", 2);
    auto result = a < b;

    result->request_delay_slots(1);

    Codegen codegen({{"result", result}});

    std::vector<std::string> expected = {
        "localparam[11:0] a = 1024;",
        "localparam[12:0] b = 2048;",
        "reg lt_a_b;",
        "always @(posedge clk) begin",
        "\tlt_a_b <= $signed(a)<$signed(b);",
        "end",
        "reg lt_a_b_1;",
        "always @(posedge clk) begin",
        "\tlt_a_b_1 <= lt_a_b;",
        "end"
    };
    require_same_lines(codegen.generate_code(FixedFixpoint{10}), expected);
}

TEST_CASE( "Min and max functions have reasonalbe names", "[codegen]") {
    auto var1 = Variable::create("a", 0, 100, 0);
    auto var2 = Variable::create("b", 0, 100, 0);

    REQUIRE(min(var1, var2)->base_name() == "min_a_b");
    REQUIRE(max(var1, var2)->base_name() == "max_a_b");
}

TEST_CASE( "Min function generate code", "[codegen]") {
    auto var1 = Variable::create("a", 0, 100, 0);
    auto var2 = Variable::create("b", 0, 100, 0);

    std::vector<std::string> expected = {
        "reg[17:0] min_a_b;",
        "always @(posedge clk) begin",
        "\tif(lt_a_b) begin",
        "\t\tmin_a_b <= a_1;",
        "\tend",
        "\telse begin",
        "\t\tmin_a_b <= b_1;",
        "\tend",
        "end"
    };

    REQUIRE(join(min(var1, var2)->get_code(FixedFixpoint{10})) == join(expected));
}


TEST_CASE("Long names get hashed") {
    auto a = Constant::create("aaaaaaaaaaaaaaaaaaaaaaaaa", 0);
    auto b = Constant::create("bbbbbbbbbbbbbbbbbbbbbbbbb", 0);

    std::string name = "add_aaaaaaaaaaaaaaaaaaaaaaaaa_bbbbbbbbbbbbbbbbbbbbbbbbb";

    std::size_t hash = std::hash<std::string>{}(name);
    // +name.size() because apparently hash collisions between simiarly lengthed
    // vars are very uncommmon
    auto expected = "h_" + std::to_string(hash) + std::to_string(name.size());

    auto result = a + b;
    REQUIRE(result->base_name() == expected);
}

TEST_CASE("Long bool binop names get hashed when generating code") {
    auto a = Constant::create("aaaaaaaaaaaaaaaaaaaaaa", 0);
    auto b = Constant::create("bbbbbbbbbbbbbbbbbbbbbb", 0);
    auto one = Constant::create("one", 1);

    std::string name = "or_lt_aaaaaaaaaaaaaaaaaaaaaa_one_lt_bbbbbbbbbbbbbbbbbbbbbb_one";

    auto result = (a<one) || (b<one);

    Codegen gen({{"", result}});

    auto code = gen.generate_code(FixedFixpoint{10});
    for(auto line : code) {
        if(line.find(name) != std::string::npos) {
            REQUIRE(line == "");
        }
    }
}


TEST_CASE("Lookup table module generation works") {
    auto lut = LookupTable::create("test", {1, 2}, {1.5, 0.5});

    std::vector<std::string> expected = {
        "module test",
        "\t\t#( parameter input_size = 32",
        "\t\t, parameter output_size = 32",
        "\t\t)",
        "\t\t( clk",
        "\t\t, addr",
        "\t\t, result",
        "\t\t);",
        "\tinput clk;",
        "\tinput[input_size-1:0] addr;",
        "\toutput[output_size-1:0] result;",
        "\treg[output_size-1:0] result_buffer;",
        "\tassign result = result_buffer;",
        "\talways @(posedge clk) begin",
        "\t\tcase(addr)",
        "\t\t\t4096: result_buffer <= 6144;",
        "\t\t\t8192: result_buffer <= 2048;",
        "\t\t\tdefault: result_buffer <= 0;",
        "\t\tendcase",
        "\tend",
        "endmodule"
    };

    REQUIRE(join(lut->get_module()) == join(expected));
}


TEST_CASE("Variable lengths are exported correctly") {
    auto bool_var = BoolConst::True;
    auto var1 = Variable::create("var1", -5, 5, 0);

    var1->optimal_fractional_bits = 1;

    auto codegen = Codegen({{"var1_out", var1}, {"true_out", bool_var}});

    std::vector<std::string> expected = {
        "`define LEN_var1 5", // 3 bits for value, one sign, one fractional
        "`define LEN_true 1",
    };

    require_same_lines(expected, codegen.variable_length_defines());
}
