#include <catch2/catch.hpp>

#include "../src/node.hpp"
#include "../src/codegen.hpp"
#include "../src/lookup_table.hpp"
#include "../src/interpolation.hpp"

////////////////////////////////////////////////////////////////////////////////
//              Node blanket impl tests

TEST_CASE( "parent_register works", "[node]" ) {
    auto a = Variable::create("a", 0, 100, 0);
    auto b = Variable::create("b", 0, 100, 0);
    auto partial = a + a;
    auto sum = partial + b;

    REQUIRE(sum->parent_register(b) == "b_1");
    REQUIRE(sum->parent_register(partial) == "add_a_a");
}

TEST_CASE( "Unique ancestors works", "[node]" ) {
    auto a = Constant::create("a", 0);
    auto b = Constant::create("b", 10);

    auto a_b = a + b;
    auto a_b_b = a_b + b;
    auto result = a_b_b + b;

    std::set<Node*> expected = {
        a.get_inner().get(),
        b.get_inner().get(),
        a_b.get_inner().get(),
        a_b_b.get_inner().get(),
    };
    REQUIRE(result->unique_ancestors() == expected);
}
////////////////////////////////////////////////////////////////////////////////
// Interpolation functions functions

TEST_CASE( "Interp1Equid works", "[interpolation]" ) {
    auto input = Variable::create("a", 0, 2, 1);

    auto result = interp1equid("i1", {0, 2}, {1, 3}, input);
    auto interpolator = result.second;
    auto validator = result.first;

    // TODO: Remove
    // interpolator->dump_graph();

    REQUIRE(interpolator->calculate_value() == 2);
    REQUIRE(validator->calculate_value() == true);
    // REQUIRE(interpolator->min() == 0);
    // REQUIRE(interpolator->max() == 2);
    // Test overflow
    input->set_value(4);
    interpolator->traverse_depth_first([](auto node){node->mark_dirty();});
    validator->traverse_depth_first([](auto node){node->mark_dirty();});
    REQUIRE(interpolator->calculate_value() == 1);
    REQUIRE(validator->calculate_value() == false);
    // Test underflow
    input->set_value(-4);
    interpolator->traverse_depth_first([](auto node){node->mark_dirty();});
    validator->traverse_depth_first([](auto node){node->mark_dirty();});
    REQUIRE(interpolator->calculate_value() == 1);
    REQUIRE(validator->calculate_value() == false);

    REQUIRE(interpolator->base_name() == "interp1_i1");
    REQUIRE(validator->base_name() == "interp1valid_i1");
}

double reference_interp2equid(
    const std::vector<double> &x,
    const std::vector<double> &y,
    const std::vector<double> &z,
    unsigned long nx,
    unsigned long ny,
    double xmin,
    double ymin,
    double dx,
    double dy,
    double xp,
    double yp
) {
    int x_idx = 0;
    int y_idx = 0;
    
    double q11, q12, q21, q22;
    
    if( (xp < x[0]) || (xp > x[nx-1]) || (yp < y[0]) || (yp > y[ny-1])){
        return *std::max_element(z.begin(), z.end());
    }
    else {
        
        // estimate
        x_idx = trunc( (xp-xmin)/dx ) + 1;
        y_idx = trunc( (yp-ymin)/dy ) + 1;
        
        q11 = z[(x_idx-1)*ny + y_idx-1];
        q12 = z[(x_idx-1)*ny + y_idx];
        q21 = z[x_idx*ny + y_idx-1];
        q22 = z[x_idx*ny + y_idx];
        
        return ( q11*(x[x_idx] - xp)*(y[y_idx] - yp) + q21*(xp-x[x_idx-1])*(y[y_idx] - yp) + q12*(x[x_idx]-xp)*(yp - y[y_idx-1]) + q22*(xp-x[x_idx-1])*(yp - y[y_idx-1]) )/( ( x[x_idx] - x[x_idx-1])*(y[y_idx] - y[y_idx-1]) );
    }
}

TEST_CASE( "Interp2Equid works", "[interpolation]" ) {
    std::vector<double> x = {0,2};
    std::vector<double> y = {0,4};
    std::vector<double> z = {0, 1, 1, 2};
    auto nx = x.size();
    auto ny = y.size();
    auto dx = 2;
    auto dy = 4;
    auto xmin = 0;
    auto ymin = 0;
    auto ref_xp = 1;
    auto ref_yp = 1;
    auto reference_result = reference_interp2equid(
        x,
        y,
        z,
        nx,
        ny,
        xmin,
        ymin,
        dx,
        dy,
        ref_xp,
        ref_yp
    );

    auto xp = Variable::create("xp", -100, 2, ref_xp);
    auto yp = Variable::create("yp", -100, 2, ref_yp);

    auto result_ = interp2equid("result", x, y, z, xp, yp);
    auto result_valid = result_.first;
    auto result = result_.second;

    REQUIRE(int(result->calculate_value() * 1000) == int(reference_result * 1000));
    REQUIRE(result->min() == 0);
    // TODO: Re-enable test
    // REQUIRE(result->max() == 2);

    REQUIRE(result_valid->calculate_value() == true);

    // Out of bounds checks
    xp->set_value(-3);
    yp->set_value(-3);
    xp->mark_dirty();
    yp->mark_dirty();
    result_valid->traverse_depth_first([](auto node) {node->mark_dirty();});
    result->traverse_depth_first([](auto node) {node->mark_dirty();});
    REQUIRE(result_valid->calculate_value() == false);
    REQUIRE(result->calculate_value() == 0);

    xp->set_value(3);
    yp->set_value(3);
    xp->mark_dirty();
    yp->mark_dirty();
    result_valid->traverse_depth_first([](auto node) {node->mark_dirty();});
    result->traverse_depth_first([](auto node) {node->mark_dirty();});
    REQUIRE(result_valid->calculate_value() == false);
    // REQUIRE(result->calculate_value() == 2);
    // REQUIRE(result->graph_label_name() == "interp2");
    // REQUIRE(result_valid->graph_label_name() == "interp2valid");
    REQUIRE(result->base_name() == "interp2_result");
    REQUIRE(result_valid->base_name() == "interp2valid_result");
}

////////////////////////////////////////////////////////////////////////////////
// Standalone function tests

TEST_CASE( "Min produces correct expression tree", "[codegen]") {
    auto a = Variable::create("a", 0, 100, 50);
    auto b = Variable::create("b", 25, 150, 40);
    auto result = min(a, b);
    REQUIRE(result->min() == 0);
    REQUIRE(result->max() == 100);
    REQUIRE(result->calculate_value() == 40);
}
TEST_CASE( "Max produces correct expression tree", "[codegen]") {
    auto a = Variable::create("a", 0, 100, 40);
    auto b = Variable::create("b", 25, 150, 50);
    auto result = max(a, b);
    REQUIRE(result->max() == 150);
    REQUIRE(result->min() == 25);
    REQUIRE(result->calculate_value() == 50);
}




////////////////////////////////////////////////////////////////////////////////
// Node execution functions

TEST_CASE( "Node exection works", "[codegen]" ) {
    auto a = Constant::create("a", 5);
    auto b = Variable::create("b", 5, 10, 7);
    auto c = Input<0, 10>::create("c", 7);

    auto sum = a + b;
    auto difference = a - b;
    auto product = a * b;
    auto fraction = b / c;
    auto abs = Abs::create(difference);

    REQUIRE(a->calculate_value() == 5);
    REQUIRE(b->calculate_value() == 7);
    REQUIRE(c->calculate_value() == 7);
    REQUIRE(sum->calculate_value() == 12);
    REQUIRE(difference->calculate_value() == -2);
    REQUIRE(product->calculate_value() == 35);
    REQUIRE(fraction->calculate_value() == 1);
    REQUIRE(abs->calculate_value() == 2);
}


////////////////////////////////////////////////////////////////////////////////
// Misc
TEST_CASE( "Unique ancestors for binary operators works" "[node]" ) {
    auto a = Constant::create("a", 0);
    auto b = Constant::create("b", 100);

    REQUIRE((a+b)->unique_ancestors() == std::set<Node*>{a.ptr(), b.ptr()});
    REQUIRE((a>b)->unique_ancestors() == std::set<Node*>{a.ptr(), b.ptr()});
}

