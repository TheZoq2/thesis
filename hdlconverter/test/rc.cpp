#include <catch2/catch.hpp>

#include "../src/rc.hpp"

// Pretty pointless test at the moment, but it does instanciate the Rc type
TEST_CASE("Operators", "[rc]") {
    Rc<int> a(new int(1));
}
