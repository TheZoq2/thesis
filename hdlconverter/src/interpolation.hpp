#ifndef H_INTERPOLATION
#define H_INTERPOLATION

#include "node.hpp"
#include "lookup_table.hpp"

struct Interp1EquidParams {
    public:
        Interp1EquidParams(
            std::string name,
            const std::vector<double> x,
            const std::vector<double> y,
            Rc<FracNode> xin
        );

        std::string name;
        Rc<LookupTable> x;
        Rc<LookupTable> y;
        Rc<Constant> size_x;
        Rc<Constant> dx;
        Rc<Constant> dx_inv;
        Rc<Constant> xmin;
        Rc<Constant> xmax;
        double ymin;
        double ymax;
        Rc<FracNode> x_in;

        bool output_valid();
};


std::pair<Rc<BoolNode>, Rc<FracNode>> interp1equid(
    std::string name,
    const std::vector<double> x,
    const std::vector<double> y,
    Rc<FracNode> xin
);





struct Interp2EquidParams {
    Interp2EquidParams(
        std::string name,
        const std::vector<double> x,
        const std::vector<double> y,
        const std::vector<double> z,
        Rc<FracNode> x_in,
        Rc<FracNode> y_in
    );

    std::string name;
    Rc<LookupTable> x;
    Rc<LookupTable> y;
    Rc<LookupTable> z;
    Rc<Constant> nx;
    Rc<Constant> ny;
    Rc<Constant> xmin;
    Rc<Constant> ymin;
    Rc<Constant> xmax;
    Rc<Constant> ymax;
    Rc<Constant> dx;
    Rc<Constant> dy;
    Rc<Constant> dx_inv;
    Rc<Constant> dy_inv;
    Rc<FracNode> x_in;
    Rc<FracNode> y_in;
};

std::pair<Rc<BoolNode>, Rc<FracNode>> interp2equid(
        std::string name,
        const std::vector<double> x,
        const std::vector<double> y,
        const std::vector<double> z,
        Rc<FracNode> xp,
        Rc<FracNode> yp
    );
std::pair<Rc<BoolNode>, Rc<FracNode>> interp2equid(std::string name, Interp2EquidParams params);

#endif
