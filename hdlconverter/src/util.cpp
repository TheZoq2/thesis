#include "util.hpp"

#include <cassert>

std::vector<std::string> indent_lines(std::vector<std::string> lines) {
    std::vector<std::string> result;
    for(auto line: lines) {
        result.push_back(indent(1, line));
    }
    return result;
}

std::string join(std::vector<std::string> lines) {
    std::string result;
    for(auto str : lines) {
        result += str + "\n";
    }
    return result;
}


std::string var_type_name(VarType type) {
    switch (type) {
        case VarType::INPUT: return "input";
        case VarType::LOCALPARAM: return "localparam";
        case VarType::OUTPUT: return "output";
        case VarType::REG: return "reg";
        case VarType::WIRE: return "wire";
    }
}

std::string indent(std::size_t amount, std::string line) {
    std::string result = "";
    for(std::size_t i = 0; i < amount; ++i) {
        result += "\t";
    }
    return result + line;
}

std::string verilog_variable(VarType type, std::string name, std::size_t size) {
    // The [n-1:0] part of the declaration
    auto length_specifier = size != 0 ? "[" + std::to_string(size-1) + ":0]" : "";
    if(size == 1) {
        length_specifier = "";
    }
    return var_type_name(type) + length_specifier + " " + name + ";";
}

std::string register_code(std::string name, std::size_t size) {
    return verilog_variable(VarType::REG, name, size);
}

std::vector<std::string> clocked_block(std::vector<std::string> content) {
    auto indented = indent_lines(content);
    // Create the always section
    std::vector<std::string> result = {"always @(posedge clk) begin"};
    // Add the content
    result.insert(result.end(), indented.begin(), indented.end());
    // Add end keyword
    result.push_back("end");
    return result;
}

/*
  Concatenates two vectors of strings
*/
std::vector<std::string> merge_code(const std::vector<std::vector<std::string>> code_segments) {
    std::vector<std::string> result;
    for(auto segment : code_segments) {
        result.insert(result.end(), segment.begin(), segment.end());
    }
    return result;
}

std::vector<std::string> reset_or_value(
    std::string reg_name,
    std::string reset_value,
    std::string value
) {
    return if_statement(
        "rst == 1",
        {reg_name + " <= " + reset_value + ";"},
        {reg_name + " <= " + value + ";"}
    );
}


std::vector<std::string> if_statement(
    const std::string condition,
    const std::vector<std::string> true_branch,
    const std::vector<std::string> false_branch
) {
    return merge_code({
        {"if(" + condition + ") begin"},
        indent_lines(true_branch),
        { "end"
        , "else begin"
        },
        indent_lines(false_branch),
        {"end"}
    });
}


std::string sign_extend(std::string reg, int original_size, int extended_size) {
    if(original_size < extended_size) {
        return "{{" + std::to_string(extended_size - original_size)
            + "{" + reg + "[" + std::to_string(original_size - 1) + "]}}, " + reg + "}";
    }
    else {
        return reg;
    }
}


ExecutionType approximate_log2(ExecutionType input) {
#ifdef USE_FIXPOINT
    const int fixpoint_mul = pow(2, FIXPOINT);

    float as_float = input.ToFloat();

    int largest_1 = floor(log2(as_float));

    int x = (input * fixpoint_mul).ToInt<int>();
    int shifted;
    if(largest_1 > 0) {
        shifted = x >> largest_1;
    }
    else {
        shifted = x << (-largest_1);
    }

    float lutout = log2((float) shifted / fixpoint_mul);

    return (ExecutionType) (largest_1 + lutout);
#else
    return log2(input);
#endif
}

ExecutionType approximate_pow2(ExecutionType input) {
#ifdef USE_FIXPOINT
    auto integer_part = input.ToInt<int>();

    auto fractional_part = input - integer_part;

    auto lutout = pow(2, fractional_part.ToDouble());

    return lutout * pow(2, integer_part);
#else
    return pow(2, input);
#endif
}

