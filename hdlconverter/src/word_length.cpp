#include "word_length.hpp"

#include <pybind11/embed.h>
#include <pybind11/functional.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include <functional>


bool WordLengthOptimizer::initialised = false;

namespace py = pybind11;

struct FnWrapper {
    std::function<double(py::array_t<double>)> fn;
};

PYBIND11_EMBEDDED_MODULE(cpp, m) {
    py::class_<FnWrapper>(m, "FnWrapper")
        .def(py::init<>())
        .def("__call__",
            [](const FnWrapper &a, py::array_t<double> in) {
                return a.fn(in);
            }
        );
}

void WordLengthOptimizer::optimize(
    std::vector<std::tuple<double, Rc<FracNode>>> outputs,
    std::vector<Rc<Node>> non_frac_outputs,
    WordLengthOptions options
) {
    if(!initialised) {
        py::initialize_interpreter();
        initialised = true;
    }

    std::set<FracNode*> fractional_nodes;
    // Insert all nodes reachable from fractional outputs
    for(auto [_max_error, node] : outputs) {
        fractional_nodes.insert(node.ptr());
        for(auto ancestor : node->unique_ancestors()) {
            auto casted = dynamic_cast<FracNode*>(ancestor);
            if(casted != nullptr) {
                fractional_nodes.insert(casted);
            }
        }
    }
    // Insert nodes only reachable from non-frac-nodes
    for(auto node : non_frac_outputs) {
        for(auto ancestor : node->unique_ancestors()) {
            auto casted = dynamic_cast<FracNode*>(ancestor);
            if(casted != nullptr) {
                fractional_nodes.insert(casted);
            }
        }
    }

    // Give each node a unique index
    std::size_t current_index = 0;
    for(auto ancestor : fractional_nodes) {
        ancestor->set_array_index(current_index);
        current_index++;
    }



    auto scope = py::globals();
    // py::object scope = py::module::import("__main__").attr("__dict__");

    scope["np"] = py::module::import("numpy");
    scope["cpp"] = py::module::import("cpp");
    std::vector<FnWrapper> functions{};

    for(auto output : outputs) {
        // As always, C++ can not have non-half-assed implementations of anything, naturally a
        // closure can not capture variables defined in a structured bindings!
        // (╯°□°）╯︵ ┻━┻
        // https://stackoverflow.com/questions/46114214/lambda-implicit-capture-fails-with-variable-declared-from-structured-binding
        auto max_error = std::get<0>(output);
        auto error_function = std::get<1>(output)->error_function();
        functions.push_back(
            FnWrapper{
                [max_error, error_function](py::array_t<double> points) {
                    auto buf = points.request();
                    auto ptr = (double*) buf.ptr;

                    return max_error - error_function(ptr);
                }
            }
        );
    }
    scope["functions"] = functions;

    std::vector<std::tuple<int, int>> constraint_tuples;
    for(auto ancestor : fractional_nodes) {
        for(auto constraint : ancestor->smaller_word_constraints()) {
            constraint_tuples.push_back(
                {ancestor->get_array_index(), constraint->get_array_index()}
            );
        }
    }
    scope["constraint_tuples"] = constraint_tuples;
    scope["node_amount"] = fractional_nodes.size();
    scope["filename"] = options.output_name;
    scope["print_info"] = options.print_info;
    scope["regenerate"] = options.regenerate;

    py::eval_file("python/word_length.py", scope);

    auto result = (py::array_t<double>) scope["result_array"];
    auto word_lengths = (double*) result.request().ptr;

    for(auto node : fractional_nodes) {
        // These optimisation algorithms get close to the optimal value but don't hit it
        // exactly. Subtracting 0.01 from the result should mitigate that at the cost
        // of non-guaranteed correctness
        node->optimal_fractional_bits = ceil(word_lengths[node->get_array_index()] - 0.01);
    }
}





WordLengthOptions WordLengthOptions::from_file() {
    auto result = WordLengthOptions(*this);
    result.regenerate = false;
    return result;
}
WordLengthOptions WordLengthOptions::with_filename(std::string name) {
    auto result = WordLengthOptions(*this);
    result.output_name = name;
    return result;
}
WordLengthOptions WordLengthOptions::output_info() {
    auto result = WordLengthOptions(*this);
    result.print_info = true;
    return result;
}
