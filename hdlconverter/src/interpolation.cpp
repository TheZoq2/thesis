#include "interpolation.hpp"

#include <math.h>

Interp1EquidParams::Interp1EquidParams(
        std::string name,
        const std::vector<double> x,
        const std::vector<double> y,
        Rc<FracNode> x_in
    )
    : name(name)
    , x(LookupTable::create(name + "_x", x))
    , y(LookupTable::create(name + "_y", y))
    , size_x(Constant::create(name + "_size_x", x.size()))
      // Works because x is equidistant
    , dx(Constant::create(name + "_dx", x[1]-x[0]))
    , dx_inv(Constant::create(name + "_dx_inv", 1/(x[1]-x[0])))
      // Works because x monotonically increasing
    , xmin(Constant::create(name + "_xmin", x[0]))
    , xmax(Constant::create(name + "_xmax", x[x.size()-1]))
    , ymin(*std::min_element(y.begin(), y.end()))
    , ymax(*std::max_element(y.begin(), y.end()))
    , x_in(x_in)
{}

std::pair<Rc<BoolNode>, Rc<FracNode>> interp1equid(
            std::string name,
            Interp1EquidParams params
        )
{
    auto zero = Constant::create(name + "_zero", 0);
    auto one = Constant::create(name + "_one", 1);
    auto valid = !( (params.x_in < params.xmin)
                 || (params.x_in > params.xmax)
                 );

    auto xp = params.x_in;
    // auto xp = _if(valid, params.x_in, params.xmin);

    Rc<FracNode> ii_ = Truncate::create((xp - params.xmin) / params.dx ) + one;
    ii_->override_name(name + "_ii_");

    // This differes from the original implementation but seems required to avoid out
    // of bounds access
    ii_ = min(ii_, Constant::create(name + "_max", params.x->size()-1));
    ii_->override_name(name + "_ii_min");
    auto ii = max(ii_, zero);
    ii->override_name(name + "_ii");

    auto sub_ii_one = ii - one;

    auto x0 = LutResult::create(params.x, sub_ii_one);
    x0->override_name(name + "_x0");
    auto x1 = LutResult::create(params.x, ii);
    x1->override_name(name + "_x1");
    auto y0 = LutResult::create(params.y, sub_ii_one);
    y0->override_name(name + "_y0");
    auto y1 = LutResult::create(params.y, ii);
    y1->override_name(name + "_y1");
    auto result_ = y0 + (y1-y0) * (xp - x0) * params.dx_inv;
    result_->override_name(name + "_result_");

    auto result = _if(valid, result_, Constant::create(name + "_min", params.ymin));
    result->override_name(name + "_result");
    // This is needed because if-statements can't automatically find this relation
    result->_override_min(params.ymin);
    result->_override_max(params.ymax);

    result->override_name("interp1_" + name);
    valid->override_name("interp1valid_" + name);

    return std::make_pair(valid, result);
}

std::pair<Rc<BoolNode>, Rc<FracNode>> interp1equid(
        std::string name,
        const std::vector<double> x,
        const std::vector<double> y,
        Rc<FracNode> xin
    )
{
    Interp1EquidParams params(name, x, y, xin);
    return interp1equid(name, params);
}




//////////////////////////////////////////////////////////////////////
//  Interp2

Interp2EquidParams::Interp2EquidParams(
        std::string name,
        std::vector<double> const x,
        std::vector<double> const y,
        std::vector<double> const z,
        Rc<FracNode> x_in,
        Rc<FracNode> y_in
    )
    : name(name)
    , x(LookupTable::create(name + "_x", x))
    , y(LookupTable::create(name + "_y", y))
    , z(LookupTable::create(name + "_z", z))
    , nx(Constant::create(name + "_nx", x.size()))
    , ny(Constant::create(name + "_ny", y.size()))
    , xmin(Constant::create(name + "_xmin", *x.begin()))
    , ymin(Constant::create(name + "_ymin", *y.begin()))
    , xmax(Constant::create(name + "_xmax", *(x.end()-1)))
    , ymax(Constant::create(name + "_ymax", *(y.end()-1)))
    // // This works because the steps are equidistant
    , dx(Constant::create(name + "_dx", x[1] - x[0]))
    , dy(Constant::create(name + "_dy", y[1] - y[0]))
    , dx_inv(Constant::create(name + "_dx_inv", 1/(x[1] - x[0])))
    , dy_inv(Constant::create(name + "_dy_inv", 1/(y[1] - y[0])))
    , x_in(x_in)
    , y_in(y_in)
{}


std::pair<Rc<BoolNode>, Rc<FracNode>> interp2equid(std::string name, Interp2EquidParams params) {
    auto zero = Constant::create(name + "interp_" + name + "_zero", 0);
    auto one = Constant::create(name + "interp_" + name + "_one", 1);

    auto valid =
            !( (params.x_in < params.xmin)
            || (params.x_in > params.xmax)
            || (params.y_in < params.ymin)
            || (params.y_in > params.ymax)
            );

    auto xp = _if(valid, params.x_in, params.xmin);
    auto yp = _if(valid, params.y_in, params.ymin);
    xp->override_name("interp_" + name + "_xp");
    yp->override_name("interp_" + name + "_yp");
    // auto xp = max(params.xmin, min(params.x_in, params.xmax));
    // auto yp = max(params.ymin, min(params.y_in, params.ymax));


    auto x_idx = Truncate::create((xp-params.xmin) / params.dx) + one;
    x_idx->override_name("interp_" + name + "_x_idx");
    auto y_idx = Truncate::create((yp-params.ymin) * params.dy_inv) + one;
    y_idx->override_name("interp_" + name + "_y_idx");

    auto sub_x_idx_one = (x_idx-one);
    auto sub_y_idx_one = (y_idx-one);
    auto mul_sub_x_idx_one_ny = sub_x_idx_one*params.ny;
    auto mul_x_idx_ny = x_idx*params.ny;

    auto q11 = LutResult::create(params.z, (mul_sub_x_idx_one_ny + sub_y_idx_one));
    auto q12 = LutResult::create(params.z, (mul_sub_x_idx_one_ny + y_idx));
    auto q21 = LutResult::create(params.z, (mul_x_idx_ny + sub_y_idx_one));
    auto q22 = LutResult::create(params.z, (mul_x_idx_ny + y_idx));
    q11->override_name("interp_" + name + "_q11");
    q12->override_name("interp_" + name + "_q12");
    q21->override_name("interp_" + name + "_q21");
    q22->override_name("interp_" + name + "_q22");

    auto x0 = LutResult::create(params.x, sub_x_idx_one);
    auto y0 = LutResult::create(params.y, sub_y_idx_one);
    auto x1 = LutResult::create(params.x, x_idx);
    auto y1 = LutResult::create(params.y, y_idx);
    x0->override_name("interp_" + name + "_x0");
    y0->override_name("interp_" + name + "_y0");
    x1->override_name("interp_" + name + "_x1");
    y1->override_name("interp_" + name + "_y1");


    auto result = (
                q11*(x1 - xp)*(y1 - yp) +
                q21*(xp-x0)*(y1 - yp) +
                q12*(x1-xp)*(yp - y0) +
                q22*(xp-x0)*(yp - y0)
            ) * params.dx_inv * params.dy_inv;


    result->override_name("interp2_" + name);
    valid->override_name("interp2valid_" + name);

    return std::make_pair(valid, result);
}

std::pair<Rc<BoolNode>, Rc<FracNode>> interp2equid(
        std::string name,
        const std::vector<double> x,
        const std::vector<double> y,
        const std::vector<double> z,
        Rc<FracNode> xp,
        Rc<FracNode> yp
    )
{
    auto params = Interp2EquidParams(name, x, y, z, xp, yp);
    return interp2equid(name, params);
}
