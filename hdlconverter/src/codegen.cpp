#include "codegen.hpp"

#include "util.hpp"
#include "rc.hpp"

#include <algorithm>
#include <utility>

/*
  Sets the specified nodes as outputs.

  This computes the required delay slots and adds all nodes to the codegen for generation
*/
Codegen::Codegen(std::map<std::string, Rc<Node>> nodes) {
    std::vector<Rc<Node>> only_nodes;
    for(auto n : nodes) {
        only_nodes.push_back(n.second);
        this->nodes.merge(n.second->unique_ancestors());
        this->nodes.insert(n.second.ptr());
    }
    this->outputs = nodes;
    assign_delay_slots(only_nodes, this->output_depth() + 1);
}

/*
  Generates the code for each node
*/
std::vector<std::string> Codegen::generate_code(WordLength word_lengths) {
    // Since verilog is such a nice and friendly language, it declares variables
    // passed as inputs or outputs to modules as they appear. Then it gets upset
    // when the actual variable is declared later -.-
    // To remedy that, we have to sort the variables
    std::vector<Node*> sorted_nodes;
    for(auto node : nodes) {
        sorted_nodes.push_back(node);
    }
    std::sort(sorted_nodes.begin(), sorted_nodes.end(), [](auto a, auto b){
        return a->depth() < b->depth();
    });

    std::vector<std::string> result;
    for(auto node : sorted_nodes) {
        // Get the code for the node and insert it into the result
        auto code = node->get_code(word_lengths);
        result.insert(result.end(), code.begin(), code.end());
        auto pipeline = node->generate_pipeline_code(word_lengths);
        result.insert(result.end(), pipeline.begin(), pipeline.end());
    }

    return result;
}


/*
  Generates a module for the current nodes. Input type nodes will be added
  as inputs to the model and the specified outputs will be outputs.
*/
std::vector<std::string> Codegen::generate_module(std::string name, WordLength word_lengths) {
    // Generate the module keyword and name
    std::vector<std::string> result = {"module " + name};

    // Add clock input
    result.push_back(indent(2, "( clk"));
    result.push_back(indent(2, ", rst"));
    result.push_back(indent(2, ", input_valid"));
    result.push_back(indent(2, ", output_valid"));
    // Add other inputs
    for(auto node : nodes) {
        if(node->is_input()) {
            result.push_back(indent(2, ", " + node->base_name() + "_non_reg"));
        }
    }
    // Add outputs
    for(auto output : outputs) {
        result.push_back(indent(2, ", " + output.first));
    }
    // Add end of parameter declaration
    result.push_back(indent(2, ");"));

    // Declare types of outputs
    for(auto [name, node] : outputs) {
        // Add the output keyword and name
        result.push_back(
            indent(1, verilog_variable(VarType::OUTPUT, name, node->register_size(word_lengths)))
        );
        // Addign the variable containing the output value to the actual output
        auto var_name = node->pipeline_buffer_name(
            output_depth() - node->depth()
        );
        result.push_back(indent(
            1,
            "assign " + name + " = " + var_name + ";"
        ));
    }

    // Add output validity output
    result.push_back(indent(1, "output output_valid;"));
    // Declare the clock and reset lines as input
    result.push_back(indent(1, "input clk;"));
    result.push_back(indent(1, "input rst;"));
    // Add input validity input
    result.push_back(indent(1, "input input_valid;"));

    // Generate the code for all the nodes
    auto code = indent_lines(this->generate_code(word_lengths));
    result.insert(result.end(), code.begin(), code.end());

    auto pl_validity_code = indent_lines(
        validation_delay("input_valid", "output_valid", output_depth())
    );
    result.insert(result.end(), pl_validity_code.begin(), pl_validity_code.end());
    // Add endmodule
    result.push_back("endmodule");

    result.push_back("`define " + name + "_DEPTH " + std::to_string(output_depth()));

    return result;
}

int Codegen::output_depth() {
    int depth = outputs.begin()->second->depth();
    for(auto n : outputs) {
        depth = std::max(depth, n.second->depth());
    }
    return depth;
}

std::vector<std::string> Codegen::list_variables() {
    std::vector<std::string> result;
    for(auto node : nodes) {
        result.push_back(node->base_name());
    }
    return result;
}


// TODO: Remove
std::vector<std::string> Codegen::variable_length_defines() {
    std::vector<std::string> result;
    for(auto node : nodes) {
        unsigned int length = 1;
        auto casted = dynamic_cast<FracNode*>(node);
        if(casted != nullptr) {
            length = casted->total_bits();
        }

        result.push_back("`define LEN_" + node->base_name() 
                + " " + std::to_string(length));
    }
    return result;
    return {};
}


// Free functions
std::vector<std::string> validation_delay(
    std::string in_validity_name,
    std::string out_validity_name,
    int depth
) {
    if(depth == 1) {
        return merge_code({
            {register_code("pl_validity_buffer", depth)},
            clocked_block(reset_or_value("pl_validity_buffer", "0", in_validity_name)),
            {"assign " + out_validity_name + " = pl_validity_buffer;"}
        });
    }
    else {

    }

    std::string value =
                "{pl_validity_buffer["
                + std::to_string(depth-2)
                + ":0], "
                + in_validity_name
                + "}";

    return merge_code({
        {register_code("pl_validity_buffer", depth)},
        clocked_block(reset_or_value("pl_validity_buffer", "0", value)),
        { "assign " + out_validity_name + " = "
        + "pl_validity_buffer[" + std::to_string(depth - 1) + "];"
        }
    });
}

