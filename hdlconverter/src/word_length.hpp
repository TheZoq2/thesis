#ifndef H_WORD_LENGTH
#define H_WORD_LENGTH

#include <tuple>

#include "node.hpp"

class WordLengthOptions {
    public:
        WordLengthOptions from_file();
        WordLengthOptions with_filename(std::string output_name);
        WordLengthOptions output_info();

        std::string output_name = "word_length";
        bool regenerate = true;
        bool print_info = false;
};


class WordLengthOptimizer {
    public:
        static void optimize(
            std::vector<std::tuple<double, Rc<FracNode>>> outputs,
            std::vector<Rc<Node>> non_frac_outputs,
            WordLengthOptions options
        );
    private:
        static bool initialised;
};

#endif
