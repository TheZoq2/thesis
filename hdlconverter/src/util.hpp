#ifndef H_UTIL
#define H_UTIL

#include <vector>
#include <string>
#include <cmath>

#include "config.hpp"

// Different types of variables
enum class VarType {
    REG,
    INPUT,
    OUTPUT,
    LOCALPARAM,
    WIRE,
};

// Returns the keyword used to delare a variable with the specified type
std::string var_type_name(VarType type);

// Adds `amount` indentation levels to the specified line
std::string indent(std::size_t amount, std::string line);
// Adds one level of indentation to all the specified lines

std::vector<std::string> indent_lines(std::vector<std::string> lines);
// Merge a vector of lines to a string with newlines

std::string join(std::vector<std::string> lines);
// Returns a verilog "variable" with the specified type, name and amount of bits
std::string verilog_variable(VarType type, std::string name, std::size_t size);

// Generates verilog code for a register with the specified name and size.
std::string register_code(std::string name, std::size_t size);

// Surrounds the specified block of code in `always @(posedge clk)`
std::vector<std::string> clocked_block(std::vector<std::string> content);

// Merges several vectors of lines into a single vector of lines
std::vector<std::string> merge_code(const std::vector<std::vector<std::string>> code_segments);

// Generates code for a verilog if statement
std::vector<std::string> if_statement(
    const std::string condition,
    const std::vector<std::string> true_branch,
    const std::vector<std::string> false_branch
);

// Generate code to reset reg_name if rst is high and set it to value otherwise
std::vector<std::string> reset_or_value(
    std::string reg_name,
    std::string reset_value,
    std::string value
);

// Sign extend the content of a register if the original size is smaller than the extended.
// Otherwise the original register is returned unmodified
std::string sign_extend(std::string reg, int original_size, int extended_size);


ExecutionType approximate_log2(ExecutionType input);
ExecutionType approximate_pow2(ExecutionType input);

#endif
