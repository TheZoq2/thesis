#ifndef H_GRAPH
#define H_GRAPH

#include "node.hpp"

void dump_graph(Rc<Node> root);
void dump_graph(std::string filename, Rc<Node> root, std::function<std::string(Node*)>);
void dump_bounds(Rc<Node> root);
std::vector<std::string> generate_graph(Rc<Node> root, std::function<std::string(Node*)>);


#endif
