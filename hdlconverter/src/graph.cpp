#include "graph.hpp"

#include <set>
#include <fstream>
#include <chrono>

std::vector<std::string> generate_graph(Rc<Node> root, std::function<std::string(Node*)> fn) {
    auto unique = root->unique_ancestors();

    std::vector<std::string> code;
    for(auto node : unique) {
        code = merge_code({code, node->generate_graph(fn)});
    }
    code = merge_code({code, root->generate_graph(fn)});

    return code;
}

void dump_graph(std::string filename, Rc<Node> root, std::function<std::string(Node*)> fn) {
    std::ofstream output(filename);
    output << "digraph G {";
    output << join(generate_graph(root, fn));
    output << "}";
}
void dump_graph(Rc<Node> root) {
    std::ofstream output("graph.gv");
    output << "digraph G {";
    output << join(generate_graph(root, [](Node* node){return node->display_value();}));
    output << "}";
}

void dump_bounds(Rc<Node> root) {
    auto fn = [](Node* node) {
        auto frac = dynamic_cast<FracNode*>(node);
        if(frac != nullptr) {
            return std::to_string(frac->min()) + " | " + std::to_string(frac->max());
        }
        else {
            return std::string("-");
        }
    };
    std::ofstream output("graph.gv");
    output << "digraph G {";
    output << join(generate_graph(root, fn));
    output << "}";

}
