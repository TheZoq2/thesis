#include "argparser.hpp"

#include <iostream>
#include <map>

std::map<std::string, RunType> RUN_TYPE_MAP = {
    {"execute", RunType::EXECUTE},
    {"codegen", RunType::CODEGEN},
    {"word_length", RunType::WORD_LENGTH},
    {"samples", RunType::RUN_SAMPLES},
};

RunType parse_options(int argc, char* argv[]) {
    if(argc != 2) {
        std::cerr << "Expected run type argument" << std::endl;
        exit(-1);
    }

    auto type_string = std::string(argv[1]);

    if(RUN_TYPE_MAP.find(type_string) != RUN_TYPE_MAP.end()) {
        return RUN_TYPE_MAP[type_string];
    }
    else {
        std::cerr << "Unexpected run type: " << type_string
            << " expected one of ";
        for(auto[key, _] : RUN_TYPE_MAP) {
            std::cerr << key << " ";
        }
        std::cerr << std::endl;
        exit(-1);
    }
}
