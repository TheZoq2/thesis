#include "lookup_table.hpp"

#include <sys/stat.h>

#include <fstream>

const std::string LUT_STORAGE_LOCATION = "luts";

////////////////////////////////////////////////////////////
// LUT impls
////////////////////////////////////////////////////////////

LookupTable::LookupTable(
    std::string name,
    std::vector<double> inputs,
    std::vector<double> values
)
    : name(name)
{
    if(inputs.size() != values.size()) {
        std::cout
            << "Error creating lookup table for "
            << name
            << " input and output vector size must be equal"
            << std::endl;
        exit(-1);
    }

    for(std::size_t i = 0; i < values.size(); ++i) {
        this->map[inputs[i]] = values[i];
    }


    mkdir(LUT_STORAGE_LOCATION.c_str(), S_IRWXU | S_IROTH | S_IXOTH);
    std::ofstream f(LUT_STORAGE_LOCATION + "/" + name + ".v");
    f << join(get_module());
}

Rc<LookupTable> LookupTable::create(std::string name, std::vector<double> values) {
    std::vector<double> inputs;
    for(std::size_t i = 0; i < values.size(); ++i) {
        inputs.push_back(i);
    }
    return create(name, inputs, values);
}
Rc<LookupTable> LookupTable::create(
    std::string name,
    std::vector<double> inputs,
    std::vector<double> values
) {
    return Rc<LookupTable>(new LookupTable(name, inputs, values));
}

std::string LookupTable::get_name() const {
    return name;
}

double LookupTable::min() const {
    return std::min_element(
        map.begin(),
        map.end(),
        [](auto x, auto y){return x.second < y.second;}
    )->second;
}
double LookupTable::max() const {
    return std::max_element(
        map.begin(),
        map.end(),
        [](auto x, auto y){return x.second < y.second;}
    )->second;
}
int LookupTable::size() const {
    return map.size();
}

double LookupTable::lookup(std::size_t index) const {
    return map.at(index);
}

// Helper class for the WordLengthVisitor
template<class T> struct always_false : std::false_type {};

std::vector<std::string> LookupTable::get_module() const {
    std::vector<std::string> case_branches;

    // Move the elements out of the map and into a vector to sort them
    // for more readable code
    std::vector<std::pair<double, double>> elems;
    for(auto elem : map) {
        elems.push_back(elem);
    }
    std::sort(elems.begin(), elems.end(), [](auto a, auto b){return a.first < b.first;});

    for(auto elem : elems) {
        case_branches.push_back(
            std::to_string(int(elem.first)) +
            ": result_buffer <= " + std::to_string(int(elem.second * pow(2, FIXPOINT))) + ";"
        );
    }

    case_branches.push_back("default: result_buffer <= 0;");

    return merge_code({
        {"module " + this->name},
        indent_lines(indent_lines({
            "#( parameter input_size = 32",
            ", parameter output_size = 32",
            ")"
        })),
        indent_lines(indent_lines({"( clk", ", addr", ", result", ");"})),
        indent_lines({
            verilog_variable(VarType::INPUT, "clk", 1),
            "input[input_size-1:0] addr;",
            "output[output_size-1:0] result;",
            "reg[output_size-1:0] result_buffer;",
            "assign result = result_buffer;",
        }),
        indent_lines(clocked_block(merge_code({
            // The sign bit is always one, so we can remove it. If we don't do this,
            // the synthesis tool gets sad and confused
            {"case(addr[input_size-2:0])"},
            indent_lines(case_branches),
            {"endcase"}
        }))),
        {"endmodule"}
    });
}



//////////////////////////////////////////////////////////////////////
// Lookup result impls
//////////////////////////////////////////////////////////////////////

LutResult::LutResult(Rc<LookupTable> lut, Rc<FracNode> index)
    : FracNode({index.ptr()}), index(index), lut(lut)
{}

Rc<LutResult> LutResult::create(
    Rc<LookupTable> lut,
    Rc<FracNode> index
) {
    return Rc<LutResult>(new LutResult(lut, index));
}

std::string LutResult::variable_name() const {
    return "lut_" + lut->get_name() + "_" + this->parent_register(index);
}

std::vector<std::string> LutResult::get_code(WordLength word_length) {
    auto integer_size = index->register_size(word_length) - fractional_bits(word_length);
    auto module_instance = lut->get_name()
        + "#(.input_size("
        + std::to_string(integer_size)
        + "), .output_size("
        + std::to_string(register_size(word_length))
        + "))";
    return {
        verilog_variable(VarType::WIRE, base_name(), register_size(word_length)),
        module_instance + " " + base_name() + "_lut(.clk(clk), .addr("
            + this->parent_register(index)
            + "[" + std::to_string(index->register_size(word_length) - 1) + ":"
            + std::to_string(fractional_bits(word_length))
            + "]), .result(" + base_name() + "));"
    };
}

double LutResult::min() const {
    return lut->min();
}
double LutResult::max() const {
    return lut->max();
}

ExecutionType LutResult::recalculate_value() {
    try {
        return lut->lookup((int) index->calculate_value());
    }
    catch(std::out_of_range e) {
        index->dump_graph();
        std::cout << "LUT went out of bounds, dumped graph" << std::endl;
        throw(e);
    }
}

std::vector<Rc<Node>> LutResult::get_parents() const {
    return {index};
}

std::string LutResult::graph_label_name() {return "lut";}

ErrorFnType LutResult::error_function() const {
    return [](double*){return 0;};
}
// TODO: Test
std::vector<Rc<FracNode>> LutResult::smaller_word_constraints() const {return {};}
