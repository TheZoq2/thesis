#ifndef H_NODE
#define H_NODE


#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <optional>
#include <functional>
#include <memory>
#include <set>
#include <variant>

#include "../MFixedPoint/include/MFixedPoint/FpF.hpp"

#include "config.hpp"
#include "util.hpp"
#include "rc.hpp"

using ErrorFnType = std::function<double(double* array_lengths)>;

struct FixedFixpoint {int position;};
using WordLength = std::variant<FixedFixpoint>;

class Node {
    public:
        Node(std::set<Node*> parents);
        virtual ~Node() = default;

        // Non-virtual functions
        int depth() const;
        void request_delay_slots(int amount);
        void traverse_depth_first(std::function<void(Node*)> fn);
        void override_name(std::string forced_name);
        std::optional<int> get_delay_slots() {return this->delay_slots;}
        std::string parent_register(Rc<Node> parent) const;
        std::string base_name();
        std::vector<std::string> generate_graph(std::function<std::string(Node*)> fn);
        std::string graph_identifier();
        std::set<Node*> unique_ancestors();
        // Convenience function that calls the non member dump_graph function
        // to avoid GDB issues
        void dump_graph();

        // Overrideable virtual functions
        virtual std::string pipeline_buffer_name(int n);
        virtual bool is_input() const {return false;}
        virtual std::vector<std::string> generate_pipeline_code(WordLength word_length);

        // Pure virtual functions
        virtual std::vector<std::string> get_code(WordLength word_length) = 0;
        virtual std::vector<Rc<Node>> get_parents() const = 0;
        virtual void mark_dirty() = 0;
        virtual std::string display_value() = 0;
        virtual std::string graph_label_name() = 0;
        virtual int register_size(WordLength word_length) const = 0;
        // Recalculate the value if it is dirty
        virtual void update_value() = 0;
    protected:
        virtual std::string variable_name() const = 0;

        std::optional<int> delay_slots = std::nullopt;
    private:
        std::optional<std::string> cached_name;
        std::optional<std::string> forced_name;

        std::set<Node*> ancestors;

        int depth_ = 0;

        int name_end;
};

// Dummy node with no behaviour but which can be used to collect a list of nodes,
// for example to generate a graph
class DummyNode : public Node {
    public:
        DummyNode(std::vector<Rc<Node>> parents);
        std::vector<std::string> get_code(WordLength word_length) override;
        std::vector<Rc<Node>> get_parents() const override;
        void mark_dirty() override;
        std::string display_value() override;
        std::string graph_label_name() override;
        int register_size(WordLength word_length) const override;
        // Recalculate the value if it is dirty
        void update_value() override;
    protected:
        std::string variable_name() const override;
    private:
        std::vector<Rc<Node>> parents;
};

class FracNode : public Node {
    public:
        FracNode(std::set<Node*> ancestors);

        virtual double min() const = 0;
        virtual double max() const = 0;


        ExecutionType  calculate_value();
        void mark_dirty() override;
        std::string display_value() override;
        void update_value() override;
        int register_size(WordLength word_length) const override;
        int fractional_bits(WordLength word_length) const;

        void set_array_index(std::size_t array_position);
        std::size_t get_array_index() const;
        virtual std::vector<Rc<FracNode>> smaller_word_constraints() const = 0;

        virtual ErrorFnType error_function() const = 0;

        // Total amount of bits needed to fully store this variable
        unsigned int total_bits();

        // TODO: replace fractional_bits with this once the calculation is correct
        std::optional<int> optimal_fractional_bits;
    protected:
        virtual ExecutionType recalculate_value() = 0;

        // Position of the size of this node in the array containing the
        // sizes of variables
        std::optional<std::size_t> array_position;

        double current_fractional_bits(double* fractional_bit_array) const;
        double default_truncation_error(
            std::vector<FracNode*> fractional_parents,
            double* fractional_bit_array
        ) const;
    private:
        std::optional<ExecutionType> previous_value;
};

class BoolNode : public Node {
public:
    BoolNode(std::set<Node*> ancestors);

    bool calculate_value();
    void mark_dirty() override;
    void update_value() override;
    int register_size(WordLength word_length) const override;

    std::string display_value() override;
protected:
    virtual bool recalculate_value() = 0;

    std::optional<bool> previous_value;
};

class Constant : public FracNode {
    public:
        static Rc<Constant> create(std::string name, const double value);

        const std::string name;
        const double value;

        std::vector<std::string> get_code(WordLength word_length) override;
        double min() const override {return value;}
        double max() const override {return value;}
        ErrorFnType error_function() const override;
        std::vector<Rc<FracNode>> smaller_word_constraints() const override;

        std::string pipeline_buffer_name(int n) override;
        std::string graph_label_name() override;
        std::vector<std::string> generate_pipeline_code(WordLength word_length) override;
    protected:
        std::string variable_name() const override {return name;};
        std::vector<Rc<Node>> get_parents() const override {return {};}
        ExecutionType recalculate_value() override;
    private:
        Constant(std::string name, const double value);
};


class Variable : public FracNode {
    public:
        static Rc<Variable> create(
            std::string name,
            const double min,
            const double max,
            const ExecutionType value
        );

        void set_value(double value);

        const std::string name;
        const double min_val;
        const double max_val;

        std::vector<std::string> get_code(WordLength word_length) override;
        double min() const override {return min_val;}
        double max() const override {return max_val;}
        ErrorFnType error_function() const override;
        std::vector<Rc<FracNode>> smaller_word_constraints() const override;

        std::string graph_label_name() override;
    protected:
        ExecutionType recalculate_value() override;
        std::string variable_name() const override {return name;};
        std::vector<Rc<Node>> get_parents() const override {return {};}
    private:
        Variable(std::string name, const double min, const double max, const ExecutionType value);

        ExecutionType value;
};

template<int MIN, int MAX>
class Input : public FracNode {
    public:
        static Rc<Input<MIN, MAX>> create( std::string name, ExecutionType value) {
            return Rc<Input<MIN, MAX>>(new Input(name, value));
        }

        const std::string name;

        std::vector<std::string> get_code(WordLength word_length) override {
            auto register_end = std::to_string(register_size(word_length) - 1);
            return merge_code({
                {"reg[" + register_end + ":0] " + base_name() + ";"},
                {"input[" + register_end + ":0] " + base_name() + "_non_reg;"},
                clocked_block({base_name() + " <= " + base_name() + "_non_reg;"})
            });
        }
        double min() const override {return MIN;}
        double max() const override {return MAX;}
        bool is_input() const override {return true;}
        ErrorFnType error_function() const override {
            return [this](double* in){return pow(2, -in[array_position.value()]);};
        }
        std::vector<Rc<FracNode>> smaller_word_constraints() const override {
            return {};
        }

        void set_value(ExecutionType value) {
            this->value = value;
        }

        std::string graph_label_name() override {
            return "in " + base_name();
        }
    protected:
        ExecutionType recalculate_value() override {
            return this->value;
        }

        std::string variable_name() const override {return name;};

        std::vector<Rc<Node>> get_parents() const override {return {};}
    private:
        Input(std::string name, ExecutionType value) : FracNode({}), name(name), value(value) {}

        ExecutionType value;
};

/////////////////////////////////////////////////////
// Function-like nodes
/////////////////////////////////////////////////////

class Abs : public FracNode {
    public:
        static Rc<Abs> create(Rc<FracNode> input);

        std::vector<std::string> get_code(WordLength word_length) override;
        double min() const override;
        double max() const override;
        ErrorFnType error_function() const override;
        std::vector<Rc<FracNode>> smaller_word_constraints() const override;

        std::string graph_label_name() override;
    protected:
        std::string variable_name() const override;
        std::vector<Rc<Node>> get_parents() const override;
        ExecutionType recalculate_value() override;
    private:
        Abs(Rc<FracNode> other);

        Rc<FracNode> input;
};

class Sqrt : public FracNode {
    public:
        static Rc<Sqrt> create(Rc<FracNode> input);

        // TODO: Decide what to do when inputs can be < 0
        double min() const override;
        double max() const override;
        ErrorFnType error_function() const override;
        std::vector<Rc<FracNode>> smaller_word_constraints() const override;

        // TODO: Implement and test
        std::vector<std::string> get_code(WordLength word_length) override;
        std::vector<Rc<Node>> get_parents() const override;

        std::string graph_label_name() override;
    protected:
        ExecutionType recalculate_value() override;
    protected:
        // TODO: Test
        std::string variable_name() const override;

        Sqrt(Rc<FracNode> input);
        Rc<FracNode> input;
};

class Truncate : public FracNode {
    public:
        static Rc<Truncate> create(Rc<FracNode> input);

        double min() const override;
        double max() const override;
        // TODO: Handle bit lengths < 0
        ErrorFnType error_function() const override;
        std::vector<Rc<FracNode>> smaller_word_constraints() const override;

        // TODO: Implement and test
        std::vector<std::string> get_code(WordLength word_length) override;
        std::vector<Rc<Node>> get_parents() const override;

        std::string graph_label_name() override;
    protected:
        ExecutionType recalculate_value() override;
        std::string variable_name() const override;

        Truncate(Rc<FracNode> input);
        Rc<FracNode> input;
};

////////////////////////////////////////////////////////////////////////////////
// Boolean nodes
////////////////////////////////////////////////////////////////////////////////


class BoolConst : public BoolNode {
    public:
        const static Rc<BoolConst> True;
        const static Rc<BoolConst> False;

        std::vector<std::string> get_code(WordLength word_length) override;

        // Not tested because it's too trivial
        std::vector<Rc<Node>> get_parents() const override {return {};}

        std::string pipeline_buffer_name(int) override { return base_name(); }
        std::vector<std::string> generate_pipeline_code(WordLength) override {return {};}

        std::string graph_label_name() override;
    protected:
        bool recalculate_value() override {return value;}
        static Rc<BoolConst> create(bool value);
        BoolConst(bool value);

        std::string variable_name() const override;

        bool value;
};

/////////////////////////////////////////////////////
// Fractional operators
/////////////////////////////////////////////////////

class USub : public FracNode {
    public:
        static Rc<USub> create(Rc<FracNode> input);

        std::vector<std::string> get_code(WordLength word_length) override;
        std::vector<Rc<Node>> get_parents() const override;
        double min() const override;
        double max() const override;
        ErrorFnType error_function() const override;
        std::vector<Rc<FracNode>> smaller_word_constraints() const override;


        std::string graph_label_name() override;
    protected:
        ExecutionType recalculate_value() override;
    protected:
        std::string variable_name() const override;

        USub(Rc<FracNode> input);
        Rc<FracNode> input;
};

class BinaryOperator : public FracNode {
    public:
        std::vector<Rc<Node>> get_parents() const override;

        std::string graph_label_name() override;
    protected:
        std::string variable_name() const override;

        virtual std::string operator_symbol() const = 0;
        virtual std::string operator_name() const = 0;

        Rc<FracNode> lhs;
        Rc<FracNode> rhs;

        BinaryOperator(Rc<FracNode> lhs, Rc<FracNode> rhs);
};

#define BINOP(Name) class Name : public BinaryOperator { \
    public: \
        Name(Rc<FracNode> rhs, Rc<FracNode> lhs); \
        double min() const override; \
        double max() const override; \
        ErrorFnType error_function() const override; \
        std::string operator_symbol() const override; \
        std::string operator_name() const override; \
        std::vector<std::string> get_code(WordLength word_length) override; \
        std::vector<Rc<FracNode>> smaller_word_constraints() const override; \
    protected: \
        ExecutionType recalculate_value() override; \
}

BINOP(Add);
BINOP(Sub);
BINOP(Mul);
// TODO: Test error function
BINOP(Div);

/////////////////////////////////////////////////////
// Boolean operators
/////////////////////////////////////////////////////

class Not : public BoolNode {
    public:
        static Rc<Not> create(Rc<BoolNode> input);

        std::vector<std::string> get_code(WordLength word_length) override;
        // TODO: Test
        std::vector<Rc<Node>> get_parents() const override;

        std::string graph_label_name() override;
    protected:
        bool recalculate_value() override;
    protected:
        std::string variable_name() const override;

        Not(Rc<BoolNode> input);

        Rc<BoolNode> input;
};

template<typename T>
class BooleanBinaryOperator : public BoolNode {
    public:
        std::vector<Rc<Node>> get_parents() const override {
            return {lhs, rhs};
        }
        std::string graph_label_name() override {
            return operator_symbol();
        }
    protected:
        std::string variable_name() const override {
            return operator_name() + "_" + this->parent_register(lhs)
                + "_" + this->parent_register(rhs);
        }
        virtual std::string operator_symbol() const = 0;
        virtual std::string operator_name() const = 0;

        Rc<T> lhs;
        Rc<T> rhs;

        BooleanBinaryOperator(Rc<T> lhs, Rc<T> rhs)
            : BoolNode({lhs.ptr(), rhs.ptr()}), lhs(lhs), rhs(rhs)
        {}
};

#define BOOLBINOP(NAME, INPUT_TYPE) class NAME : public BooleanBinaryOperator<INPUT_TYPE> { \
    public: \
        NAME(Rc<INPUT_TYPE> rhs, Rc<INPUT_TYPE> lhs); \
        std::vector<std::string> get_code(WordLength word_length) override;\
        std::string operator_symbol() const override; \
        std::string operator_name() const override; \
    protected: \
        bool recalculate_value() override; \
}

BOOLBINOP(LessThan, FracNode);
BOOLBINOP(GreaterThan, FracNode);
BOOLBINOP(LessThanOrEq, FracNode);
BOOLBINOP(GreaterThanOrEq, FracNode);
BOOLBINOP(Or, BoolNode);


//////////////////////////////////////////////////////////////////////
// Operator overloads
//////////////////////////////////////////////////////////////////////

Rc<Add> operator+(Rc<FracNode> lhs, Rc<FracNode> rhs);
Rc<Sub> operator-(Rc<FracNode> lhs, Rc<FracNode> rhs);
Rc<Mul> operator*(Rc<FracNode> lhs, Rc<FracNode> rhs);
Rc<Div> operator/(Rc<FracNode> lhs, Rc<FracNode> rhs);
Rc<USub> operator-(Rc<FracNode> input);

Rc<LessThan> operator<(Rc<FracNode> lhs, Rc<FracNode> rhs);
Rc<GreaterThan> operator>(Rc<FracNode> lhs, Rc<FracNode> rhs);
Rc<LessThanOrEq> operator<=(Rc<FracNode> lhs, Rc<FracNode> rhs);
Rc<GreaterThanOrEq> operator>=(Rc<FracNode> lhs, Rc<FracNode> rhs);

Rc<Or> operator||(Rc<BoolNode> lhs, Rc<BoolNode> rhs);

Rc<Not> operator!(Rc<BoolNode> input);


//////////////////////////////////////////////////////////////////////
// If-statement
//////////////////////////////////////////////////////////////////////

template<class T>
class If : public T {
    public:
        std::vector<std::string> get_code(WordLength word_length) override {
            auto true_register = sign_extend(
                this->parent_register(on_true),
                on_true->register_size(word_length),
                this->register_size(word_length)
            );
            auto false_register = sign_extend(
                this->parent_register(on_false),
                on_false->register_size(word_length),
                this->register_size(word_length)
            );
            // If we need to sign extend the true branch
            return merge_code({
                {register_code(this->base_name(), this->register_size(word_length))},
                clocked_block(if_statement(
                    this->parent_register(condition),
                    {this->base_name() + " <= " + true_register + ";"},
                    {this->base_name() + " <= " + false_register + ";"}
                ))
            });
        }
        std::vector<Rc<Node>> get_parents() const override {
            return {condition, on_true, on_false};
        }
        std::string graph_label_name() override {
            return "if";
        }
    protected:
        std::string variable_name() const override {
            return "if_" + condition->base_name() + "_then_" +
                on_true->base_name() + "_else_" + on_false->base_name();
        }

        Rc<BoolNode> condition;
        Rc<T> on_true;
        Rc<T> on_false;

        If(Rc<BoolNode> condition, Rc<T> on_true, Rc<T> on_false)
            : T({condition.ptr(), on_true.ptr(), on_false.ptr()})
            , condition(condition)
            , on_true(on_true)
            , on_false(on_false)
        {
            static_assert(std::is_base_of<Node, T>::value, "If-result must be a node");
        }
};

class IfFractional : public If<FracNode> {
    public:
        static Rc<IfFractional> create(
            Rc<BoolNode> condition,
            Rc<FracNode> on_true,
            Rc<FracNode> on_false
        ) {
            return Rc<IfFractional>(new IfFractional(condition, on_true, on_false));
        }

        double min() const override {
            if(forced_min.has_value()) {
                return forced_min.value();
            }
            return std::min(on_true->min(), on_false->min());
        }
        double max() const override {
            if(forced_max.has_value()) {
                return forced_max.value();
            }
            return std::max(on_true->max(), on_false->max());
        }
        ErrorFnType error_function() const override {
            return [this](double* in) {
                // return on_true->error_function()(in) + on_false->error_function()(in);
                return std::max(on_true->error_function()(in), on_false->error_function()(in))
                    + default_truncation_error({on_true.ptr(), on_false.ptr()}, in);
            };
        }
        std::vector<Rc<FracNode>> smaller_word_constraints() const override {
            return {on_true, on_false};
        }


        // Change the min value from the automatically calculated value to this.
        // Useful if an if-statement clearly changes the bounds. But incorrect use can
        // lead to over or underflows
        void _override_min(double min) {forced_min = min;}
        // Change the max value from the automatically calculated value to this.
        // Useful if an if-statement clearly changes the bounds. But incorrect use can
        // lead to over or underflows
        void _override_max(double max) {forced_max = max;}
    protected:
        ExecutionType recalculate_value() override {
            if(condition->calculate_value()) {
                return on_true->calculate_value();
            }
            return on_false->calculate_value();
        }
    private:
        IfFractional(
            Rc<BoolNode> condition,
            Rc<FracNode> on_true,
            Rc<FracNode> on_false
        )
            : If<FracNode>(condition, on_true, on_false)
        {}

        std::optional<double> forced_min;
        std::optional<double> forced_max;
};
class IfBoolean : public If<BoolNode> {
    public:
        static Rc<IfBoolean> create(
            Rc<BoolNode> condition,
            Rc<BoolNode> on_true,
            Rc<BoolNode> on_false
        ) {
            return Rc<IfBoolean>(new IfBoolean(condition, on_true, on_false));
        }

    protected:
        bool recalculate_value() override {
            if(condition->calculate_value()) {
                return on_true->calculate_value();
            }
            return on_false->calculate_value();
        }
    private:
        IfBoolean(
            Rc<BoolNode> condition,
            Rc<BoolNode> on_true,
            Rc<BoolNode> on_false
        )
            : If<BoolNode>(condition, on_true, on_false)
        {}
};

Rc<IfFractional> _if(Rc<BoolNode> condition, Rc<FracNode> on_true, Rc<FracNode> on_false);
Rc<IfBoolean> _if(Rc<BoolNode> condition, Rc<BoolNode> on_true, Rc<BoolNode> on_false);

Rc<IfFractional> min(Rc<FracNode> lhs, Rc<FracNode> rhs);
Rc<IfFractional> max(Rc<FracNode> lhs, Rc<FracNode> rhs);

// Free functions

void assign_delay_slots(std::vector<Rc<Node>> nodes, int depth);

#endif
