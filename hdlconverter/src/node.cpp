#include "node.hpp"

#include "graph.hpp"

#include <iostream>
#include <sstream>

/// Free functions


/*
  Computes and assigns the required delay slots for each node in the supplied vector
  given that their values are used at `depth`
*/
void assign_delay_slots(std::vector<Rc<Node>> nodes, int depth) {
    std::set<Node*> unique_nodes;
    for(auto node : nodes) {
        unique_nodes.merge(node->unique_ancestors());
        unique_nodes.insert(node.ptr());

        node->request_delay_slots(depth - node->depth() - 1);
    }

    for(auto node : unique_nodes) {
        auto depth = node->depth();
        for(auto parent : node->get_parents()) {
            parent->request_delay_slots(depth - parent->depth() - 1);
        }
    }
}


////////////////////////////////////////////////////////////////////////////////

// Nodes

Node::Node(std::set<Node*> parents) {
    this->ancestors = parents;
    for(auto ancestor : ancestors) {
        depth_ = std::max(depth_, ancestor->depth() + 1);
        this->ancestors.merge(ancestor->unique_ancestors());
        this->ancestors.insert(ancestor);
    }

    name_end = rand() % (1 << 16);
}


/*
  Sets the required delay slots of the node to at least `amount`
*/
void Node::request_delay_slots(int amount) {
    this->delay_slots = std::max(this->delay_slots.value_or(0), amount);
}

/*
  Returns the name of the nth pipeline register for this variable
*/
std::string Node::pipeline_buffer_name(int n) {
    if (n == 0) {
        return base_name();
    }
    return base_name() + "_" + std::to_string(n);
}

/*
  Generates the code for the pipeline registers required by this node.

  This function will throw an exception if `delay_slots` has not been set before calling.
*/
std::vector<std::string> Node::generate_pipeline_code(WordLength word_length) {
    // Ensure that we know how many delay slots will be used
    if (this->delay_slots == std::nullopt) {
        throw "Generate pipeline code was run without delay slots being assigned";
        exit(-1);
    }
    // Unwrap the value since we know it's not nullopt
    int delay_slots = this->delay_slots.value();
    // If no delay slots are required, we don't want to generate surrounding codej
    if(delay_slots == 0) {
        return {};
    }
    std::vector<std::string> result = {};
    // generate the registers
    auto last_var = base_name();
    for(int i = 0; i < delay_slots; ++i) {
        auto buffer_name = pipeline_buffer_name(i+1);
        result.push_back(register_code(buffer_name, register_size(word_length)));
        last_var = buffer_name;
    }
    // Generate the assignment code
    std::vector<std::string> assignment = {};
    last_var = base_name();
    for(int i = 0; i < delay_slots; ++i) {
        auto buffer_name = pipeline_buffer_name(i+1);
        assignment.push_back(buffer_name + " <= " + last_var + ";");
        last_var = buffer_name;
    }
    auto assignment_block = clocked_block(assignment);
    result.insert(result.end(), assignment_block.begin(), assignment_block.end());
    return result;
}

/*
  Calls fn on the nodes in this tree in a depth first order, i.e. child nodes
  are evaluated before parents. The order of evaluation for child nodes is the order
  they appear in get_parents
*/
void Node::traverse_depth_first(std::function<void(Node*)> fn) {
    for(auto child : get_parents()) {
        child->traverse_depth_first(fn);
    }
    fn(this);
}


int Node::depth() const {
    return depth_;
}

std::string Node::parent_register(Rc<Node> parent) const {
    return parent->pipeline_buffer_name(depth() - parent->depth() - 1);
}

void Node::override_name(std::string forced_name) {
    this->forced_name = forced_name;
}

std::string Node::base_name() {
    if(forced_name.has_value()) {
        return forced_name.value();
    }
    else if(cached_name.has_value()){
        return cached_name.value();
    }
    else {
        std::string name;
#ifdef RANDOM_NAME_SUFFIX
        if(!is_input()) {
            std::stringstream stream;
            stream << variable_name() << "_" << std::hex << name_end;
            name = stream.str();
        }
        else {
            name = variable_name();
        }
#else
        name = variable_name();
#endif
        if(name.size() > 40) {
            std::size_t hash = std::hash<std::string>{}(name);
            // +name.size() because apparently hash collisions between simiarly lengthed
            // vars are very uncommmon
            cached_name = "h_" + std::to_string(hash) + std::to_string(name.size());
        }
        else {
            cached_name = name;
        }
        return cached_name.value();
    }
}

std::vector<std::string> Node::generate_graph(std::function<std::string(Node*)> fn) {
    std::string parent_list = "";
    for(auto parent : get_parents()) {
        parent_list += parent->graph_identifier() + ",";
    }
    // Remove trailing ,
    if(!parent_list.empty()) {
        parent_list.pop_back();
    }
    auto label = forced_name.value_or(graph_label_name());
    return merge_code({
        {graph_identifier() + " [label=\"" + label + "\\n" + fn(this) + "\"];"},
        {graph_identifier() + " -> {" + parent_list + "};"},
    });
}

std::string Node::graph_identifier() {
    return base_name();
}

std::set<Node*> Node::unique_ancestors() {
    // std::set<Node*> result;

    // this->traverse_depth_first([&result](auto node){result.insert(node);});
    // return result;
    return ancestors;
}

void Node::dump_graph() {
    ::dump_graph(this);
}

////////////////////////////////////////////////////////////////////////////////
// Dummy node

std::set<Node*> raw_pointers(std::vector<Rc<Node>> in) {
    std::set<Node*> result;
    for(auto node : in) {
        result.insert(node.ptr());
    }
    return result;
}

DummyNode::DummyNode(std::vector<Rc<Node>> parents) : Node(raw_pointers(parents)) {}
std::vector<std::string> DummyNode::get_code(WordLength) {throw "Dummy nodes cant have code";}
std::vector<Rc<Node>> DummyNode::get_parents() const {return parents;}
void DummyNode::mark_dirty() {};
std::string DummyNode::display_value() {return "dummy";}
std::string DummyNode::graph_label_name() {return "dummy";}
void DummyNode::update_value() {}
std::string DummyNode::variable_name() const {return "dummy";}
int DummyNode::register_size(WordLength) const {return 0;}


////////////////////////////////////////////////////////////////////////////////
// Abstract nodes

FracNode::FracNode(std::set<Node*> ancestors) : Node(ancestors) {}

ExecutionType FracNode::calculate_value() {
    if (!previous_value.has_value()) {
        previous_value = recalculate_value();
    }
    // return recalculate_value();
    return previous_value.value();
}
void FracNode::mark_dirty() {
    previous_value = std::nullopt;
}
void FracNode::update_value() {
    this->calculate_value();
}
std::string FracNode::display_value() {
    return std::to_string((double) calculate_value());
}
void FracNode::set_array_index(std::size_t array_position) {
    this->array_position = array_position;
}
std::size_t FracNode::get_array_index() const {
    return this->array_position.value();
}
double FracNode::current_fractional_bits(double* fractional_bit_array) const {
    return fractional_bit_array[array_position.value()];
}
double FracNode::default_truncation_error(
    std::vector<FracNode*> parents,
    double* bit_counts
) const {
    if(parents.size() == 0) {
        return pow(2, -this->current_fractional_bits(bit_counts));
    }
    else {
        // double result = 0;
        double highest_bit_count = -1000;
        for(auto node : parents) {
            if(node->current_fractional_bits(bit_counts) > highest_bit_count) {
                highest_bit_count = node->current_fractional_bits(bit_counts);
            }
        }
        return pow(2, -this->current_fractional_bits(bit_counts))
                - pow(2, -highest_bit_count);
    }
}
unsigned int FracNode::total_bits() {
    // 1+ for sign bit
    auto integer_bits = 1 + ceil(log2(std::max(std::abs(min()), std::abs(max()))));
    return integer_bits + optimal_fractional_bits.value();
}
int FracNode::register_size(WordLength word_length) const {
    auto integer_bits = ceil(log2(1+std::max(std::abs(min()), std::abs(max()))));
    if(min() == 0 && max() == 0) {
        integer_bits = 0;
    }
    auto fractional_bits = this->fractional_bits(word_length);
    auto sign_bit = 1;
    return fractional_bits + integer_bits + sign_bit;
}
template<class T> struct always_false : std::false_type {};
int FracNode::fractional_bits(WordLength word_length) const {
    // see https://en.cppreference.com/w/cpp/utility/variant/visit for explanation
    return std::visit([](auto&& arg) {
        using T = std::decay_t<decltype(arg)>;
        if constexpr (std::is_same_v<T, FixedFixpoint>) {
            return arg.position;
        }
        else {
            static_assert(always_false<T>::value, "non-exhaustive visitor!");
        }
    }, word_length);
}


// Bool nodes


BoolNode::BoolNode(std::set<Node*> ancestors) : Node(ancestors) {}
bool BoolNode::calculate_value() {
    if (!previous_value.has_value()) {
        previous_value = recalculate_value();
    }
    // return recalculate_value();
    return previous_value.value();
}
void BoolNode::mark_dirty() {
    previous_value = std::nullopt;
}
void BoolNode::update_value() {
    this->calculate_value();
}

std::string BoolNode::display_value() {
    return std::to_string(calculate_value());
}

int BoolNode::register_size(WordLength) const {
    return 1;
}

////////////////////////////////////////////////////////////////////////////////
// Constants

Constant::Constant(std::string name, const double value)
    : FracNode({}), name(name), value(value)
{}

Rc<Constant> Constant::create(std::string name, const double value) {
    return Rc<Constant>(new Constant(name, value));
}

std::vector<std::string> Constant::get_code(WordLength word_length) {
    auto fractional_bits = this->fractional_bits(word_length);
    auto fixpoint_value = int(value * pow(2, fractional_bits));
    auto register_end = std::to_string(this->register_size(word_length) - 1);
    return {
        "localparam[" + register_end + ":0] " + base_name()
            + " = " + std::to_string(fixpoint_value) + ";"
    };
}

std::string Constant::pipeline_buffer_name(int) {
    return base_name();
}

ExecutionType Constant::recalculate_value() {
    return value;
}

std::string Constant::graph_label_name() {
    return "const " + base_name();
}

std::vector<std::string> Constant::generate_pipeline_code(WordLength) {
    return {};
}

ErrorFnType Constant::error_function() const {
    return [this](double* in){return pow(2, -in[array_position.value()]);};
}
std::vector<Rc<FracNode>> Constant::smaller_word_constraints() const {
    return {};
}


////////////////////////////////////////////////////////////////////////////////

// Variables

Variable::Variable(std::string name, const double min, const double max, ExecutionType value)
    : FracNode({}), name(name), min_val(min), max_val(max), value(value)
{}

Rc<Variable> Variable::create(
    std::string name,
    const double min,
    const double max,
    ExecutionType value
) {
    return Rc<Variable>(new Variable(name, min, max, value));
}

std::vector<std::string> Variable::get_code(WordLength word_length) {
    return {register_code(base_name(), register_size(word_length))};
}

ExecutionType Variable::recalculate_value() {
    return this->value;
}

void Variable::set_value(double value) {
    this->value = value;
}

std::string Variable::graph_label_name() {
    return "var " + base_name();
}

ErrorFnType Variable::error_function() const {
    return [this](double* in){return pow(2, -in[this->array_position.value()]);};
}
std::vector<Rc<FracNode>> Variable::smaller_word_constraints() const {
    return {};
}

////////////////////////////////////////////////////////////////////////////////

// Absolute value

Abs::Abs(Rc<FracNode> input)
    : FracNode({input.ptr()}), input(input)
{}

Rc<Abs> Abs::create(Rc<FracNode> input) {return Rc<Abs>(new Abs(input));}

std::vector<std::string> Abs::get_code(WordLength word_length) {
    return merge_code({
        {register_code(base_name(), register_size(word_length))},
        clocked_block(if_statement(
            {"$signed(" + this->parent_register(input) + ") < 0"},
            {base_name() + " <= -" + this->parent_register(input) + ";"},
            {base_name() + " <= " + this->parent_register(input) + ";"}
        ))
    });
}


std::string Abs::variable_name() const {return "abs_" + input->base_name();}

// TODO: Update bounds for cases where input bounds are on the same side of 0
// I.e. -100 < x < -50 -> 50 < y < 100, not 0 < y
double Abs::min() const {return 0;}
double Abs::max() const {return std::max(std::abs(input->min()), std::abs(input->max()));}

std::vector<Rc<Node>> Abs::get_parents() const {return {this->input};}

ExecutionType Abs::recalculate_value() {
    // return abs(this->input->calculate_value());
    // Fixpoint lib does not support abs
    auto input = this->input->calculate_value();
    if(input < (ExecutionType) 0) {
        return -input;
    }
    return input;
}

std::string Abs::graph_label_name() {return "abs";}

ErrorFnType Abs::error_function() const {
    return [this](double*in){
        return this->input->error_function()(in)
            + default_truncation_error({input.ptr()}, in);
    };
}
std::vector<Rc<FracNode>> Abs::smaller_word_constraints() const {return {this->input};}


// Sqrt

Sqrt::Sqrt(Rc<FracNode> input) : FracNode({input.ptr()}), input(input) {}
Rc<Sqrt> Sqrt::create(Rc<FracNode> input) {
    return Rc<Sqrt>(new Sqrt(input));
}
double Sqrt::min() const {return sqrt(input->min());}
double Sqrt::max() const {return sqrt(input->max());}
ExecutionType Sqrt::recalculate_value() {
#ifdef APPROXIMATE_SQRT
    #error "Approximate sqrt not implemented"
    return 0;
#else
    // TODO: Check if there is a sqrt function in the fp lib
    if(input->calculate_value() >= (ExecutionType) 0) {
        return sqrt((double)input->calculate_value());
    }
    else {
        std::cout << "Warning: Computing square root of -x, returning 0" << std::endl;
        return 0;
    }
#endif
};
std::vector<std::string> Sqrt::get_code(WordLength word_length) {
    auto input_size = std::to_string(input->register_size(word_length));
    auto output_size_str = std::to_string(register_size(word_length));
#ifndef PIPELINED_SQRT
    auto module_instance = std::string("sqrt#(.input_size(") + input_size
        + "), .output_size(" + output_size_str + ")) ";
#else
    auto module_instance = std::string("sqrt_pipelined#(.input_size(") + input_size
        + "), .output_size(" + output_size_str + ")) ";
#endif
    return merge_code({
        {verilog_variable(VarType::WIRE, base_name(), register_size(word_length))},
        {module_instance + base_name() + "_mod(clk, " + input->base_name() + ", "
            + base_name() + ");"}
    });
    return {verilog_variable(VarType::WIRE, base_name(), register_size(word_length))};
}
std::vector<Rc<Node>> Sqrt::get_parents() const {return {input};}
std::string Sqrt::variable_name() const {return "sqrt_" + parent_register(input);}
std::string Sqrt::graph_label_name() {return "sqrt";}
ErrorFnType Sqrt::error_function() const {
    return [this](double* in) {
        return pow(2, -in[array_position.value()]);
    };
}
std::vector<Rc<FracNode>> Sqrt::smaller_word_constraints() const {return {this->input};}


// Truncation
Truncate::Truncate(Rc<FracNode> input) : FracNode({input.ptr()}), input(input) {}
Rc<Truncate> Truncate::create(Rc<FracNode> input) {
    return Rc<Truncate>(new Truncate(input));
}
double Truncate::min() const {return (int)(input->min());}
double Truncate::max() const {return (int)(input->max());}
std::vector<std::string> Truncate::get_code(WordLength word_length) {
    auto output_fixpoint = std::to_string(this->fractional_bits(word_length));
    auto input_fixpoint = std::to_string(input->fractional_bits(word_length));
    auto input_end = std::to_string(input->register_size(word_length) - 1);
    return merge_code({
        {register_code(base_name(), register_size(word_length))},
        clocked_block({base_name() + " <= " + "{" + input->base_name()
                + "[" + input_end + ":" + input_fixpoint + "], " + output_fixpoint + "'b0};"})
    });
}
std::vector<Rc<Node>> Truncate::get_parents() const {return {input};}
std::string Truncate::graph_label_name() {return "truncate";}
ExecutionType Truncate::recalculate_value() {return (int)input->calculate_value();}
std::string Truncate::variable_name() const {return "truncate_" + input->base_name();}
ErrorFnType Truncate::error_function() const {return [](double*){return 0;};}
std::vector<Rc<FracNode>> Truncate::smaller_word_constraints() const {return {};}


////////////////////////////////////////////////////////////////////////////////

// Unary operators

USub::USub(Rc<FracNode> input) : FracNode({input.ptr()}), input(input) {};
Rc<USub> USub::create(Rc<FracNode> input) {
    return Rc<USub>(new USub(input));
}
std::vector<std::string> USub::get_code(WordLength word_length) {
    return merge_code({
        {register_code(this->base_name(), register_size(word_length))},
        clocked_block({base_name() + " <= -" + parent_register(input) + ";"})
    });
}
std::vector<Rc<Node>> USub::get_parents() const {return {input};}
double USub::min() const {return -input->max();}
double USub::max() const {return -input->min();}
ExecutionType USub::recalculate_value() {return -input->calculate_value();}
std::string USub::variable_name() const {
    return "usub_" + input->base_name();
}
std::string USub::graph_label_name() {return "-";}
ErrorFnType USub::error_function() const {
    return [this](double* in){
        return this->input->error_function()(in)
            + default_truncation_error({input.ptr()}, in);
    };
}
std::vector<Rc<FracNode>> USub::smaller_word_constraints() const {return {this->input};}

Rc<USub> operator-(Rc<FracNode> input) {return USub::create(input);}

// Binary operators

BinaryOperator::BinaryOperator(Rc<FracNode> lhs, Rc<FracNode> rhs)
    : FracNode({lhs.ptr(), rhs.ptr()}), lhs(lhs), rhs(rhs)
{}
std::string BinaryOperator::variable_name() const {
    return operator_name() + "_" + lhs->base_name() + "_" + rhs->base_name();
}
std::vector<Rc<Node>> BinaryOperator::get_parents() const {
    return {this->lhs, this->rhs};
}
std::string BinaryOperator::graph_label_name() {return operator_symbol();}



#define BINOP_BOILERPLATE(CLASS, SYMBOL, NAME, OP, CALC_OP) \
    CLASS::CLASS(Rc<FracNode> lhs, Rc<FracNode> rhs) \
        : BinaryOperator(lhs, rhs) \
    {} \
    std::string CLASS::operator_symbol() const {return SYMBOL;} \
    std::string CLASS::operator_name() const {return NAME;} \
    Rc<CLASS> OP(Rc<FracNode> lhs, Rc<FracNode> rhs) { \
        return Rc<CLASS>(new CLASS(lhs, rhs)); \
    } \
    ExecutionType CLASS::recalculate_value() { \
        return this->lhs->calculate_value() CALC_OP this->rhs->calculate_value(); \
    }

#define BINOP_DEFAULT_CODE(CLASS) \
    std::vector<std::string> CLASS::get_code(WordLength word_length) { \
        auto lhs_name = this->parent_register(lhs); \
        auto rhs_name = this->parent_register(rhs); \
        return merge_code({ \
            { \
                register_code(base_name(), register_size(word_length)), \
            }, \
            clocked_block( \
                { base_name() + " <= $signed(" + lhs_name + ")" + operator_symbol() + "$signed(" + rhs_name + ");"} \
            ) \
        }); \
    } \

double Add::min() const {return rhs->min() + lhs->min();}
double Add::max() const {return rhs->max() + lhs->max();}
BINOP_BOILERPLATE(Add, "+", "add", operator+, +);
BINOP_DEFAULT_CODE(Add);
ErrorFnType Add::error_function() const {
    return [this](double* in){
        return lhs->error_function()(in)
             + rhs->error_function()(in)
             + this->default_truncation_error({lhs.ptr(), rhs.ptr()}, in);
    };
}
// TODO: Test
std::vector<Rc<FracNode>> Add::smaller_word_constraints() const {
    return {lhs, rhs};
}

double Sub::min() const {return lhs->min() - rhs->max();}
double Sub::max() const {return lhs->max() - rhs->min();}
BINOP_BOILERPLATE(Sub, "-", "sub", operator-, -);
BINOP_DEFAULT_CODE(Sub);
// TODO: Test
ErrorFnType Sub::error_function() const {
    return [this](double* in){
        return lhs->error_function()(in)
             + rhs->error_function()(in)
             + this->default_truncation_error({lhs.ptr(), rhs.ptr()}, in);
    };
}
// TODO: Test
std::vector<Rc<FracNode>> Sub::smaller_word_constraints() const {return {};}

double Mul::min() const {return lhs->min() * rhs->min();}
double Mul::max() const {return lhs->max() * rhs->max();}
BINOP_BOILERPLATE(Mul, "*", "mul", operator*, *);
std::vector<std::string> Mul::get_code(WordLength word_length) {
    auto lhs_name = this->parent_register(lhs);
    auto rhs_name = this->parent_register(rhs);

    auto buf_name = base_name() + "_buf";

    auto register_size = this->register_size(word_length);
    auto buffer_size = lhs->register_size(word_length) + rhs->register_size(word_length);
    auto fractional_bits = this->fractional_bits(word_length);
    auto output_start = std::to_string(fractional_bits);
    auto output_end = std::to_string(fractional_bits + register_size - 1);

    return merge_code({
        { register_code(base_name(), register_size)
        , verilog_variable(VarType::WIRE, buf_name, buffer_size)
        },
        { "assign " + buf_name + " = $signed(" + lhs_name + ")*$signed(" + rhs_name + ");"},
        clocked_block({
            base_name() + " <= " + buf_name + "[" + output_end + ":" + output_start + "]" + ";"
        })
    });
}
ErrorFnType Mul::error_function() const {
    return [this](double* in) {
        return lhs->max()*rhs->error_function()(in)
             + rhs->max()*lhs->error_function()(in)
             + pow(2, -current_fractional_bits(in));
    };
}
// TODO: Test
std::vector<Rc<FracNode>> Mul::smaller_word_constraints() const {return {};}


double Div::min() const {return lhs->min() / rhs->max();}
double Div::max() const {return lhs->max() / rhs->min();}
BINOP_BOILERPLATE(Div, "/", "div", operator/, /);
std::vector<std::string> Div::get_code(WordLength word_length) {
    auto lhs_name = this->parent_register(lhs);
    auto rhs_name = this->parent_register(rhs);

    auto buffer_name = base_name() + "_buffer";

    int fractional_bits = this->fractional_bits(word_length);
    int register_size = this->register_size(word_length);
    int buffer_size = lhs->register_size(word_length) + lhs->fractional_bits(word_length);

#ifndef FAKE_DIVISION_FOLDING
    return merge_code({
        { register_code(base_name(), register_size)
        , verilog_variable(VarType::WIRE, buffer_name, buffer_size)
        , "assign " + buffer_name + " = $signed({" + lhs_name + ","
                + std::to_string(fractional_bits) + "'b0})" + "/$signed(" + rhs_name + ");"
        },
        clocked_block({
            base_name() + " <= " + buffer_name + "[" + std::to_string(register_size-1) +  ":0];"
        })
    });
#else
    return merge_code({
        { register_code(base_name(), register_size)},
        clocked_block({
            base_name() + " <= " + lhs_name + "*" + rhs_name + ";"
        })
    });
#endif
}
ErrorFnType Div::error_function() const {
    return [this](double* in){
        return lhs->error_function()(in) / rhs->min()
            + default_truncation_error({lhs.ptr(), rhs.ptr()}, in);
    };
}
// TODO: Test
std::vector<Rc<FracNode>> Div::smaller_word_constraints() const {return {lhs, rhs};}



#define BOOL_BINOP_BOILERPLATE(CLASS, INPUT_CLASS, SYMBOL, NAME, OP, CALC_OP, RUNTIME_TYPE) \
    CLASS::CLASS(Rc<INPUT_CLASS> lhs, Rc<INPUT_CLASS> rhs) \
        : BooleanBinaryOperator<INPUT_CLASS>(lhs, rhs) \
    {} \
    std::string CLASS::operator_symbol() const {return SYMBOL;} \
    std::string CLASS::operator_name() const {return NAME;} \
    Rc<CLASS> OP(Rc<INPUT_CLASS> lhs, Rc<INPUT_CLASS> rhs) { \
        return Rc<CLASS>(new CLASS(lhs, rhs)); \
    } \
    bool CLASS::recalculate_value() { \
        return this->lhs->calculate_value() CALC_OP this->rhs->calculate_value(); \
    }

#define BOOL_BINOP_SIGNED_COMPARISON(CLASS) \
    std::vector<std::string> CLASS::get_code(WordLength) {\
        auto lhs_name = lhs->pipeline_buffer_name(depth() - lhs->depth() - 1); \
        auto rhs_name = rhs->pipeline_buffer_name(depth() - rhs->depth() - 1); \
        return merge_code({ \
            { \
                register_code(base_name(), 1), \
            }, \
            clocked_block( \
                { base_name() + " <= $signed(" + lhs_name + ")" + operator_symbol() \
                    + "$signed(" + rhs_name + ");"} \
            ) \
        }); \
    }
#define BOOL_BINOP_UNSIGNED_COMPARISON(CLASS) \
    std::vector<std::string> CLASS::get_code(WordLength) {\
        auto lhs_name = lhs->pipeline_buffer_name(depth() - lhs->depth() - 1); \
        auto rhs_name = rhs->pipeline_buffer_name(depth() - rhs->depth() - 1); \
        return merge_code({ \
            { \
                register_code(base_name(), 1), \
            }, \
            clocked_block( \
                { base_name() + " <= " + lhs_name + operator_symbol() + rhs_name + ";"} \
            ) \
        }); \
    }

BOOL_BINOP_BOILERPLATE(LessThan, FracNode, "<", "lt", operator<, <, ExecutionType);
BOOL_BINOP_SIGNED_COMPARISON(LessThan);
BOOL_BINOP_BOILERPLATE(GreaterThan, FracNode, ">", "gt", operator>, >, ExecutionType);
BOOL_BINOP_SIGNED_COMPARISON(GreaterThan);
BOOL_BINOP_BOILERPLATE(LessThanOrEq, FracNode, "<=", "leq", operator<=, <=, ExecutionType);
BOOL_BINOP_SIGNED_COMPARISON(LessThanOrEq);
BOOL_BINOP_BOILERPLATE(GreaterThanOrEq, FracNode, ">=", "geq", operator>=, >=, ExecutionType);
BOOL_BINOP_SIGNED_COMPARISON(GreaterThanOrEq);

BOOL_BINOP_BOILERPLATE(Or, BoolNode, "|", "or", operator||, ||, bool);
BOOL_BINOP_UNSIGNED_COMPARISON(Or);

//////////////////////////////////////////////////////////////////////
// Not operator

Not::Not(Rc<BoolNode> input) : BoolNode({input.ptr()}), input(input) {}
Rc<Not> Not::create(Rc<BoolNode> input) {
    return Rc<Not>(new Not(input));
}
std::vector<std::string> Not::get_code(WordLength) {
    return merge_code({
        {register_code(this->base_name(), 1)},
        clocked_block({this->base_name() + " <= !" + this->parent_register(input) + ";"})
    });
}
std::vector<Rc<Node>> Not::get_parents() const {return {input};}
bool Not::recalculate_value() {return !input->calculate_value();}
std::string Not::variable_name() const {
    return "not_" + this->parent_register(input);
}
std::string Not::graph_label_name() {return "!";}

Rc<Not> operator!(Rc<BoolNode> input) {
    return Not::create(input);
}



//////////////////////////////////////////////////////////////////////
// Boolean constants

BoolConst::BoolConst(bool value) : BoolNode({}), value(value) {}

Rc<BoolConst> BoolConst::create(bool value) {
    return Rc<BoolConst>(new BoolConst(value));
}
std::vector<std::string> BoolConst::get_code(WordLength) {
    return {
        "localparam " + base_name() + " = " + (value ? "1" : "0") + ";"
    };
}
std::string BoolConst::variable_name() const {
    return value ? "true" : "false";
}
std::string BoolConst::graph_label_name() {
    return "boolconst " + base_name();
}

const Rc<BoolConst> BoolConst::True = BoolConst::create(true);
const Rc<BoolConst> BoolConst::False = BoolConst::create(false);



//////////////////////////////////////////////////////////////////////
// Standalone functions
//////////////////////////////////////////////////////////////////////


#define OVERLOADED_IF(TYPE, IF_TYPE) \
Rc<IF_TYPE> _if(Rc<BoolNode> condition, Rc<TYPE> on_true, Rc<TYPE> on_false) { \
    return IF_TYPE::create(condition, on_true, on_false); \
}
OVERLOADED_IF(FracNode, IfFractional);
OVERLOADED_IF(BoolNode, IfBoolean);

Rc<IfFractional> min(Rc<FracNode> lhs, Rc<FracNode> rhs) {
    auto result = _if(lhs < rhs, lhs, rhs);
    result->override_name("min_" + lhs->base_name() + "_" + rhs->base_name());
    result->_override_min(std::min(lhs->min(), rhs->min()));
    result->_override_max(std::min(lhs->max(), rhs->max()));
    return result;
}
Rc<IfFractional> max(Rc<FracNode> lhs, Rc<FracNode> rhs) {
    auto result = _if(lhs > rhs, lhs, rhs);
    result->override_name("max_" + lhs->base_name() + "_" + rhs->base_name());
    result->_override_min(std::max(lhs->min(), rhs->min()));
    result->_override_max(std::max(lhs->max(), rhs->max()));
    return result;
}
