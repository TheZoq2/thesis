#ifndef H_LOOKUP_TABLE
#define H_LOOKUP_TABLE

#include "node.hpp"
#include "rc.hpp"

#include <map>

extern const std::string LUT_STORAGE_LOCATION;

class LutResult;

class LookupTable {
    public:
        static Rc<LookupTable> create(
            std::string name,
            std::vector<double> inputs,
            std::vector<double> values
        );
        static Rc<LookupTable> create(std::string name, std::vector<double> values);

        std::string get_name() const;
        double min() const;
        double max() const;
        double lookup(std::size_t index) const;
        int size() const;

        std::vector<std::string> get_module() const;
    private:
        std::string name;

        // std::map<double, double> map;
        std::unordered_map<double, double> map;

        LookupTable(std::string name, std::vector<double> inputs, std::vector<double> values);
};


class LutResult : public FracNode {
    public:
        static Rc<LutResult> create(
            Rc<LookupTable> lut,
            Rc<FracNode> index
        );

        std::string variable_name() const override;
        std::vector<std::string> get_code(WordLength word_length) override;
        double min() const override;
        double max() const override;
        // TODO: Test
        ErrorFnType error_function() const override;
        // TODO: Test
        std::vector<Rc<FracNode>> smaller_word_constraints() const override;

        ExecutionType recalculate_value() override;
        std::vector<Rc<Node>> get_parents() const override;
        std::string graph_label_name() override;
    private:
        LutResult(Rc<LookupTable> lut, Rc<FracNode> index);

        Rc<FracNode> index;
        Rc<LookupTable> lut;
};
#endif
