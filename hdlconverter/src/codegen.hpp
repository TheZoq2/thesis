#ifndef H_CODEGEN
#define H_CODEGEN

#include <vector>
#include <map>

#include "node.hpp"

std::vector<std::string> validation_delay(
    std::string in_validity_name,
    std::string out_validity_name,
    int depth
);

class Codegen {
    public:
        Codegen(std::map<std::string, Rc<Node>> nodes);

        std::vector<std::string> generate_code(WordLength word_lengths);
        std::vector<std::string> generate_module(std::string name, WordLength word_lengths);

        std::vector<std::string> list_variables();

        std::vector<std::string> variable_length_defines();
    private:
        int output_depth();
        std::set<Node*> nodes;

        void add_node(Node* node);

        std::map<std::string, Rc<Node>> outputs;
};

#endif
