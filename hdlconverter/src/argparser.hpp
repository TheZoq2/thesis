#ifndef H_MAIN
#define H_MAIN

enum class RunType {
    EXECUTE,
    CODEGEN,
    WORD_LENGTH,
    RUN_SAMPLES,
};

RunType parse_options(int argc, char* argv[]);

#endif
