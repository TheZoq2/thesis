#ifndef H_RC
#define H_RC

#include <memory>
#include <vector>

/*
  A wrapper around shared_ptr which does not contain overloads for comparison oprators
*/
template<class T>
class Rc {
    public:
        Rc(T* inner) {
            this->inner =  std::shared_ptr<T>(inner);
        }

        // This abomination is spawned from the depths of the linked blog and
        // allows construction of an Rc<Base> from an Rc<Derived> where Derived
        // : public base
        // https://cpptruths.blogspot.com/2015/11/covariance-and-contravariance-in-c.html
        template <typename U>
        Rc(const Rc<U>& other,
             typename std::enable_if<std::is_convertible<U*, T*>::value, void>::type * = 0)
        : inner(other.get_inner())
        {}

        T& operator*() const noexcept {
            return *inner;
        }
        T* operator->() const noexcept {
            return inner.operator->();
        }

        std::shared_ptr<T> get_inner() const {
            return inner;
        }
        T* ptr() const {
            return inner.get();
        }

        bool is_same(Rc<T> other) const {
            return this->inner == other.inner;
        }
    private:
        std::shared_ptr<T> inner;
};

template<class T>
bool compare_rcs(std::vector<Rc<T>> lhs, std::vector<Rc<T>> rhs) {
    if (lhs.size() != rhs.size()) {
        return false;
    }
    for(std::size_t i = 0; i < std::min(lhs.size(), lhs.size()); ++i) {
        if(!lhs[i].is_same(rhs[i])) {
            return false;
        }
    }
    return true;
}

#endif

