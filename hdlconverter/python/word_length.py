import scipy.optimize
import pdb
import time

def info(a):
    if print_info:
        print(a)

if regenerate:
    def fun(x):
        return np.sum(x)

    bounds = list([(12, 20) for _ in range(node_amount)])

    constraints = []

    for function in functions:
        constraints.append({'type': 'ineq', 'fun': function});

    # This function is required because you can't capture variables inside for-loops
    # because they capture references.
    def create_constraint(destination, source):
        return lambda x: x[source] - x[destination]

    def final_fun(x):
        in_bounds = True
        extra_error = 0
        for constraint in constraints:
            if constraint['fun'](x) < 0:
                extra_error += -constraint['fun'](x) * 10000
                # return 10_000_000
        for (destination, source) in constraint_tuples:
            if x[destination] > x[source]:
                extra_error += (x[destination] - x[source]) * 10000
        return fun(x) + extra_error

    # pdb.set_trace()

    result = scipy.optimize.differential_evolution(final_fun, bounds
             , popsize=7, disp=print_info, maxiter=1000)
    result_array = np.round_(result.x, 4);

    for (destination, source) in constraint_tuples:
        if create_constraint(destination, source)(result.x) < 0:
            info("Unsatisfied constraint")

    np.save(filename, result.x)

    info(result)
else:
    info("Reading word lengths from file")
    result_array = np.load(filename + ".npy")
