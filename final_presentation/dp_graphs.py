#!/bin/python

from copy import deepcopy

from graphviz_lib import modify_node, edge, hidden_edge

SELECTED_COLOR = "orchid"

original = None
with open("dp0.gv") as f:
    original = f.readlines()


costs = {
    "z1": 1,
    "z2": 3,
    "z3": 2,
    "z4": 5,
    "z5": 4,
    "y1": 3,
    "y2": 2,
    "y4": 4,
    "y5": 5,
}

travel_costs = {
    ("y3", "z1"): 3,
    ("y3", "z2"): 1,
    ("y3", "z3"): 1,
    ("y3", "z4"): 0,
    ("y3", "z5"): 1,
}

travels = [
    (None, None),
    ("y3", "z1"),
    ("y3", "z2"),
    ("y3", "z3"),
    ("y3", "z4"),
    ("y3", "z5"),
]

correctors = {
    None: [("y3", "z5")],
    "z1": [("y3", "z5"), ("y3", "z4")],
    "z2": [("y3", "z5")],
    "z3": [("y3", "z5")],
    "z4": [("y3", "z1")],
    "z5": [("y3", "z1"), ("y3", "z2")],
}



best_cost = None
best_node = None

final_modified = None;
for (i, (src, dest)) in enumerate(travels):
    modified = deepcopy(original)
    if src != None:
        travel_cost = travel_costs[(src, dest)]
        total_cost = costs[dest] + travel_cost

        if best_cost == None or total_cost < best_cost:
            best_cost = total_cost
            best_node = dest

        visible_edge = [edge(src, dest)]
        modified = modify_node(
            modified,
            dest,
            f"label=\"{costs[dest]}\", style=filled, fillcolor={SELECTED_COLOR}"
        )
    else:
        visible_edge = []
        modified=original

    displaycost = "-" if best_cost == None else best_cost
    modified = modify_node(
        modified,
        "y3",
        f"label=\"{displaycost}\", style=filled, fillcolor={SELECTED_COLOR}"
    )
    if best_node != None:
        modified = modify_node(
            modified,
            best_node,
            f"label=\"{costs[best_node]}\", style=filled, fillcolor=green"
        )

    hacky_edges = list(map(hidden_edge, correctors[dest]))
    new_edges = list(map(lambda line: line + "\n", visible_edge + hacky_edges))
    new_graph = modified[:-1] + new_edges + [modified[-1]]

    with open(f"_dp{i}.gv", 'w') as f:
        f.writelines(new_graph)

    final_modified = modified



