theme: theme

--

<h1 class=title>High Level Synthesis for Optimising Hybrid Electric Vehicle Fuel Consumption using FPGAs and Dynamic Programming</h1>

## Frans Skarman


--

# Background

--

### Hybrid electric vehicles (HEVs)

[[(animated)
- Not just an electric vehicle with a combustion engine
>>
- Degrees of hybridisation
    - **Micro HEV**: Tiny electric motor for starting the combustion engine
    - **Mild HEV**: Electric motor for extra torque and **energy storage**
    - **Full HEV**: Larger electric motor
    - **Plug-in HEV**
>>
- This project models a mild HEV
]]


--

### The optimisation problem

<img src="../report/fig/state_space.svg" class="center w90p">

- What's the optimal path?

--

### Optimising a state

<img src="generated/_dp0.svg" class="dp_example">

- Cost of state = cost to get to goal
- Cost of goal node is 0
- Cost of other nodes can be computed recursively

--


### Optimising a state

<img src="generated/_dp1.svg" class="dp_example">

Input cost: 3

--

### Optimising a state

<img src="generated/_dp2.svg" class="dp_example">

Input cost: 1

--

### Optimising a state

<img src="generated/_dp3.svg" class="dp_example">

Input cost: 1

--

### Optimising a state

<img src="generated/_dp4.svg" class="dp_example">

Input cost: 2

--

### Optimising a state

<img src="generated/_dp5.svg" class="dp_example">

Input cost: 3



--


### With electricity

<img class="layer_img" src="../report/fig/layer_example.png">

- HEVs have more states and inputs
- Find the optimal path through this large state space



--

### Required computations

[[(animated)
- Each input must be tested in each state
<img class=small_layer_img src="../report/fig/layer_example.png">
>>
- The model has
    - 6 gears, 30 motor steps, 30 engine steps
>>
        ⇒ 5400 inputs
>>
    - 30 velocity steps and 30 state of charge steps
>>
        ⇒ 900 states
>>
    - 680 time steps
>>

⇒ `680 · 900 · 5400` ≅ 3 billion model executions

>>
*We want to do this in real time ⇒ ~1 second*
]]

--

### Parallelism


<img class="layer_img" src="../report/fig/layer_example.png">

[[(animated)
- Cars can't time travel
>>
- Cost of a layer is only dependent on the cost of the next
>>
⇒ Parallelism is possible
]]


--

### Field Programmable Gate Array (FPGA)

[[(animated)
- Programmable hardware
>>
- Lots of potential for parallelism
>>
- Difficult to program
]]









--

### High level synthesis

- FPGAs are not easy to program
- Vehicle models are written in conventional languages
- Automatic conversion would be advantageous


--

### Research questions

- How can a vehicle drivetrain model automatically be converted to FPGA hardware?
- Can an FPGA implementation of the HEV optimisation algorithm be run in real time?
- What modifications can be made to the optimisation problem to improve the calculation speed and resource usage?
- What impact do those modifications have on the resulting vehicle fuel consumption and FPGA resource usage?

--



# Method

--

### Hardware generation


- Goal: C++ → Verilog
- Computations are represented by expression trees
    - Allow easy manipulation and analysis
- Custom classes convert from C++ to expression tree


--

### Usage example

[[(animated)
```cpp
// Original
double simple_model(double a, double b) {
    double sum = a + b;
    double product = sum * a;
    return product + 3.14;
}
```
>>

```cpp
// Converted
Node* simple_model(Input a, Input b) {
    auto sum = a + b;
    auto product = sum * a;
    return product + Constant::create("some_const", 3.14);
}
```

>>

- Code structure is the same
- Text replacement can do the bulk of the work
]]




--

### Conversion to verilog

<img src="generated/expression_tree.svg" class="right" style="width: 35%;">

Expression tree → verilog conversion

```verilog
module model begin
endmodule
```

--

### Conversion to verilog

<img src="generated/animated/expression_tree_a.svg" class="right" style="width: 35%;">

Expression tree → verilog conversion

```verilog
module model begin
    input[..] a;
endmodule
```

--

### Conversion to verilog

<img src="generated/animated/expression_tree_b.svg" class="right" style="width: 35%;">

Expression tree → verilog conversion

```verilog
module model begin
    input[..] a;

    input[..] b;
endmodule
```

--

### Conversion to verilog

<img src="generated/animated/expression_tree_sum.svg" class="right" style="width: 35%;">

Expression tree → verilog conversion

```verilog
module model begin
    input[..] a;

    input[..] b;

    reg sum[..];
    always @(posedge clk)
        sum = a + b;
endmodule
```
--

### Conversion to verilog

<img src="generated/animated/expression_tree_product.svg" class="right" style="width: 35%;">

Expression tree → verilog conversion

```verilog
module model begin
    input[..] a;

    input[..] b;

    reg sum[..];
    always @(posedge clk)
        sum = a + b;

    reg product[..];
    always @(posedge clk)
        product = a * sum;
endmodule
```

--

### Word length selection

- Word length is still unknown.
- Fractional and integer word length must be decided
- Floats would be nice, but are expensive 
    - Fix point numbers are an alternative


--

### Integer word length

- Controls the size of values that can be stored
- Required size gives required bit amount
- Required size can be computed recursively

[[(animated)
>>
if `10 < x < 15` and `-20 < y < 30`
>>

then `10-20 < (x+y) < 30+15`
]]


--

### Fractional word length

- More tricky because errors propagate
- Per-variable optimisation was unsuccessful
- Uniform fractional word length was not
    - Optimum determined by simulation

--

# Results

--

### Fractional word length

<img class="center w80p" src="plots/build/fractional_word_length.svg">

--


### Hardware usage

<img class="center half left" src="plots/build/lut_flip_flops.svg">
<img class="center half right" src="plots/build/block_rams.svg">

--

### Minimum clock period

<img class="center w80p" src="plots/build/minimum_period.svg">

--

### Model performance

- Pipelining allows one model execution per clock cycle
- 3 billion model executions required
- At 9 MHz, that requires ~350 seconds
- Slower than CPU model


--

### Optimisations

- Code has some bottle necks
    - Division
    - Square root
- Significantly reduced resource usage
    - ~100k -> ~10k LUT-flip-flop pairs
    - Slightly more block RAMs
- Optimisation leads to ~100 MHz
    - Faster than CPU, still not real time

--

### More parallelism!

- Parallelism is only exploited through pipelining
- Concurrent pipelines
    - Requires some extra hardware
    - Significant performance increase
- At 100 MHz, 30 pipelines are required
- At 500 MHz, only 6 are required



--

# Model modifications

--

### Avoid expensive computations

- Replace expensive operations
    - Divisions by multiplication
    - Use `v` as state instead of `v²`
    - No effect on output



--

### Scale large values

<img src="generated/large_value_graph.svg" class="center half">



--


### Fully utilised hardware

Not all hardware is created equal

- Block RAM must be addressed in powers of 2
    - 200 values ⇒
    - 8 bits ⇒ 256 possible values
    - Round up for free precision
    - Round down for less hardware usage
- Similar idea with multipliers



--

### Conclusions

[[(animated)
- C++ → Verilog conversion
    - Class replacement for expression trees works well
>>
- Real-time performance?
    - Not possible in the current state
    - Probably with some optimisations
]]

