# Background

## HEVs

To start off, I'm going to talk about the different types of hybrid electric
vehicles that are out there. Before I started working on this project, I was
under the impression that a HEV is always an electric vehicle with an extra
combustion engine for long distance journeys.

These do exist and are called Plug In HEVs because you can charge them from the
power grid. However, there are also a lot of HEVs that use the electric motor
as support for the combustion engine.

This ranges from micro HEVs where the electric motor is exclusively used for
starting the ICE, to full hybrids which can drive long distances using the
electric motor. In the middle are mild hybrids which primarily use the ICE,
but can get some extra assistance from the electric motor. This is kind is
the primary focus of this project.

The powertrain of an electric vehicle can be configured in different ways, but
the one used in this project is called a parallel HEV where both the electric
motor and combustion engine are used to provide torque to the wheels.

## The optimisation problem

As you heard from the exessively long title, this project is focused on
optimising the driving pattern of HEV which is done to reduce fuel usage, and
in turn, emissions of the vehicle.

But what makes an HEV special, and how can we reduce fuel usage by adding an FPGA?

Since a HEV has an internal battery which can be recharged by regenerative breaking,
energy spent by burning fuel in the combustion engine isn't always lost, it can be
put back into the battery. As long as the battery isn't full that is.

Additionally, because there is an electric motro and a combustion engine, you
can combine the two in several ways to reach a target torque. One method for
optimising the driving pattern is to simulate driving the desired route using
varying control inputs to see which one works best. However, as we shall see,
this is a very expensive computation which is difficult to do on an CPU. However,
the hypothesis is that it is possible with an FPGA.

## Dynamic programming

Dynamic programming was another word mentioned in the title, and it is a family
of algorithms which can be used to sovle problems like this, where lots of
similar computations have to be made.



## Required computations

In order to solve this problem, we define the cost of a state as the cost of getting
to the goal node from that state. This cost can be calculated recursively by checking
all the inputs in the state. By doing so for each of the states in a layer, we have
moved the problem forward one step.




## Required computations

While this usage of dynamic programmming saves a lot of computations, the
resulting computation still is not cheap. First, we need to go through each
distance step, which in this case has 680 values which equates to roughly 7 km.

In each of these time slices, we need to compute the cost of each state. This
model has 30 possible values for the state of charge, and 30 values for the
velocity. This results in 900 different states per time step, and 900 * 680 states in total.


In each state, we need to try all inputs. The vehicle model has 6 gears, 30
engine torque steps and 30 motor torque steps. This equates to 5400 inputs per
state, and 3 billion evaluations of the model.

We want this to run in real time in order to be able to re-evaluate the model
while driving, and in this case, real time corresponds to one full execution in
roughly one second.




## Number representation

There is one more bit of theory that we need to consider before finally getting
on to the work that I have actually been doing, and that is number
representation. Most general purpose CPUs and programming languages use
floating point values to represent non-integer numbers. This has plenty of
advantages, but one major disadvantage when implemented in FPGAs: they use a
very large amount of hardware.

Therefore, on FPGAs, you typically use fixed point numbers. These are analogous
to decimal numbers, but expressed in binary which means that they have an
integer part and a fractional part. Somewhat simplified, the integer part
controls the largest number that can be stored, adding an integer bit doubles
the storage capacity.

The fractional part on the other hand controls the precision of the number.
Adding an additional bit halves the maximum rounding error.

# Method

## Hardware generation

As said, the primary goal of this thesis was to write a tool to generate
hardware for a vehicle model, given the model in normal C++ code. This process
is started by converting the C++ code from producing a value, to producing a
computation that produces that value.

Computations can be stored as trees called expression trees, where a node corresponds
to a computation and edges correspond to operands. The advantage to an expression tree
is that it is easy to analyse the computation being performed.

In order to convert a C++ expression into an expression tree, I added some new classes
which rather than contain values, contain the operation to be performed. These classes
are called nodes, and keep track of the tree structure.

As an example of their usage, we can have a look at a simple conversion between normal C++
code and the corresponding expression tree generating code:

```cpp
// Original
double simple_model(double a, double b) {
    double sum = a + b;
    double product = sum * a;
    return product + 3.14;
}

// Converted
Node* simple_model(Input a, Input b) {
    auto sum = a + b;
    auto product = sum * a;
    return product + Constant::create("some_const", 3.14);
}
```

As you can tell, the structure of the code is the same, and most of the
conversions that have to be performed for such a simple example are simple text
replacement. This is close to true for larger code as well, with some pitfalls
that I won't go into today.


## Conversion to verilog

The idea behind this project is as stated, to go from C++ to working verilog
code. This is achieved by converting the expression tree that is generated in
the previous step to equivalent verilog.

This process is fairly straight forward, each node in the tree maps to a short
snippet of verilog code which creates a variable, and performs the required
operation. The code generated by an addition operation is shown here

```cpp
string Add::get_code() {
    lhs_var = lhs->variable_name();
    rhs_var = rhs->variable_name();
    return "
        reg[..] {this->variable_name()};
        always @(posedge clk) begin
            this->variable_name() <= {lhs_var} + {rhs_var};
        end
    "
}
```

This creates a register which at each clock cycle is updated with the result of the
addition of the two operands.

## Word length selection

As you can see in the previous example, the size of the word used for storage
is left out. This process comes in two parts, integer word length and fractional word
length.

Remember, the integer word length determines the maximum size of a variable. If
we know the bounds on the variable we want to store, we can use that to
determine the required size.

Luckily, the bounds can be calculated recursively with some initial help from a
user. Take for example the addition of `a` and `b` where we know that `a` is
between -50 and 120, and `b` is between -25 and 10. `a+b` then has to
be between `-50 -25` and `120 + 10`

Using arguments simular to this, we can compute the integer word length for all variables
in the system, assuming that the bounds on inputs are known.


## Fractional word length selection

Fractional word length is more difficult to optimise as that controls the error
in the output and erros propagate through the computation. Some attempts were
made at optimising this on a per-variable basis, but those attempts were
unsuccessful.

Instead, the fractional word length was set to one value for all variables, and
an optimal value for this was determined through simulation.



## Performance evaluation

That's the end of the bulk of the method, but how well does it work? The performance
of FPGA hardware is described by two primary factors: the amount of hardware used,
and the execution time of the resulting hardware.

In order to determine this, the resulting model was synthesised using xilinx
ISE 14.7 with a virtex 6 board as a target. For hardware usage, I primarily
looked at the amount of LUT flip-flop pairs which gives a good measure of the
hardware usage. Additionally, I also report the amount of block RAMs as those
are special hardware in FPGAs. For the execution time, the synthesis tool gives
a maximum clock frequency that the design can run at.


However, we still don't know how many fractional bits to use. To determine
that, the model was simulated using between 12 and 32 bits and the error in the
output compared to the reference model was analysed.

# Results

Let's start off with the results of the fractional word length. This graph
shows the resulting cost when running a whole optimisation on two different
road profiles as a function of the amount of fractional bits used. The dashed
line represents the output of the original model.

As you can see, at around 20 fractional bits, the error is within 1% of the
original model and remains unchanged until 27 or 28 bits where it equals the
reference output.


## Hardware usage

As mentioned, hardware usage is an important metric in FPGA hardware. This plot
shows the amount of LUT-flip-flop pairs used by the design at different word
lengths. The result is pretty much linear with some strange behaviour at the end.

The graph for the amount of block RAMs looks similar.


Ultimately, what decides the time required to finish the optimisation is the
frequency of the FPGA which is the inverse of the maximum clock period.

This is shown in the following graph. The reason for the jaggedness is that
FPGA synthesis is a stocastic process. As you can tell, the clock frequency is
between 8 and 9 MHz.


## Model performance

Thanks to the pipelining that was used, one model execution can be started
almost every clock cycle, which means that 3 billion clock cycles are required
in order to run the full optimisation problem. At 8 MHz, this would take
roughly 360 seconds, which is far too long for real time performance. In fact,
it is slower than the execution on a normal CPU.


## Optimisations

Luckily, some parts of the code are very unoptimised, specifically divisions and
computations of the square root. While this wasn't fully fixed in this thesis,
some simulations were made with proposed fixes which lead to performance
jumping to roughly 100 MHz. This is still too slow for real time performance,
but it is faster than the CPU implementation.

Additionally, this only uses one pipeline for computation, but it could be
expanded to use more than one for aditional performance, at the cost of some
RAM access complexity.

At 500 MHz, which is around the maximum frequency of some FPGAs, only 6 concurrent 
pipelines would be required to achieve a computation time of 1 second.


## Current usefulness

So, are these modifications required for the system to be useful. Certainly for
the current model and optimisation problem. However, the system has onetrick up
its sleeve, the runtime is largely independent of the model complexity. A more
complex model would dramatically increase the the CPU execution time, but not
the FPGA execution time.

## Model modifications

Finally, part of the goal of the project was to identify possible modifications
to the model and possibly optimisation problem. First, it is clear from the
previous discussion that the square root and division operations are pretty
expensive.

## Avoiding square roots

The reason for one of the square root calculations is that the velocity state
is actually the kinetic energy, and to get the velocity, it has to be square
rooted. one could instead use the velocity as a parameter and square it to get
the energy. However, that would lead to the distances between states not being
linear.



## Avoiding large values

It is clear from the synthesis results that smaller values lead to both lower
resource usage and higher frequencies. Additionally, since all values in the
model are stored at the same amount of fractional bits, some values are probably
stored at much too high precision.

Therefore, some large values could be identified and scaled down while the values being computed are large. This figure shows an example of this:

\<Figure from report\>



## Fully utilise hardware

Finally, not all hardware is built from the basic building blocks of an FPGA,
things like block RAMS and multipliers have dedicated hardware inside the
device.

As this can't be fully customised, once you start using part of it, you can use
all of it at no additional cost which means that things like lookup tables for functions
should be resized to fill units of powers of two.
