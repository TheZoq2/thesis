#!/bin/python

import graphviz_lib as gv

original = None
with open("expression_tree.gv") as f:
    original = f.readlines()

labels = {
    "a": "a",
    "b": "b",
    "sum": "+",
    "product": "*",
}

for node in ["a", "b", "sum", "product"]:
    modified = gv.modify_node(original, node, f"label=\"{labels[node]}\", style=filled, fillcolor=green")
    with open(f"animated/expression_tree_{node}.gv", "w") as f:
        f.writelines(modified)
