import re

def modify_node(lines, node, attributes):
    def inner(line):
        if line.strip().split(' ')[0] == node and re.search(f"\w*{node} \[", line):
            return f"{node} [{attributes}]"
        else:
            return line
    return list(map(inner, lines))

def edge(src, dest, visible=True):
    visibility = "" if visible else ",style=invis"
    return f"{src} -> {dest} [constraint=false{visibility}]"

def hidden_edge(source_dest):
    (src, dest) = source_dest
    return edge(src, dest, False)

