load "plots/style.gp"

set xlabel "Fractional bits" font ", 25" offset -1,0

$OLD_ROAD_REF << EOD
12, 359.735
31, 359.735
EOD

$NEW_ROAD_REF << EOD
12, 337.93895
31, 337.93895
EOD

plot \
"../report/fig/word_length_output_result.csv" using 1:3 title "Road 1" with linespoints ls 1, \
"../report/fig/word_length_output_new_road.csv" using 1:3 title "Road 2" with linespoints ls 3, \
"$OLD_ROAD_REF" title "original" with lines ls 2, \
"$NEW_ROAD_REF" notitle with lines ls 4
