set terminal svg

set datafile separator ","

set style line 1 lt 1 lw 2 pt 7 ps 1 lc rgb "#6e288e"
set style line 2 lc rgb "#6e288e" lw 3 dashtype 7
set pointintervalbox 3

set style line 3 lt 1 lw 2 pt 7 ps 1 lc rgb "#338e28"
set style line 4 lc rgb "#338e28" lw 3 dashtype 7

set style line 5 lc rgb "#555555" lw 2 dashtype 7


set xtics font ", 20"
set ytics font ", 15"

set key font ",20"

