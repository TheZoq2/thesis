load "plots/style.gp"

set ylabel "Block RAMs" font ", 25" offset -1,0
set xlabel "Fractional bits" font ", 25" offset -1,0

set arrow from 20, graph 0 to 20, graph 1 nohead ls 5
set arrow from 27, graph 0 to 27, graph 1 nohead ls 5

plot \
"../report/fig/synth_results.csv" using 1:4 notitle with linespoints ls 3, \
