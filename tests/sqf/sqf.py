import numpy as np
import scipy.optimize

def fun(x):
    return np.sum(x)




def constraint(x):
    tmp0_error = 255 * 2**(-x[0]) + 2**(-x[3])
    tmp1_error = 255 * 2**(-x[1]) + 2**(-x[4])
    tmp2_error = 255 * 2**(-x[2]) + 2**(-x[5])

    tmp3_error = (2**(-x[6]) - 2**(-x[3])) + (2**(-x[6]) - 2**(-x[4]))
    y_error = (2**(-x[7]) - 2**(-x[5])) + (2**(-x[7]) - 2**(-x[6]))


    return 0.5 - (\
        tmp0_error + \
        tmp1_error + \
        tmp2_error + \
        tmp3_error + \
        y_error
    )

bounds = [(0, 32) for _ in range(8)]

x0 = list([3 for x in range(8)])

constraints = [
    {'type': 'ineq', 'fun': constraint},
    {'type': 'ineq', 'fun': lambda x: x[3] - x[6]},
    {'type': 'ineq', 'fun': lambda x: x[4] - x[6]},
    {'type': 'ineq', 'fun': lambda x: x[5] - x[7]},
    {'type': 'ineq', 'fun': lambda x: x[6] - x[7]},
]

result = scipy.optimize.minimize(fun, x0, bounds=bounds, constraints=constraints)
