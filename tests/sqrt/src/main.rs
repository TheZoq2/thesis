// log(x) = log(2^n *(1 + f)) = n + log(1+f)
//
// 2^n * (1+f) = x   <=>
// x / (2^n) = (1+f) = x >> n <=>

// log2(1+f) = log2(x >> n) <=>
//
// log2(x) = n + log2(x >> n)

fn rough_log(x: i32) -> i32 {
    let as_float = (x as f32) / 128.;

    let largest_1 = as_float.log2().floor() as i32;

    // println!("largest_1 {}, as_float {}", largest_1, as_float);


    // Shift the input to put the largest 1 bit at position 8
    let shifted = if largest_1 > 0 {
        x >> largest_1
    }
    else {
        x << (-largest_1)
    };

    // println!("shifted {}", shifted);


    let lutout = (shifted as f32 / 128.).log2() * 128.;

    (largest_1 << 7) + (lutout as i32)
    // largest_1 << 8
    // priencout * 128
}

fn rough_exp(x: i32) -> i32 {
    let integer_part = x >> 7;

    let fractional_part = x & 127;

    let lutout = (2. as f32).powf(fractional_part as f32 / 128.);

    if integer_part > 0 {
        ((lutout * 128.) as i32) << integer_part
    }
    else {
        (lutout * 128.) as i32 >> -integer_part
    }
}

fn main() {
    let range_size = 1000;
    let range = 1..range_size;
    for i in range {
        // let rough = (rough_log(i) as f32) / 128.;
        // let exact = (i as f32 / 128.).log2();

        // let rough = rough_exp(i) as f32 / 128.;
        // let exact = (2. as f32).powf(i as f32 / 128.);

        let rough = rough_exp(rough_log(i)) as f32 / 128.;
        let exact = (i as f32 / 128.).sqrt();

        println!("{} {}", rough, exact);

        // sum_err += (exact-rough).abs();
    }

    // println!("{}", rough_exp(64) as f32 / 128.);

    // println!("{}", rough_log(4 * 128)/128);
    // println!("{}", rough_exp(64) as f32 / 128.);

    // println!("average error: {}", sum_err / (range_size - 1) as f32);
}
