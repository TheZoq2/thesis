ExecutionType Add::calculate_value() {
    lhs_value = this->lhs->calculate_value();
    rhs_value = this->rhs->calculate_value();
    return lhs_value + rhs_value;
}
