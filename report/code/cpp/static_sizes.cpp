template<int MIN, int MAX> class FractionalValue {};

template<int MIN1, int MAX1, int MIN2, int MAX2>
FractionalValue<MIN1-MIN2, MAX1+MAX2> operator+(
    const FractionalValue<MIN1, MAX1>& lhs,
    const FractionalValue<MIN2, MAX2>& rhs
);

void test_fn() {
    FractionalValue<0, 123> combustionTorque;
    FractionalValue<0, 400> electricTorque;
    auto totalTorque = combustionTorque + electricTorque;
    // Total torque will now have the type FractionalValue<0, 523>
}

