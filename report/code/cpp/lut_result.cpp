class LutResult : FracNode {
    public:
        LutResult(FracNode input, LookupTable lut) : input(input), lut(lut) {}
        get_code() {
            return {
                // "Storage" for the LUT output
                "wire[..]" + this->variable_name() + ";",
                // Instanciate lut module
                lut->name " " + this->variable_name + "_lut"
                    // Configure the size of the input variable
                    + "#(.input_size(" + this->integer_bit_count() + "))"
                    // Bind normal variables
                    + "(.clk(clk),"
                    + " .input(" + input.variable_name() + "),"
                    + " .output(" + this->variable_name() "))"
            }
        }
    private:
        FracNode input;
        LookupTable lut;
};
