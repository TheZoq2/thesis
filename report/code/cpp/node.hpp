class Node {
    public:
        vector<Node*> operands() const = 0;
};
