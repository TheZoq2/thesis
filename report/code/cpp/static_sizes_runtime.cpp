class Node {
    virtual double min_value() const = 0;
    virtual double max_value() const = 0;
};
class Add : public Node {
    Add(Node* lhs, Node* rhs) {
        // Compute bounds and store the nodes
    }
};
class Variable : public Node {};

Add operator+(const Node& rhs, const Node& lhs) {
    return Add(lhs, rhs);
}

void test_fn() {
    Variable combustionTorque(0, 100);
    Variable electricTorque(0, 50);
    auto totalTorque = combustionTorque + electricTorque;
    // Total torque will now have the have the bounds 0 < x < 150
}

