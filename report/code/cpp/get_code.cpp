vector<string> Add::get_code() {
    return {
        "wire[..]" + this->variable_name() + ";",
        "assign " + this->variable_name()
             + " = " this->lhs->variable_name()
             + " + " this->rhs->variable_name(),
    }
}
