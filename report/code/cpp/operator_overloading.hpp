shared_ptr<Node> operator+(const shared_ptr<Node> lhs, const shared_ptr<Node> rhs) {
    return make_shared(new Add(&lhs, &rhs));
}

// Example usage
shared_ptr<Node> sum_constants() {
    shared_ptr<Constant> a = make_shared(Constant(5));
    shared_ptr<Constant> b = make_shared(Constant(3));
    return a + b;
}
