class Add : public Node {
    public:
        Add(Node* lhs, Node* rhs) : lhs(lhs), rhs(rhs)
            {}
        vector<Node*> operands() const override {
            return {lhs, rhs};
        }
    private:
        Node* lhs;
        Node* rhs;
};

class Constant : public Node {
    public:
        Constant(double value) : value(value) {}
        vector<Node*> operands() const override {
            // A constant does not require any calculation
            // which means that it has no operands
            return {};
        }
    private:
        double value;
};
