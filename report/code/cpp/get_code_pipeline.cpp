vector<string> Add::get_code() {
    return {
        "reg[..]" + this->variable_name() + ";",
        "assign " + this->variable_name()
             + "=" + lhs->register_name(this->depth() - lhs->depth()-1)
             + "+" + rhs->register_name(this->depth() - rhs->depth()-1)
    }
}
