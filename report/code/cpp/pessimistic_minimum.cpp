const MIN_VALUE = 0.3;
const MAX_VALUE = 1;
// Holds a value between 0 and 100
Input<0, 100> some_value;

// Cap that value between 0.3 and 1
auto value_capped = _if(some_value < MIN_VALUE,
    MIN_VALUE,
    _if(some_value > MAX_VALUE, MAX_VALUE, some_value),
);

auto result1 = value_capped + 1;
auto result2 = 1 / value_capped;



