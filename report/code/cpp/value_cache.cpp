class Node {
    public:
        // Function that actually recomputes the value.
        // Replaces calculate_value in subclasses
        virtual ExecutionType recalculate_value() = 0;

        ExecutionType calculate_value() {
            if(!cached_value.has_value()) {
                cached_value = this->recalculate_value();
            }
            return cached_value.value();
        }

        void mark_dirty() {
            this->cached_value = std::nullopt;
        }
    private:
        std::optional<ExecutionType> cached_value;
};
