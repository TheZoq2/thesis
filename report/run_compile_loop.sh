#!/bin/bash

sources="$(ls src/*.tex)\n$(ls code/cpp/*)\n$(ls fig/*.gv)"

echo -e "$sources" | entr make "$1"
