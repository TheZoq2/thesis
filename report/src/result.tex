\chapter{Results}\label{cha:results}

This chapter presents the results of the evaluation described in
Section~\ref{sec:performance_evaluation}. Section~\ref{sec:word_length_effect}
presents the model output quality at different amounts of fractional bits.
In Section~\ref{sec:synthesis_results} the resource usage and performance of
the model as reported by the synthesis tool will be presented. This is further
analysed in Section~\ref{sec:result:execution_time} to determine the theoretical
running time of a full optimisation. Finally, in Section~\ref{sec:performance_bottle_necks}, various model changes
and their effects on execution time and resource usage will be
presented.



\section{Word length effect on output}\label{sec:word_length_effect}

Figure~\ref{fig:truncated_cost} shows the fuel consumption achieved when
running the model using double precision floats and truncating the output to
various bit lengths. From this simulation, it was determined that anything
less than 12 fractional bits would introduce large errors into the output.

\begin{figure}[h]
    \centering
    \begin{tikzpicture}
        \begin{axis}[
                xlabel={Number of fractional bits},
                ylabel={Fuel consumption},
            ]
            \addplot[red]coordinates{(4,301.15341) (32,301.15341)};
            \addplot[blue,mark=o] table [x=a, y=b, col sep=comma] {fig/fuel_over_rounding.csv};
        \end{axis}
    \end{tikzpicture}
    \caption{Model output as a function of the amount of fractional bits which
    the minimum cost in each state was truncated to. All other values were
    computed at full precision. The red line represents the output with no
    truncation}\label{fig:truncated_cost}
\end{figure}


Figure~\ref{fig:cost_per_word_size} shows the total cost over a test
sequence as a function of the amount of fractional bits used for all the
variables in the computation. These results seem to indicate that 20 bits is
enough to achieve a result that is within $1\%$ of the original model, while 28
bits is required for output that is near identical to the original model.

\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \begin{axis}[
                xlabel={Number of fractional bits},
                ylabel={Total cost},
            ]
            \addplot[blue,mark=o] table [x=x, y=cost, col sep=comma] {fig/word_length_output_result.csv};
            \addplot[dashed, blue, thick]coordinates{(12,359.735) (31,359.735)};

            \addplot[red,mark=o] table [x=x, y=cost, col sep=comma] {fig/word_length_output_new_road.csv};
            \addplot[dashed, red, thick]coordinates{(12,337.93895) (31,337.93895)};
        \end{axis}
    \end{tikzpicture}
    \caption{Model output as a function of the amount of fractional bit used to
    store every variable. The blue plot is the result of American the road
    profile while the red is the Swedish road profile.  The dashed lines
    represent the output of the original CPU implementation}\label{fig:cost_per_word_size}
\end{figure}


\subsubsection{Velocity profile}

Figure~\ref{fig:velocity_profile} and~\ref{fig:soc_profile} show the optimal
velocity and state of charge in each state as calculated by the model when run
at different word lengths. This shows, as one might expect, that the driving
pattern is more different from the original model at 12 bits than at at 20
bits. However, it still differs slightly, even at 20 bits.

Figure~\ref{fig:velocity_profile_high_bits} shows the optimal velocity as
calculated at 20, 25 and 31 bits. It shows that the velocity profile is
identical to the original at 31 bits, and that there is no difference between
the results when running with 20 or 25 bits.






\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \begin{axis}[
                xlabel={Distance},
                ylabel={Velocity ($\frac{m}{s}$)},
                width = \linewidth,
                height = 6cm,
                legend pos=north west,
                legend style={font=\tiny},
                xmin = 0,
                xmax = 690,
                ymin = 0,
                scaled ticks=true
            ]
            \addplot[cyan, thick] table [x=distance, y=12, col sep=comma] {fig/velocity.csv};
            \addplot[black, thick] table [x=distance, y=19, col sep=comma] {fig/velocity.csv};
            \addplot[red, thick] table [x=distance, y=20, col sep=comma] {fig/velocity.csv};
            \addplot[green, dashed, thick] table [x=x, y=y, col sep=comma] {fig/velocity_ref.csv};
            \addplot[dotted] table [x=distance, y=upper, col sep=comma] {fig/velocity.csv};
            \addplot[dotted] table [x=distance, y=lower, col sep=comma] {fig/velocity.csv};
            \legend{12 bits, 19 bits, 20 bits, double, bounds}
        \end{axis}
    \end{tikzpicture}
    \caption{Optimal velocity on the American road profile as calculated by the
    model when executed with varying word lengths}\label{fig:velocity_profile}
\end{figure}

\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \begin{axis}[
                xlabel={Distance},
                ylabel={State of charge (\%)},
                width = \linewidth,
                height = 6cm,
                legend pos=south west,
                legend style={font=\small},
                xmin = 0,
                xmax = 690,
                ymin = 0,
                scaled ticks=true
            ]
            \addplot[cyan, thick] table [x=distance, y=12, col sep=comma] {fig/state_of_charge.csv};
            \addplot[black, thick] table [x=distance, y=19, col sep=comma] {fig/state_of_charge.csv};
            \addplot[red, dashed, thick] table [x=distance, y=20, col sep=comma] {fig/state_of_charge.csv};
            \addplot[dotted, thick] table [x=distance, y=upper, col sep=comma] {fig/state_of_charge.csv};
            \addplot[dotted, thick] table [x=distance, y=lower, col sep=comma] {fig/state_of_charge.csv};
            \legend{12 bits, 19 bits, 20 bits}
        \end{axis}
    \end{tikzpicture}
    \caption{Optimal state of charge on the American road profile as calculated
    by the model when executed with varying word
    lengths}\label{fig:soc_profile}
\end{figure}

\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \begin{axis}[
                xlabel={Distance},
                ylabel={Velocity ($\frac{m}{s}$)},
                width = \linewidth,
                height = 6cm,
                legend pos=north west,
                legend style={font=\tiny},
                xmin = 0,
                xmax = 690,
                ymin = 0,
                scaled ticks=true
            ]
            \addplot[black, thick] table [x=distance, y=20, col sep=comma] {fig/velocity.csv};
            \addplot[cyan, dashed, thick] table [x=distance, y=25, col sep=comma] {fig/velocity.csv};
            \addplot[red, thick] table [x=distance, y=31, col sep=comma] {fig/velocity.csv};
            \addplot[green, dashed, thick] table [x=x, y=y, col sep=comma] {fig/velocity_ref.csv};
            \addplot[dotted] table [x=distance, y=upper, col sep=comma] {fig/velocity.csv};
            \addplot[dotted] table [x=distance, y=lower, col sep=comma] {fig/velocity.csv};
            \legend{20 bits, 25 bits, 31 bits, double, bounds}
        \end{axis}
    \end{tikzpicture}
    \caption{Optimal velocity on the american road profile at higher bit
    counts.}\label{fig:velocity_profile_high_bits}
\end{figure}

\section{Synthesis results}\label{sec:synthesis_results}

Table~\ref{tab:synth_results} contains the amount of FPGA resources used by the
model as well as the minimum clock period at which the model can run. These
numbers are also shown as graphs in
Figures~\ref{fig:lut_flip_flop_pairs},~\ref{fig:block_rams}
and~\ref{fig:frequency}.  It looks like all three resource usage metrics
increase linearly with the amount of fractional bits. However, the minimum
clock period has quite a bit of noise. This might be caused by the stochastic
nature of \ac{HDL} synthesis.

\pgfplotstabletypeset[
    col sep=comma,
    string type,
    columns/x/.style={column name=Fractional bits, column type={r}},
    columns/slices/.style={column name=LUT flip-flop pairs, column type={r}},
    columns/freq/.style={column name=Min.\ clock period (ns), column type={r}},
    columns/brams/.style={column name=Block RAMs, column type={r}},
    every head row/.style={before row=\caption{Synthesis results}\label{tab:synth_results}\\, after row=\hline},
    ]{fig/synth_results.csv}

\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \begin{axis}[
                xlabel={Number of fractional bits},
                ylabel={Number of LUT flip-flop pairs used},
                ymin=0,
            ]
            \addplot[blue,mark=o] table [x=x, y=slices, col sep=comma] {fig/synth_results.csv};
        \end{axis}
    \end{tikzpicture}
    \caption{Number of LUT flip-flop pairs used as a function of the
    fractional word length}\label{fig:lut_flip_flop_pairs}
\end{figure}

\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \begin{axis}[
                xlabel={Number of fractional bits},
                ylabel={Number of block RAMs used},
                ymin=0,
            ]
            \addplot[blue,mark=o] table [x=x, y=brams, col sep=comma] {fig/synth_results.csv};
        \end{axis}
    \end{tikzpicture}
    \caption{Number of block RAMs used as a function of the
    fractional word length}\label{fig:block_rams}
\end{figure}

\begin{figure}[H]
    \centering
    \begin{tikzpicture}
        \begin{axis}[
                xlabel={Number of fractional bits},
                ylabel={Minimum clock period (ns)},
                ymin=0,
            ]
            \addplot[blue,mark=o] table [x=x, y=freq, col sep=comma] {fig/synth_results.csv};
        \end{axis}
    \end{tikzpicture}
    \caption{Minimum clock period that the model can run at at different word
    lengths}\label{fig:frequency}
\end{figure}




\section{Execution time}\label{sec:result:execution_time}

As shown in Section~\ref{sec:word_length_effect}, $20$ fractional bits is
enough to achieve close to full precision of the model. At that bit count, the
maximum clock frequency of the model is $9$ MHz.

The model used in this project model has $t=680$ time steps, $n=900$ states
per time step and $i=5400$ different inputs. The depth $d$ of the
generated pipeline is 93 steps. 
Using Equations~\eqref{eq:execution_time_always_flush}
and~\eqref{eq:execution_time_flush_after_time} which are repeated below, the total execution time of the optimisation problem can be computed.

The amount of clock cycles required if the pipeline is flushed after every state is:

\[
    T = t n \cdot (i+d)
\]

Which means that $T=680 \cdot 900 \cdot (5400 + 93) = 3\ 361\ 716\ 000$
clock cycles are required.  At $9$ MHz, this would take $\frac{T}{9 \cdot 10^{6}} \approx
373$ seconds.

Similarly, the amount clock cycles required if the pipeline is flushed after
every time step is:

\[
    T = t \cdot (n i + d)
\]

which means that $T=680 (800 \cdot 5400 + 93) = 3\ 304\ 863\ 240$ clock cycles are required.
At $9$ MHz would take roughly $367$ seconds to compute. Flushing the pipeline
after each time step would require some extra logic for keeping track of which
state each model execution corresponds to, but the amount of hardware required
is most likely negligible compared to what is used for the model and the
dynamic programming hardware itself.

For comparison, the original model runs in 251 seconds on a Intel Core i7-7500U
CPU with no parallelisation.


\section{Performance bottle necks}\label{sec:performance_bottle_necks}

After initial synthesis, some bottle necks were identified and worked around in order
to get an idea of the performance with some more work put into the project. Those bottle
necks and workarounds are presented in this section. All synthesis here is done at 20
fractional bits.

\subsection{Square root computation limits performance}

According to the synthesis tool, the slowest part of the model is the
computation of the square root which is a long combinatorial circuit. By
pipelining this computation, the performance might improve. However, on its
own, this turns out to not be the case, the minimum clock period is actually
increased slightly, from 118 ns to $122$ ns. The lack of an improvement is most
likely caused by the square root not being an actual bottle neck, while the
slight increase is possibly caused by the stochastic nature of \ac{HDL}
synthesis.

While the changes to clock frequency are mediocre, the changes to resource
usage are much better. The amount of LUT flip-flop pairs is reduced from 101
849 to 66 267. However, when the square root computation is clocked, the lookup table
uses block RAM instead of hardware which means that the total RAM usage changes
from 95 to 100 block RAMs.


\subsection{Divisions use a large amount of multiplexers}\label{sub:removing_division}

Division is currently implemented using the verilog division operator which
generates a fully combinatorial circuit for the division. This is inefficient,
both in terms of hardware usage and frequency, as each division in the model uses
between 1000 and 3000 multiplexers.

Almost all divisions in the model are divisions by constants which can be
replaced with multiplications by the inverse of the constant. In order to estimate
the effect of that modification, the model was synthesised with \textit{all} divisions
replaced by multiplications.

At 20 fractional bits, without the modified square root hardware, this resulted
in the amount of LUT flip-flop pairs used being reduced to $50\ 193$ and the
minimum clock period decreasing to 19.468 ns. This significant improvement
indicates that the divisions are the main bottle neck of the design.

Additionally, when both of these optimisations are combined, the minimum
frequency is reduced even further, to 10.346 ns and the number of LUT
flip-flop-pairs is reduced to 8 832. This indicates, as suspected that the
square root is suboptimal and that once the division bottle neck has been
resolved, optimisations to the square root computation can further improve
performance.



