\chapter{Theory}\label{chap:theory}

\acp{HEV} can be split into different categories
depending on the configuration of the powertrain. In serial \acp{HEV}, the combustion
engine drives a generator which provides electricity to the battery and the
electric motor that actually drives the wheels. A parallel \ac{HEV} uses the
combustion engine together with the electric motor to drive the wheels but does
not use the combustion engine for providing electric power. Finally, a combined
\ac{HEV} uses the combustion engine both for driving the wheels and for electric
power generation~\cite{sciarretta_guzzella_HEV_control_overview}.

\acp{HEV} can also be categorised by their degree of hybridisation, how much
power the electric motor contributes to the powertrain. Micro hybrids only use
the motor for quickly starting the combustion engine but not for providing
torque to the vehicle. In a mild hybrid, the electric motor contributes some
torque to the vehicle and can use regenerative braking to to save energy. Full
hybrids contribute more torque to the vehicle which reduces the size of the
combustion engine. Finally, plug-in hybrids have a larger battery which is
charged from the power grid and they can drive long distances using only
electric power\cite{govardhan_fundamentals_and_classification_of_hevs}. The
model used throughout this thesis models a mild \ac{HEV}.


In general, an \ac{HEV} has a set of control inputs which includes things like
brake force, electric motor torque, as well as combustion engine torque and gear. The
\ac{HEV} also has a set of state variables which includes things like velocity
or kinetic energy, battery state of charge, and fuel amount. In each state,
varying the control inputs takes the vehicle to a new future
state~\cite{sciarretta_guzzella_HEV_control_overview}.

\begin{figure}
    \centering
    \includegraphics[width=0.8\linewidth]{fig/state_space.pdf}
    \caption{Example of state space for a vehicle with a single state variable:
    velocity. The grey areas are disallowed states, the red path is a sample
    trajectory and the blue triangle indicates the reachable states in a single
    time step.}\label{fig:state_space}
\end{figure}

The set of states over time (or distance) can be seen as points in an $n+1$
dimensional space where $n$ is the amount of state variables.
Figure~\ref{fig:state_space} shows an example of this with a single state
variable: velocity. With the car in a specific state at time $t$, any of the
control inputs will put the car in another state that is reachable from the
current state within the limits of the control
inputs at time $t+ \Delta t$. This is represented by the blue triangle in
Figure~\ref{fig:state_space}.


The set of possible states is limited by physical characteristics of the
vehicle and road. For example, the speed limit constrains the upper and lower
bound on the vehicle velocity, the battery state of charge is bounded in order
to not damage the battery and the fuel tank can not have a negative amount of
fuel. Similarly, the control inputs are bounded, for example, the engine and
motor have upper bounds on the amount of torque they can provide.

There are also additional logical bounds on the control inputs, for example,
braking when the same braking force can be achieved using regenerative
braking is a waste of energy, and so is running the engine at the same time as
applying a braking force. By exploiting the latter point, the control inputs
for the engine and braking can be combined into one control input which if
negative indicates that the vehicle should brake and if positive, indicates that
the combustion engine should be used.



\section{Terminology}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.8\linewidth]{fig/layer_example.png}
    \caption{A graphical representation of the full state space}
    \label{fig:layer_structure}
\end{figure}

To make describing different parts of the optimisation problem easier, some
terms will be defined here. A graphical representation of these terms is shown
in Figure~\ref{fig:layer_structure}. The full optimisation problem takes place
in a state space consisting of distance, as well as other state variables like
velocity and state of charge. A slice of the state space at a specific distance
from the start will be refered to as a,
\textbf{time step} or just \textbf{step}. In each step, all state variables
have some values, a set of values for these variables will be referred to as a
\textbf{state}. A set of state variables which includes distance is called a
\textbf{full state}. When performing dynamic programming, the optimisation is
done ``in reverse'', meaning that the cost of the goal state is evaluated first
followed by the step before it. In order to avoid confusion, the
\textbf{next} time step will always refer to a future state, not necessarily
the next step to be evaluated.




\section{Dynamic programming}

The goal of this project was to implement hardware to search the full state
space for a set of control inputs that take the car from a starting state to an
end state as \textit{efficiently} as possible. Efficiently here means that a
tradeoff is made between fuel consumption and time taken to get to the
destination. In order to achieve this, dynamic programming was used.

Dynamic programming solves recursive optimisation problems where subproblems
are re-computed multiple times. This is done by computing and storing the
solution to each subproblem once and re-using that solution next time the same
subproblem is computed~\cite{cormen-introduction_to_algorithms}. In this case,
the optimization problem to be solved is finding a driving pattern which takes
the vehicle from an initial state to an end state as efficiently as possible.

In order to solve this problem, the cost of each point in the state
space is defined as the cost of the most efficient path from that point to the
destination. This is denoted by $v[d,x]$ where $d$ is the distance and $x$ is a
vector of state variables.

The most efficient path to the goal can then be computed by

\begin{equation}
    \label{eq:dp_formula}
    v[d,x] = \min_{i \in \texttt{states(d)}}(v[d+\Delta d,i] + \texttt{travelCost}((d,x), i))
\end{equation}

where the \texttt{travelCost} function denotes the cost of getting from one
state to another, and \texttt{states} is a set of the possible values of the
state variables at time $d$. We also define the cost in the goal node as $0$.

Unfortunately,~\eqref{eq:dp_formula} can not be computed directly because there
is an infinite amount of possible states and $\Delta d$ can be arbitrarily
small. In order to make the computation possible, the state- and input space
must be discretised into a fixed amount of points. In this case, distance has
690 discretisation steps, state of charge and velocity have 30 discretisation
steps each, the engine and motor torque also have 30 discrete steps each and
the gear has 6 separate steps.

Additionally, which states in the next time step can be reached from one state
is not known and must be calculated based on the drivetrain model. This
calculation is done by iterating through the possible control inputs to work
out the next state if that control input is applied. This means
that~\eqref{eq:dp_formula} can't easily be calculated directly. Instead, the
pseudocode shown in Listing~\ref{code:dp_pseudo} is used.

\lstinputlisting[
    label={code:dp_pseudo},
    caption={Pseudocode showing dynamic programming when reachable states are unknown},
    language=c
]{code/pseudo/cost_to_go}

The discretised control inputs will generally not result in a discrete
future state, which means that the cost of the resulting state must be computed
based on the surrounding states using interpolation.

\subsection{Forward search}

After the cost to go for each state has been evaluated by the dynamic
programming algorithm, the result can be refined by a forward search through
the state space based on the vehicle's present state.

Here, the discretised grid produced by dynamic programming is refined by
interpolating between the points in order to reach a more optimal result. This
computation is deemed fast enough to run on a general purpose CPU and is
therefore not optimised in this project.



\subsection{Parallelism}

In order for an algorithm to be executed efficiently on an FPGA, some degree of
parallelism is required. Luckily, the vehicle can only travel forward in time,
and can not travel between states in a single time step. Therefore, the cost of
each state in a single time step can be computed in parallel. When the cost of
every state in a single time step have been computed, the next time step can
not easily be processed until all states in the closest future time step are
fully calculated.






\section{The vehicle model}\label{sec:vehicle_model}

The vehicle model that was used throughout this thesis has two state variables:
battery charge and kinetic energy as well as three inputs: combustion engine
torque, electric motor torque, and gear. It also has a large amount of
configuration parameters which do not change between states or input, but still
affect the performance of the car, for example, torque values at a given motor
RPM, minimum and maximum state of charge, and gear ratios.

In any given state and set of inputs, the model computes the state that would
be reached using those inputs along with the cost of reaching that state.  All
the details of the model will not be explained in this thesis, instead, an
overview of the different computations that are performed will be given. For
more details on a similar model, see~\cite{jung_et_al_vehicle_model_overview}.

Figure~\ref{fig:powertrain} shows an overview of the components
in the vehicle being simulated. 

\begin{figure}[h]
    \centering
    \includegraphics[width=0.6\linewidth]{fig/powertrain.pdf}
    \caption{Overview of the modelled powertrain}\label{fig:powertrain}
\end{figure}

While fuel usage is important, if it is the only thing being optimised, the
best strategy is not to drive at all. Therefore, a tradeoff between fuel usage
and travel time is made, and the cost of a journey is defined as

\[
    m_{fuel} \cdot \gamma + T \cdot (1 - \gamma)
\]

where $m_{fuel}$ is the amount of fuel consumed and $T$ is the travel time.
Gamma is a tunable parameter which controls how much to prioritise fuel usage
over arrival time. If it is 0, only fuel usage will be considered, and if it is
1, only arrival time will be taken into account.

If time is used as a state variable, the cost of each state can be computed as

\begin{equation}\label{eq:cost_as_time}
    c_k = \dot{m}_{\text{fuel}} \cdot \Delta t \cdot \gamma + (1 - \gamma) \cdot \Delta t
\end{equation}

where $\dot{m}_{fuel}$ is the fuel usage per second and $\Delta t$ is the length of
the time step. However, distance is used as a state variable rather than time, which
means that the fuel usage calculation is slightly more complex.

The time used in a step $k$ is computed based on the velocity in the step
$v_{\text{avg,k}}$ and step distance $\Delta d$ as

\[
    \Delta t_k = \frac{\Delta d}{v_{\text{avg,k}}}
\]

This can then be combined with~\eqref{eq:cost_as_time} to calculate the cost of a
set of state variables as

\[
    c_k = \frac{\Delta d}{v_{\text{avg,k}}} \cdot (\gamma \cdot \dot{m}_{\text{fuel,k}} + (1-\gamma))
\]

This equation shows examples of a lot of the operations that make up the model.
$\gamma$ and $\Delta d$ are constants which are used as operands to arithmetic
operations.  $\dot{m}_{fuel}$ and $v_{avg,k}$ are non-constant variables which
are also arithmetic operands.

The current consumed by the electric motor in step $k$ is computed by

\[
    I_{bsg,k} = \frac{V_\text{oc}(\text{SOC}) - \sqrt{V_{\text{oc}}(\text{SOC})^2 - 4 \cdot R_0 P_\text{bsg,k}}}{2 \cdot R_0(\text{SOC})}
\]

where $V_{\text{oc}}$ and $R_0$ is the open circuit voltage and internal resistance of
the battery while $P_{\text{bsg,k}}$ is the desired power from the electric motor at
step $k$.  This equation showcases the use of both the square, and square root
functions as well as more arithmetic operations. Additionally, $V_{\text{oc}}(\text{SOC})$
and $R_{0}(\text{SOC})$ are functions of the current state of charge. In this model,
$V_\text{oc}(\text{SOC})$ and $R_{0}(\text{SOC})$ are computed by interpolation between values in
a lookup table. The model contains interpolation of both one dimensional and
two dimensional functions.

Another interesting computation is the calculation of the torque at the wheels which
is calculated by

\[
    T_{wheel} =
    \begin{dcases}
        \alpha \eta T_{pt}               & \text{if } T_{pt} \geq 0 \\
        \frac{\alpha}{\eta} \cdot T_{pt} & \text{otherwise}
    \end{dcases}
\]

where $T_{pt}$ is the torque input to the gearbox, $\alpha$ is the ratio
between motor rotation and wheel rotation and $\eta$ is the efficiency of the
gearbox. In order to compute this, logic to handle conditionals is required in
addition to arithmetic operations.


\section{Expressions, expression trees and expression graphs}


\begin{figure}[h]
    \centering
    \begin{tikzpicture}
        [scale=1,auto=left,every node/.style={circle,draw=black}]
        \node (na) at (1,10) {$a$};
        \node (nb) at (3,10) {$b$};
        \node (nc) at (5,10) {$c$};
        \node (napb) at (3,9) {$+$};
        \node (napbpc) at (5,8) {$*$};
        \foreach \from/\to in {na/napb, nb/napb, nc/napbpc, napb/napbpc}
          \draw[->] (\from) -- (\to);
    \end{tikzpicture}
    \caption{
        An expression tree which represents the expression $(a+b) \cdot c$
    }\label{fig:basic_expression_tree}
\end{figure}

An expression is a piece of a program which calculates a value when
executed~\cite{foldc:expression}. The model used throughout this project
produces several values, each of which can be computed by a single expression.
Expressions can be represented by expression trees which allows simple
conversion to and from the expression written in a programming language.

In an expression tree, each node represents a value which is either a leaf node
in the tree, or the result of another expression. A directed edge from one node
to another indicates that the first node is an input operand to the other.
Figure~\ref{fig:basic_expression_tree} shows an example of an expression tree
which represents the expression $(a + b) \cdot c$


The model used throughout this project computes three values as explained in
Section~\ref{sec:vehicle_model}. These values can be represented as three
separate expressions and therefore three expression trees. However, variables and re-used
values can not be represented in an expression tree. To see why, consider how the following
code would be represented:

\begin{lstlisting}
    c = a + b;
    d = a + c;
\end{lstlisting}

Here, $d$ can be represented as a single expression by expanding it to $d = a +
a + c$ which can then be converted to an expression tree. However, this removes
the information that the two $a$ values are the same.

Therefore, instead of an expression tree, an extension of an expression tree,
which for lack of a better term will be called an \textit{expression graph} is
used. Unlike an expression tree, an expression graph allows a node to be the
operand of multiple nodes. This means that an expression graph is a directed
acyclic graph rather than a tree which can be converted into an expression tree
through depth-first traversal. Figure~\ref{fig:expression_graph} shows an
example of an expression tree on the left and an equivalent expression graph on
the right.

\begin{figure}[h]
    \centering
    \begin{tikzpicture}
        [scale=1,auto=left,every node/.style={circle,draw=black}]
        \node (neta1) at (2,10) {$b$};
        \node (neta2) at (3,10) {$a$};
        \node (neta3) at (4,10) {$a$};
        \node (netb1) at (3,9) {$c$};
        \node (netc) at (4,8) {$d$};
        \foreach \from/\to in {neta1/netb1, neta2/netb1, neta3/netc, netb1/netc, neta3/netc}
          \draw[->] (\from) -- (\to);

        \node (nega) at (8,10) {$a$};
        \node (neg1) at (7,10) {$b$};
        \node (negb1) at (7,9) {$c$};
        \node (negc) at (8,8) {$d$};
        \foreach \from/\to in {nega/negb1, neg1/negb1, nega/negc, negb1/negc}
          \draw[->] (\from) -- (\to);
    \end{tikzpicture}
    \caption{
        An example of the difference between an expression tree (left) and expression graph (right). Both represent the calculation of \texttt{c = a + b; d = a + c}.
    }\label{fig:expression_graph}
\end{figure}



\section{Pipelining}\label{sub:pipelining_introduction}

When designing hardware for a large expression, the straight forward solution
of computing the whole expression using a single large combinatorial circuit is
often very inefficient. As an example, consider the expression $a + b + c$ which
could be implemented using two adders as shown in Figure~\ref{fig:sequential_addition}.

\begin{figure}[h]
    \centering
    \begin{tikzpicture}
        [scale=1,auto=left,every node/.style={rectangle,draw=black}]
        \node (na) at (1,10) {a};
        \node (nb) at (3,10) {b};
        \node (nc) at (5,10) {c};
        \node (napb) at (3,9) {+};
        \node (napbpc) at (5,8) {+};

        \foreach \from/\to in {na/napb, nb/napb, nc/napbpc, napb/napbpc}
          \draw[->] (\from) -- (\to);
    \end{tikzpicture}
    \caption{Graph representing a circuit for calculating $(a+b)+c.$}
    \label{fig:sequential_addition}
\end{figure}

In this case, the result of the second addition depends on the result of the
first, which means that the propagation delay, the delay between input and output,
of the circuit is the sum of the
propagation delays of each adder. Additionally, once the result has been
computed by the first adder, that adder is unused until the rest of the circuit
is done with its calculation. A common way to work around this and provide more
efficient hardware usage is to use pipelining where computations for a future
result are started before the previous result is done. For example, if $a + b +
c$ is to be computed for a large amount of values, $a_2 + b_2$ can be
calculated at the same time as $(a_1 + b_1) + c_1$. This is achieved
by adding registers after each calculation as shown in Figure~\ref{fig:pipelined_addition}.

\begin{figure}[h]
    \centering
    \begin{tikzpicture}
        [scale=1,auto=left,every node/.style={rectangle,draw=black}]
        \node (na) at (1,10) {a};
        \node (nb) at (3,10) {b};
        \node (nc) at (5,10) {c};
        \node (napb) at (3,9) {+};
        \node (dapb) at (3,8) {D};
        \node (napbpc) at (5,7) {+};

        \foreach \from/\to in {na/napb, nb/napb, nc/napbpc, napb/dapb, dapb/napbpc}
          \draw[->] (\from) -- (\to);
    \end{tikzpicture}
    \caption{Graph representing a pipelined circuit for calculating $(a+b)+c.$
    }\label{fig:pipelined_addition}
\end{figure}


This does not speed up the individual computations, in fact they generally become
slower when pipelining because the time used in each stage has to be uniform
which means that the whole pipeline is limited by the slowest step. However,
pipelining increases throughput as one new computation can be started every
clock cycle.

\subsection{Pipeline depth}\label{sec:theory:pipeline_depth}


One issue which remains with the circuit shown in
Figure~\ref{fig:pipelined_addition} is that the value of $(a_1 + b_1)$ will be
added to $c_2$ rather than $c_1$ as one would expect.  In order to fix that, a
delay register must be added between $c$ and the addition node.

In order to calculate the required delay between an output and an input of an
expression, the \textit{depth} of a node is defined as the earliest time at
which the value of the node can be computed. The value of nodes with no inputs
can be ``computed'' at any time, and as such, their depth is 0. The value of
all other nodes can be computed as soon as the output of all preceding nodes is
available. The depth $d_n$ of node n can then be calculated by the following equation
where $t_i$ is the computation time of node $i$.

\begin{equation}
    d_n = \max_{i \in \text{operands}(n)}(d_i + t_i)
\end{equation}

When the depth is known, the required delay slots between a node and one of its
operands can be calculated as $d_n - d_i - 1$ where $d_n$ is the depth of the node
and $d_i$ is the depth of the operand.


\section{Hardware for dynamic programming}\label{sec:dp_hardware}

In this section, the structure of the hardware that could be used to execute the
model and perform dynamic programming is presented along with the theoretical
performance characteristics that impact the usefulness of the generated model
hardware.

The algorithm which is to be performed by the hardware the one listed in
Listing~\ref{code:dp_pseudo}. This is repeated below for reference:

\lstinputlisting[language=c]{code/pseudo/cost_to_go}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\linewidth]{fig/hardware_overview.pdf}
    \caption{Overview of the dynamic programming hardware}\label{fig:hardware_overview}
\end{figure}



An overview of the hardware is shown in
Figure~\ref{fig:hardware_overview}. This  The middle
for-loop is emulated by the \textit{state\_iterator} module which iterates
through all the available states in the model. In each state, all possible
inputs are iterated through by the \textit{input\_generator} module which
emulates the innermost for-loop.

The inputs, along with the state are passed to the model which computes the
resulting state from those inputs, along with the cost of those inputs which is
then passed to a \textit{RAM} module where the cost of previous
states that are closest to the reached state are stored. The cost of the reached
state is calculated by interpolating those values, and the result together with
the cost of the inputs makes up the total cost of travelling from the current
state to the end state, starting with the current inputs.

This cost is then passed to the \textit{minimizer} module which selects
the minimum cost from all inputs in this state. Once all the inputs for one
state have been tested, the \textit{state\_summarizer} module writes the cost
of the state to RAM and sends a signal to the \textit{state\_iterator} to
increment the state to check.

The model execution in a single state is pipelined which means that the input
generator sends one input to the model every clock cycle. Once each input in a
single state has been evaluated, the pipeline can either be flushed before
starting the next state, or the pipeline can start calculating the cost of the
next state right away. In the second case, the pipeline still has to be flushed
when changing between time steps because the output of the model depends on the
cost of states in the previous time step.

\subsection{Execution time}\label{sec:execution_time}

The amount of steps required to solve an optimisation problem with the above
hardware depends on the size of the optimisation problem. Consider a problem
with $t$ time steps, $n$ possible states, and $i$ inputs where the hardware
to calculate the cost of a single input has a pipeline depth of $d$.

As mentioned before, one input can be evaluated per clock cycle in a single state
which means that the time required to compute the cost for $k$ inputs is $k+d$.

In the case where the pipeline is flushed after each state, this means that the
amount of clock cycles required to complete the optimisation is given by
following equation:

\begin{equation}\label{eq:execution_time_always_flush}
    T = t  n \cdot (i+d)
\end{equation}

Similarly, if the pipeline is only flushed after each time step, the amount of clock
cycles required is given by the following equation:

\begin{equation}\label{eq:execution_time_flush_after_time}
    T = t \cdot (n i + d)
\end{equation}

Thus, if the pipeline depth is small compared to the amount of inputs, its impact
is negligible in both cases.
