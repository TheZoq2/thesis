\chapter{Introduction}

By adapting the driving pattern of a vehicle to the road ahead, the fuel
consumption and emissions of the vehicle can be reduced. This is true for both
conventional non-hybrid vehicles~\cite{helstrom_et_al_look_ahead_for_trucks}
and especially \acp{HEV} where energy can be stored in the battery for future
use.  Dynamic
programming is one method which has been used successfully for calculating an
efficient split between the use of the electric motor and combustion
engine~\cite{perez_et_al_optimmisation_of_hev_using_dp}.
However, the runtime of such an algorithm grows
exponentially with the number of inputs and state variables which makes its use
for real time calculations in an \ac{HEV} difficult on a general purpose
CPU~\cite{sciarretta_guzzella_HEV_control_overview}.\@

Many dynamic programming problems have a high degree of parallelism which
can be exploited to reduce the computation time required to solve the problem.
\acp{FPGA} can use pipelining to exploit the parallelism in a problem in order
to quickly solve it while efficiently using the available hardware. This
thesis will investigate executing the \ac{HEV} optimisation algorithms on an
\ac{FPGA} in order to run the optimisation in real time while the vehicle is
driving. To achieve this, two things have to be done: first, the
model of the vehicle drive train must be implemented on an \ac{FPGA} and
second, the model needs to be run by the dynamic programming algorithm, also on
the \ac{FPGA}.

Every vehicle has a unique vehicle model which is generally written in a
general purpose programming language by engineers who are not familiar with
hardware design. While the model could be manually converted to an \ac{HDL},
this would take extra time and resources. Instead, this could be done
automatically by a tool which converts such a model written in C++ to FPGA
hardware with minimal knowledge of hardware design. This tool is the primary
focus of this thesis.

This thesis will also explore possible modifications to the models which
improve the runtime and reduce the amount of required hardware when running on
an \ac{FPGA}.\@ This might include things like using fixed point numbers rather
than floating point numbers and converting division by constants to
multiplications.

\section{Motivation}

Dynamic programming has been used to optimise the driving pattern of both
conventional vehicles~\cite{helstrom_et_al_look_ahead_for_trucks} and \acp{HEV}.
However, while it can run in real time on sequential hardware in a non-hybrid
vehicle, the computational complexity grows exponentially with the amount of
control inputs~\cite{sciarretta_guzzella_HEV_control_overview} which makes its
usage difficult for \acp{HEV}.

The structure of the dynamic programming algorithm allows most of the long
running computations to be pipelined in order to massively increase the
throughput of the system. This should make it possible to run the algorithm in
real time on an \ac{FPGA} which would reduce the emissions and improve the fuel
economy of an \ac{HEV}. The parallelism could also be exploited by other
hardware, such as \acp{GPU}, but they consume significantly more power than \acp{FPGA}.

While the primary goal of this project is to optimise the execution of dynamic
programming optimisation of \acp{HEV}, the methods discussed should be
applicable in other fields which also utilise dynamic programming and have real
time requirements.


\section{Related work}

Y. Hu and P. Georgiou present an \ac{FPGA} adaptation of a dynamic programming
algorithm for genome sequencing~\cite{hu_georgiou_2013}. The implementation
presented by the paper is very specific to the genome sequencing problem which
means that it is not of much use in this project.

Sean O Settle used OpenCL to perform genome sequencing using dynamic
programming on FPGAs~\cite{settle_fpga_dp_opencl}. While OpenCL allows FPGA
programming without hardware design knowledge, it still requires knowledge of
parallelism and OpenCL itself. This means that with the method presented, some
extra effort would still be required to convert a vehicle model to FPGA
hardware.

Several studies have been conducted on paralleling dynamic programming using
other hardware. M. Miyazaki and S. Matsumae present a pipelined dynamic
programming implementation for \acp{GPU} which is able to produce one output
result per clock cycle but is limited by memory bandwidth on the
\ac{GPU}~\cite{miyazaki_matsummae_pipelined_dp_gpu}. The problem they are
solving assumes that the value at position $i$ depends on the $k$ previous
values which makes it more difficult to parallelise than the problem solved in
this project. M. Cruz, P. Tomás and N. Roma present a method for efficiently
performing dynamic programming on a very large instruction word
processor~\cite{cruz_et_al_vliw_dp}.

In 2015 R. Nane et.al.\ conducted a survey of the available high level
synthesis tools. The article presents a long list of available tools as well as
performance evaluation of a subset of the
tools.~\cite{nane_et_al-hls_overview}. However, most of the non-commercial
tools presented do not support converting C++ to hardware.


\section{Aim}

In order to run the required calculations on an FPGA, two main things have to
happen. First, hardware to perform dynamic programming has to be developed.
Second, the vehicle drivetrain models have to be implemented in hardware in
such a way that the calculations can run in real time and the previously
mentioned dynamic programming hardware can execute them. The primary focus of
this project was the model conversion. Potential dynamic programming hardware was
considered in order to evaluate the theoretical performance of the full system,
but it was not implemented.

The models are often written in a general purpose language like C, C++ or
Matlab which can not directly be executed on an \ac{FPGA}. Therefore, a tool to
convert these models from C++ to an \ac{HDL} had to be developed.

Finally, possible modifications to the optimisation problem that improve the
execution time or resource usage on an \ac{FPGA} will be investigated. These
modifications may include both changes to the vehicle model such as replacing
maps with functions, as well as modifications to the full optimisation proble.

A model of a mild \ac{HEV} will be used to evaluate the performance of the
resulting hardware.

\section{Research questions}

\begin{enumerate}
    \item{How can a vehicle drivetrain model automatically be converted to \ac{FPGA} hardware?}
    \item{Can an \ac{FPGA} implementation of the \ac{HEV} optimisation algorithm be run in real time?}
    \item{What modifications can be made to the optimisation problem to improve the
        calculation speed and resource usage?\label{q:model_optimisations}}
    \item{What impact do those modifications have on the resulting vehicle fuel consumption
        and \ac{FPGA} resource usage?\label{q:model_optimisation_results}}
\end{enumerate}


\section{Delimitations}

Because \ac{HEV} models are often trade secrets and in order to limit the scope
of the project, only one sample model was considered.

Additionally, making the \ac{HDL} conversion 100\% automatic was not a goal,
instead, the conversion should do most of the work that requires knowledge of
hardware design. As pre-processing, a software developer will have to make some
changes to the software, like replacing types of variables, possibly specifying
their bounds, and in some cases replace programming constructs that can not be
automatically converted.

This project will primarily consider non-plug-in \acp{HEV} which have an
internal battery that can not be charged from the power grid as that allows for
simpler calculations when determining the end goal state of the vehicle.


