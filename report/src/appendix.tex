\begin{appendices}
    \chapter{Fractional word length optimisation}\label{sec:fractional_word_length_optimisation}

    As mentioned in Section~\ref{sec:word_length_selection}, the integer word
    length of all variables was optimised, but the fractional word length was not.
    However, some attempts at doing that were made and those attempts are
    documented in this chapter.

    In order to select the size of each register in the design, a slightly modified
    version of the algorithm presented in by N.~Doi et.al.\ was
    used~\cite{doi_et_al_minimization_of_fractional_wordlength}.

    Their method requires requires a maximum allowable error $E^{\max}_m$ for
    each output $m$ as well as a function $E_m(L)$ which computes the error in
    output $m$ where $L$ is a vector representing the word length of each
    variable.

    Using these, a set of bit lengths which uses the lowest amount of bits possible
    while ensuring that the output errors are not to large can be found by solving
    the following optimisation problem where $n$ is the total amount of nodes

    \begin{equation}
        \begin{aligned}
            & \underset{x}{\text{minimise}} & & \sum_{i=0}^n L[i] \\
            & \text{subject to} & & E_m(L) < E_m^{\max} \forall m
        \end{aligned}
    \end{equation}

    The next subsections presents different sources of errors and ways of
    calculating their impact on the error in the outputs.


    \section{Truncation errors}\label{sub:truncation_errors}

    In any node, the resulting value of the local operation can be truncated to a
    lower amount of bits. Each node has a maximum precision of its output which
    depends on the precision of its inputs. For example, an addition of two values
    with $n$ fractional bits has a precision of $2^n$. Adding more than $n$ bits to
    the output will not improve the precision, but bits can be removed in order to
    reduce the amount of bits used at the cost of precision.

    In the worst case truncating a value to $k$ bits when the output of the
    local computation has $n$ bits adds an additional error, a truncation
    error, that is $E_R = 2^{-k} - 2^{-n}$. Thus, the truncation error in node
    $m$ with an operand $k$ can be computed as $E^R_m(L) = 2^{-L[m]} -
    2^{-L[k]}$.

    However, this calculation only works if $L[m]$ is smaller or equal to
    $L[k]$. Therefore, another constraint must be added to the optimisation which
    says that $L[m] \leq L[k]$. This means that the final optimisation problem to be
    solved is

    \begin{equation}\label{eq:frac_word_length_optimisation}
        \begin{aligned}
            & \underset{x}{\text{minimise}} & & \sum_{i=0}^n L[i] & &\\
            & \text{subject to} & & E_m(L) < E_m^{\max} & \forall & m, \\
            &                   & & L[m] \leq L[k] & \forall & (m,k) \text{ where } k \succcurlyeq m
        \end{aligned}
    \end{equation}

    where $k \succcurlyeq m$ means that the output of node $k$ is truncated at
    node $m$.

    \section{Propagation errors}

    Additionally, errors propagate as operations are performed. A variable $x$ can
    be estimated by $\bar{x} + \Delta x$ where $\bar{x}$ is an estimation of x and
    $\Delta x$ is the error in that estimation.

    When adding two variables, the result is

    \[
        y = \bar{x_1} + \Delta x_1 + \bar{y_2} + \Delta x_2 \Leftrightarrow
            E_p(y) = \Delta x_1 + \Delta x_2
    \]

    which means that the propagation error $E^p_m$ in the result of an addition
    is the sum of the errors of the operands.

    Similarly, the result of a multiplication is


    \[
        y = (\bar{x_1} + \Delta x_1) \cdot (\bar{y_2} + \Delta x_2) \Leftrightarrow
            E_p(y) = \bar{x_1} \Delta x_2 + \bar{x_2} \Delta x_1
                   + \Delta x_1 \Delta x_2
    \]

    which means that the propagation error of a multiplication is the sum of the
    errors multiplied by the operands. We can assume that the errors are small,
    which means that we can ignore the $\Delta x_1 \Delta x_2$ term.

    Similar arguments can be used to calculate the errors resulting from other
    operations which means that the propagation error of a node $m$ is a function
    of the errors of all its operands.

    \section{Output error as a function of bit lengths}

    In order to optimise the amount of bits in each variable, the error in the
    output as a function of the bit length must be computed. This can be done
    recursively.  The error in a node with no input operands is the truncation
    error of the input at the current bit length.

    For other nodes, the error is the propagation error of the inputs plus possible
    truncation errors in the node:

    \[
        E_m(L) = E^R_m(L) + E^p_m(L)
    \]


    \section{Implementation}

    In order to perform this calculation, the node class was augmented with  a
    function called \textit{error\_function}. It returns a closure
    which computes $E_m(L)$, the maximum error in the node given a list of the
    bit length of each parent variable.

    As explained in Section~\ref{sub:truncation_errors}, adding more bits than
    the amount of bits in the output of a node does not improve precision.
    Because of this, a second function called
    \textit{smaller\_word\_constraints} was added to each node. This function
    returns a list of nodes which the node must be smaller or equal to.

    Using these functions, an optimal value for the amount of bits for each
    variable should be computable by solving the optimisation
    problem~\eqref{eq:frac_word_length_optimisation}. Initially, the
    \textit{optimize.minimize}\footnote{
        \url{https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html}
    } function in the python library \textit{scipy}\footnote{\url{scipy.org}}
    was used, and worked for small examples but did not converge when executed
    on the full model. Another attempt was made, this time using the
    \textit{optimize.differential\_evolution} function which may have produced
    useful results. However, there was not enough time to explore this further.



\end{appendices}
