\chapter{Discussion}

In this section, the results presented in the previous chapter will be discussed and analysed.

\section{Execution time}\label{sec:discussion:execution_time}

It is clear from the results that the optimisation problem can not be solved in
real time using the current output from the \ac{HDL} converter. However, there
are some things which could improve performance. First, the
frequency is bottlenecked by one or a few nodes which take a long time to
compute and optimising those would improve the performance of the whole system.
Some of those optimisations were discussed in
Section~\ref{sec:performance_bottle_necks}. With more optimisation and support
for more than one pipeline stage per node, it should be possible to run the
design at the maximum frequency of the \ac{FPGA} which is generally around 500 MHz.

Another thing which would improve the performance of the design is to evaluate
multiple inputs in parallel. The amount of parallelism is currently limited by
the fact that one state cost must be read in every model execution and RAM is
limited to one or a few read ports. There are several methods to get around
that, for example replication, banking or more advanced methods such as the one
presented in~\cite{laforest_et_al_multi_ported_memories}.

\subsection{Usefulness with further optimisations}

Equations~\eqref{eq:execution_time_always_flush}
and~\eqref{eq:execution_time_flush_after_time} can easily be modified to to
take parallelism into account. As a reminder, $T$ represents the total amount of
clock cycles required, $t$ is the amount of time steps in the model, $n$ is the
amount of states per time step, and $i$ is the amount of inputs to evaluate per
state. Finally, $d$ is the depth of the pipeline. For simplicity,
only the hardware where the pipeline is flushed after each time step is considered which with $p$
concurrent pipelines would require

\begin{equation}\label{eq:time_with_parallel_pipelines}
    T = t\cdot\big(\frac{ni}{p} + d\big)
\end{equation}

clock cycles to compute.

Naturally, the FPGA area required would also increase by a factor $p$.

At a given frequency $f$, the time $\tau$ required to complete $T$ operations is given by

\[
    \tau = \frac{T}{f}
\]

This, along with equation~\eqref{eq:time_with_parallel_pipelines} can be used
to calculate the amount of concurrent pipelines $p$ that would be required to
run the optimisation in a specified amount of time at a given clock frequency
as is shown in the following equation:

\[
    \tau = \frac{T}{f} = \frac{t \cdot \big(\frac{ni}{p} + d \big)}{f}
    \Leftrightarrow \frac{f \tau}{t} = \big( \frac{ni}{p} + d \big)
    \Leftrightarrow \frac{ \frac{f\tau}{t} - d }{ni} = \frac{1}{p}
    \Leftrightarrow \frac{ni}{ \frac{f\tau}{t} - d } = p
\]

Therefore, to run one full optimisation in one second at 100 MHz which is roughly the
clock speed achieved when performing the optimisations mentioned in
Section~\ref{sec:performance_bottle_necks} would require

\[
    p = \frac{900 \cdot 5400}{\frac{1\cdot10^8}{680}-93} = 33.07
\]

concurrent pipelines.

Naturally, fewer concurrent pipelines would be required if more optimisations are performed, for
example, to run one full optimisation in one second at a clock frequency of 500 MHz, only

\[
    p = \frac{900 \cdot 5400}{\frac{5\cdot10^8}{680}-93} = 6.6
\]

concurrent pipelines would be required.



\subsection{Usefulness in its current state}

Another thing to consider is wether or not the current implementation is
usefull in some cases, or if it has to have the above improvements before it can be useful.
When comparing the execution time of the FPGA with the computation time on a
single core CPU, the \ac{FPGA} may seem completely pointless without
optimisations.  However, the \ac{FPGA} still has an advantage: the computation
time of the FPGA model is largely unaffected by the complexity of the model.
The CPU implementation would take twice as long to compute each
iteration if the model execution time doubled, but the \ac{FPGA} would barely
be affected at all. Changes in model complexity would only affect the pipeline depth
and resource usage, not the execution time.

This means that optimisation problems with fewer states but more complex
models than the one used in this project would benefit more from \ac{FPGA}
hardware.



\section{Model modifications}\label{sec:model_modifications}

In this section, some modifications to the model which could be made in order to
improve the model performance on an FPGA will be discussed.

\subsection{Replace divisions with multiplication}

As can be seen in Section~\ref{sub:removing_division}, the resource usage and
maximum frequency can be substantially improved by avoiding divisions. The
divisions used in the model come in three categories: division by constants,
division by values from lookup tables, and finally, division by calculated
values.

Division by constants could easily be optimised by simply pre-calculating
$\frac{1}{c}$ and replacing the division by a multiplication. This could even
be done automatically by the conversion tool with some small modifications.

The second case, where the values to be divided by comes from the result of a
function lookup, a similar method could be used. Instead of storing $f(x)$, the
value of $\frac{1}{f(x)}$ could be stored and looked up directly. If $f(x)$ is
only used for division, this would require no additional resources, otherwise
it would use an additional block RAM to store the inverted values.

The final case, where the divisor is computed directly can not be converted
into simple multiplication. This would most likely mean that those divisions
still restrict the maximum frequency. However, more efficient division circuits
are available, especially if pipelining can be used.




\subsection{Avoid very large or small values}

As has been shown, the model contains some values which are small and require
a lot of fractional bits for storage. However, it also contains a lot of very large
values which are likely stored at much higher precision than is required. If subesxpressions
where all values are large can be identified, it might be possible to temporarily scale them
down while performing the calculation and then scaling them back up when they are combined
with values which require more precision.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.6\linewidth]{fig/value_scaling_example.pdf}
    \caption{Example of subexpression where values are large enough to most likely
    be stored at more precision than required}\label{fig:overly_large_values}
\end{figure}

For example, consider the expression shown in
Figure~\ref{fig:overly_large_values}. Here, it might be possible to scale $V$
and $x$ to much smaller values, perform the calculation, and then scale the
output back up before performing the square root operation. If the amount to
scale by is a power of two, the scaling does not affect resource usage much, apart
from adding an additional pipeline register.

It should be noted, that this will not be useful if per-node fractional word
length optimisation is implemented, as that would automatically scale values
according to the required precision.



\subsection{Use resources more effectively}

While most hardware in an FPGA is completely reconfigurable, they also have
dedicated hardware for things like RAM and multipliers. As these can not be
dynamically reconfigured to handle varying bit sizes, a computation that uses
some of the device can use all of it without additional penalty. For example,
the lookup tables that are used could have an even power of two number of
entries in order to fit better in block RAMs. The size could also be rounded
down if the data is close to fitting in a smaller RAM, at the cost of some output
quality.

\subsection{Use velocity as a state variable}

The current model uses the kinetic energy of the vehicle as a state variable
while a lot of the computations performed use the velocity. This requires the
computation of the square root of a value which is currently a performance bottle
neck. It might be possible to use the velocity as a state variable and compute
the kinetic energy using a multiplier instead, which would get rid of one of
the expensive square root computations. However, this would make the
discretisation steps non-linear which introduces additional complexity,
especially when interpolating between states to determine the cost of a
previous state.
