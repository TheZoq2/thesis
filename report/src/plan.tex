\chapter{Planning}


\section{Selecting bit lengths}

Since \acp{FPGA} typically have 18 bit multipliers\todo{source}, all values will initially be 18 bits long. Of course, this will change if the results are unsatisfactory.

With a bit length determined, the location of the fractional bit has to be
selected. This is done by adding a min- and max-value to all nodes which is
computed statically when constructing the expression tree. Using this min and
max value, the location of the fractional bit can easily be computed such that
the maximum precision is used for each node.

In practice, this will mean that all operations must shift inputs into a common
space which might be tricky when operands are very different.

\section{old}

The project will consist of three main parts:

\begin{itemize}
    \item{The model to \ac{HDL} converter}
    \item{The dynamic programming hardware}
    \item{Analysis of model changes for performance optimisation}
\end{itemize}

These can be implemented mostly in parallel since there is no direct dependence
between them apart from the fact that data must be shared correctly. However,
if the dynamic programming module and converted \ac{HDL} code is general enough, the
data types should be easily changeable.

\section{Model to \ac{HDL} converter}\label{sec:hdl_converter}

\ac{HDL} code for the drivetrain models has to be generated from the high level
language code. One way of doing this is to use one of the many off the shelf
\ac{HLS} tools. This would probably be the quickest option but its less
flexible than the alternatives. Most of the tools also convert C, or a subset
of C~\cite{nane_et_al-hls_overview} rather than C++ which the current model is
written in. This is not a huge issue as the demo model is quite simple but it
would still mean that the model has to be modified.

Another option is to do what Erik Bertilsson's tool does and replace the data
types in the program with more advanced objects that generate a signal flow
graph and \ac{HDL} code rather than perform the actual computations. While this
is undoubtedly more work than an off the shelf solution it also provides a lot
more control over exactly what is being generated.  Additionally, it might help
with inferring things like maximum and minimum values for optimisation as will
be discussed in Section~\ref{sec:model_modifications}.

A third option for the \ac{HDL} converter is to leverage a pre-existing
compiler to do part of the parsing and then parse the intermediate
representation of the compiler, for example LLVM IR or GIMPLE which was done by
Arató and Suba~\cite{arato_suba_hls_from_gimple}. This intermediate
representation can then be converted to a \ac{HDL}. The advantage of this
approach is that we can leverage all the optimisations performed by an off the
shelf compiler.

One major simplification is that the model is only composed of mathematical
calculations using floating point values, some if-statements and function calls
to interpolation functions. This means that writing a custom tool to perform
the conversion is not nearly as much work as writing a general \ac{HLS} tool.

\section{Hardware for dynamic programming}

The hardware that performs all the operations required for dynamic programming
has to be constructed. This includes hardware for scheduling
calculation of nodes to the available pipelines and a memory controller. At a
high level, this means that the algorithm shown in Listing~\ref{code:dp_pseudo}
should be implemented in hardware. For reference, the code repeated below.

\lstinputlisting[
    label={code:dp_pseudo_relisting},
    language=c++,
]{code/pseudo/cost_to_go}

The outer for-loop has to be run sequentially since the results of layer $n$
depends on layer $n+1$, however, there are no dependencies between calculations
in the middle loop. There are also no dependencies in the innermost loop apart
from the shared minimum value calculation.  Additionally, the
calculation of the input effect requires evaluating the results of the input on
the drivetrain model of the vehicle which contains a long sequence of
sequential operations.

This makes the inner loop suitable for pipelining where one iteration of the
inner clock cycle would be started and another would be finished each clock cycle.

\subsection{Concurrent pipelines}

In order to further increase the throughput of the system, many such pipelines
could be run in parallel if some issues can be worked around.  First, with many
costs being calculated in parallel, the minimum value operation would need to
handle multiple input values which limits the possible parallelism of the
innermost loop. Furthermore, the bandwidth of the memory is limited which means
that only a finite amount of node costs can be read simultaneously. One way to
get around that might be to somehow sync the pipelines to ensure that they try
to read the same values in the same clock cycle. However, that would be
difficult since it is not known ahead of time which new state a certain input
will yield.


\section{Model analysis and optimisation}\label{sec:model_modifications}

While the whole system could use floats for all values, this would require a
lot more hardware than fixed point numbers. Therefore, using fixed point values
is desirable.  In order to find the required size of those numbers, some
analysis will have to be performed.

Depending on what we want to achieve, we could take two approaches here. Either
we analyse the requirements in order to achieve nearly full parity with the model
using floating point values, or we analyse the requirements to achieve a result
that is \textit{good enough}.

One important property that can be analysed is the minimum and maximum values of
the variables in the model. Most of this analysis can probably be done by
keeping track of the minimum and maximum values of variables when running the
high level version of the model.

While this is convenient, it has some big drawbacks as well. It does not use
domain knowlege about the variables in the system. For example, anyone could
tell you that the speed of a car driving legally in Sweden is between 0 and 120 km/h.
Another drawback is that the quality of the analysis depends on the quality of
the test input being used.

\subsection{Static analysis}\label{sub:static_analyssis}

Value ranges can also be determined statically by the compiler by augmenting
the types used in the program with minimum and maximum ranges. The operators on
those types could then be defined to modify the ranges accordingly, as shown
below


\lstinputlisting[language=c++]{code/cpp/static_sizes.cpp}

This approach could be combined with the type based synthesis mentioned in
Section~\ref{sec:hdl_converter}.

Unfortunately C++ templates do not allow float values in templates, so the
min/max analysis would have to be performed using some sort of fix point
values.

One solution to get around this is to not use type-level values for bounds and
instead use runtime members of the node type. The sizes would still be
statically known when generating the \ac{HDL} code and it would allow floats
for bounds while reducing the amount of clutter in the definitions of the types
as shown in the code below. However, it would not allow specifying bounds for
variables that are inputs to functions. In practice however, this is a small
issue because the code contains very few function calls and the bounds of those
parammeters could be specified using the template method.

\lstinputlisting[language=c++]{code/cpp/static_sizes_runtime.cpp}

\subsection{Static analysis vs.\ dynamic analysis}

As with everything, there are advantages and disadvantages to both static and
dynamic analysis. With enough data, dynamic analysis allows inference of
variable bounds and required precision without any input from the user.
However, for the results to be good, the test data used to perform the analysis
must ensure that the bounds of all variables are hit, otherwise some data paths
may overflow. It might be possible to prevent this by adding guard bits and
saturation but correct results can not be guaranteed. Depending on the
situation, this may or may not be a big issue but I would argue that an
overflow in a car engine management system is something that should preferably
be guaranteed to not happen.

On the other hand, static analysis requires more input from a user when
developing the model. If nothing else, the code has to be rewritten to contain
information about variable bounds. Additionally, static analysis will most
likely give a pessimistic upper and lower bound for variables where
dependencies between values are not expressed in the bounds. For example, a
sane model should never break and run the engines at the same time but that
might be difficult to express statically






\section{A method for static C++ to \ac{HDL} conversion}\label{sec:static_cpp_to_hdl}

Here is one possible method for statically converting C++ to HDL using custom
types. Each value (constant, result of operation etc.) in the program is a
\textit{Node}. Nodes can either be values, like variables or constants or
function invocations. In the case of function invocations, the node has at
least one parent which gives the input data. By replacing the variables in the
program with these nodes, similarly to what was shown in
Section~\ref{sub:static_analyssis} we can construct a graph representing the
calculations. The graph for \(a + b + b\) is shown in Figure~\ref{fig:aplusb_graph}.

\begin{figure}
    \begin{tikzpicture}
        [scale=1,auto=left,every node/.style={circle,draw=black}]
        \node (na) at (1,10) {a};
        \node (nb) at (3,10) {b};
        \node (napb) at (2,9) {a + b};
        \node (napbpb) at (3,7) {a+b+b};

        \foreach \from/\to in {na/napb, nb/napb, napb/napbpb, nb/napbpb}
          \draw[->] (\from) -- (\to);
    \end{tikzpicture}
    \caption{Signal flow graph for addition of a+b+b}
    \label{fig:aplusb_graph}
\end{figure}

The value of $a + b$ can be computed at any time but the computation of $a+b+b$ can not start
before the previous calculation is finished. If b is a variable and we want to pipeline
the computation, we need to add a register for storing the first value of $b$ while
$a+b$ is being calculated.

This can be done by adding a \textit{depth} variable to the nodes which
corresponds to the \textit{longest} distance between the node and the output
node. In the example above, the node $a+b+b$ is at depth 0 and a+b is at depth
1. Node $b$ is needed at both depth 1, for the output calculation and at depth
2 for the $a+b$ calculation and therefore, its depth is 2. Finally, node a is
at depth 2.

The amount of pipeline registers that must be added between two nodes is then
the difference in depth between those nodes.


\section{Expected results}

In this section, some expected results from the project will be presented.

I expect a few different results from the project. Of course, some code should be
produced which includes the C++ to HDL converter which can convert a full vehicle model with
some optimizations. Additionally, the dynamic programming hardware
should support efficiently running that model, preferably with multiple
concurrent pipelines.

Further, I expect the hardware to be able to perform the computations fast
enough to be run in real time on a vehicle, at least when adding concurrent
pipelines.

Finally, there will probably be many possible small optimisations to the model
that improve the resource usage, some, like replacing division by constants
with multiplication won't affect the results at all. Other optimisations like
reducing the amount of bits used for values will surely impact the results, in
that case I will try to find a relationship between quality of results and
variable bit length.

\section{Time plan}

In this section, a list of tasks that make up the work on this thesis are presented. The tasks are described below and the estimated time they will take to complete is shown in Figure~\ref{fig:gantt}

\subsection{Tasks}

\begin{itemize}
    \item{\textbf{Write basic C++ to HDL converter}:
        An initial version of the C++ to \ac{HDL} converter will be developed.
        This version will be a proof of concept and won't necessarily support
        all constructs used in the model.
    }
    \item{\textbf{Write basic DP hardware}:
        An initial version of the hardware that performs dynamic programming
        will be developed. This version will act as an initial proof of concept
        that can be iterated on.
    }
    \item{\textbf{Forward search}:
        Setting up a way to run forward search through the cost-to-go map generated
        by the hardware.
    }
    \item{\textbf{Integrate DP with HDL converter}:
        The DP module and HDL converter will be combined to ensure that they
        work together
    }
    \item{\textbf{Set up FPGA development environment}:
        In order to test the code outside simulations, the software and
        hardware for running code on an FPGA has to be set up
    }
    \item{\textbf{Improve DP hardware}:
        The basic DP hardware will be iteratively improved to run faster and
        use less hardware. This will most likely also include adding support
        for concurrent pipelines
    }
    \item{\textbf{Optimize HDL converter}:
        The HDL converter will be improved to produce better hardware and
        support more language constructs
    }
    \item{\textbf{Write model performance analysis tool}:
        In order to answer question~\ref{q:model_optimisations}
        and~\ref{q:model_optimisation_results}, a tool to analyse and compare
        the results and performance of a model to a baseline must be developed
    }
    \item{\textbf{Analyse optimization effects}:
        Once the analysis tool has been written and optimisations have been
        performed, their results have to be analysed before being put in the
        final report
    }
    \item{\textbf{Buffer time}: An extra week that can be spent when other task takes
        longer than expected
    }
\end{itemize}
\subsubsection*{Report}

Here, all activities surrounding writing the final report are described. Most
of them simply consist of writing one chapter in the report which means they
are not given any further description.

\begin{itemize}
    \item{\textbf{Write abstract}}
    \item{\textbf{Write introduction}}
    \item{\textbf{Write theory chapter}}
    \item{\textbf{Improve previous chapters}:
        After having worked on the project for a wile, it is likely that what I
        originally wrote in the initial chapters will be partially wrong. Here, those
        issues will be fixed
    }
    \item{\textbf{Write results chapter}}
    \item{\textbf{Write conclusions}}
    \item{\textbf{Polishing the report}: Here the language of the report will be improved, small errors will be fixed and details will be added}
    \item{\textbf{Prepare final presentation}}
    \item{\textbf{Find opponent}}
    \item{\textbf{Prepare opposition}}
\end{itemize}

\begin{figure}[H]
    \begin{ganttchart}[
            title/.style={fill=teal, draw=black},
            expand chart=\textwidth,
            vgrid = {*1{black, dotted}},
            bar top shift = 0.2,
            bar height = 0.7,
            y unit chart=0.5cm,
        ]{1}{20}
        \gantttitlelist{1,...,20}{1} \\
        \ganttbar{Basic CPP to HDL converter}{2}{5} \\
        \ganttbar{Basic DP hardware}{4}{6} \\
        \ganttmilestone{Planning done}{4} \\
        \ganttbar{Set up FPGA dev tools}{6}{6} \\
        \ganttbar{Forward search}{7}{7} \\
        \ganttbar{Integrate DP with model}{7}{8} \\
        \ganttbar{Write performance analysis tool}{7}{9} \\
        \ganttbar{Improve DP hardware}{8}{14} \\
        \ganttbar{Optimize HDL converter}{8}{14} \\
        \ganttbar{Analyse optimisation effects}{10}{15} \\
        \ganttmilestone{Half time review}{11}
        \ganttnewline[thick, gray]
        \ganttbar{Write introduction}{1}{5} \\
        \ganttbar{Write theory}{1}{8} \\
        \ganttbar{Write results}{12}{15} \\
        \ganttbar{Improve original chapters}{14}{16} \\
        \ganttmilestone{Report hand in}{16} \\
        \ganttbar{Write conclusions}{17}{17} \\
        \ganttbar{Write abstract}{17}{17} \\
        \ganttbar{Find opponent}{17}{17} \\
        \ganttbar{Polishing the report}{18}{19} \\
        \ganttbar{Prepare final presentation}{18}{19} \\
        \ganttbar{Prepare opposition}{19}{19} \\
        \ganttmilestone{Final presentation}{19} \\
        \ganttbar{Buffer time}{20}{20}
    \end{ganttchart}
    \caption{Gantt chart showing the estimated times at which different tasks will be worked on. The weeks at the top indicate the amount of weeks that have passed since work on the project began}
    \label{fig:gantt}
\end{figure}

\newcommand{\done}{\ganttbar[bar/.append style={fill=green}]}
\newcommand{\kindof}{\ganttbar[bar/.append style={fill=yellow}]}
\newcommand{\notdone}{\ganttbar[bar/.append style={fill=red}]}
\begin{figure}[H]
    \begin{ganttchart}[
            title/.style={fill=teal, draw=black},
            expand chart=\textwidth,
            vgrid = {*1{black, dotted}},
            bar top shift = 0.2,
            bar height = 0.7,
            y unit chart=0.5cm,
            today = 12,
        ]{1}{20}
        \gantttitlelist{1,...,20}{1} \\
        \done{Basic CPP to HDL converter}{2}{5} \\
        \done{Basic DP hardware}{4}{6} \\
        \ganttmilestone{Planning done}{4} \\
        \kindof{Set up FPGA dev tools}{6}{6} \\
        \done{Forward search}{7}{7} \\
        \kindof{Integrate DP with model}{7}{8} \\
        \done{Write performance analysis tool}{7}{9} \\
        \notdone{Improve DP hardware}{8}{14} \\
        \kindof{Optimize HDL converter}{8}{14} \\
        \kindof{Analyse optimisation effects}{10}{15} \\
        \ganttmilestone{Half time review}{11}
        \ganttnewline[thick, gray]
        \kindof{Write introduction}{1}{5} \\
        \kindof{Write theory}{1}{8} \\
        \notdone{Write results}{12}{15} \\
        \kindof{Improve original chapters}{14}{16} \\
        \ganttmilestone{Report hand in}{16} \\
        \notdone{Write conclusions}{17}{17} \\
        \notdone{Write abstract}{17}{17} \\
        \notdone{Find opponent}{17}{17} \\
        \notdone{Polishing the report}{18}{19} \\
        \notdone{Prepare final presentation}{18}{19} \\
        \notdone{Prepare opposition}{19}{19} \\
        \notdone{Final presentation}{19}{19} \\
        \ganttbar{Buffer time}{20}{20}
    \end{ganttchart}
    \caption{Gantt chart showing the estimated times at which different tasks will be worked on. The weeks at the top indicate the amount of weeks that have passed since work on the project began}
    \label{fig:gantt}
\end{figure}

\begin{figure}[H]
    \begin{ganttchart}[
            title/.style={fill=teal, draw=black},
            expand chart=\textwidth,
            vgrid = {*1{black, dotted}},
            bar top shift = 0.2,
            bar height = 0.7,
            y unit chart=0.5cm,
            today = 12,
        ]{13}{20}
        \gantttitlelist{13,...,20}{1} \\
        \ganttbar{Bit length selection}{13}{13} \\
        \ganttbar{Verilog output verification}{13}{14} \\
        \ganttbar{Support full model DP}{14}{15} \\
        \ganttbar{Support concurrent pipelines}{15}{16} \\
        \ganttbar{Analyse optimisation effects}{13}{17} \\
        \ganttnewline[thick, gray]
        \ganttbar{Write theory and method}{13}{14} \\
        \ganttbar{Write introduction}{13}{15} \\
        \ganttbar{Write results}{15}{17} \\
        \ganttbar{Improve original chapters}{15}{17} \\
        \ganttmilestone{Report hand in}{17} \\
        \ganttbar{Write conclusions}{18}{18} \\
        \ganttbar{Write abstract}{18}{18} \\
        \ganttbar{Find opponent}{18}{18} \\
        \ganttbar{Polishing the report}{19}{20} \\
        \ganttbar{Prepare final presentation}{19}{20} \\
        \ganttbar{Prepare opposition}{20}{20} \\
        \ganttbar{Final presentation}{20}{20} \\
    \end{ganttchart}
    \caption{Gantt chart showing the estimated times at which different tasks will be worked on. The weeks at the top indicate the amount of weeks that have passed since work on the project began}
    \label{fig:gantt}
\end{figure}


\section{Risks}

As with any project, things can and will go wrong and it is important to plan
for that in advance. Therefore, this section will present some possible risks
that could affect the project, how to mitigate them and what to do if they occur.

\subsection{Type based high level synthesis might not work as well as planned}

Finding references to other projects using the ideas described in
Section~\ref{sec:static_cpp_to_hdl} seems difficult which indicates that it is
most likely an uncommon strategy. This makes it more likely that I will run
into issues using it along the way which could complicate things.

In order to avoid this, an early testable implementation should be developed
quickly which should hopefully allow me to find any major issues while there is
still time to use a different method. If problems do arise, the best strategy
is probably to try and use some existing high level synthesis tool.


\subsection{Static bound analysis proves inefficient}

It might turn out that the variable bound analysis described in
Section~\ref{sub:static_analyssis} produces very pessimistic results because it
misses details that can only be seen at runtime. Like the previous point, the
best way to avoid this is to quickly develop a proof of concept that can
highlight any major issues.

Again, if issues do arise, the best course of action is to switch to a
different technique, in this case dynamic bound analysis. It might also be
possible to use an off the shelf \ac{HLS} tool here as well because they too must
have similar issues.

\subsection{Small dataset}

Currently, only one HEV model is available, and that model only has one sample
input. This means that issues that only arise in some models may remain
unnoticed. Additionally, the lack of a large set of sample input means that
performing dynamic varaible bound analysis is difficult, and it may also make
the model seem better than it actually is with fix point values because not all
values may be hit.

If more data is avaliable, that should mitigate most of these issues.
Additionally, if bound checking is done statically, the problem is also much
less severe and will only affect the quality of the result analysis, not the
implementation.



