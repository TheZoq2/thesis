\chapter{Method}\label{cha:method}

In this chapter, the C++ to \ac{HDL} converter is described, along with the methodology
used to evaluate the performance of the hardware.

The source code of the \ac{HDL} converter is available at
\url{https://gitlab.com/TheZoq2/thesis/tree/master/hdlconverter}

\section{Conversion into an expression graph}\label{sub:graph_conversion}

In order to simplify the process of converting C++ expressions into an \ac{HDL},
the expressions are first converted into an expression graph. In order to
achieve this, all variables and calculations in the expressions are replaced by
a custom type which represents a node in the tree. The type keeps track of
which operation is to be performed as well as which operands will be used for
the operation.  Listing~\ref{code:basic_node} shows an example of an abstract
class for such a node. The function \textit{operands} returns all the operands
which are inputs to this node.

\lstinputlisting[
    label={code:basic_node},
    caption={Example of an expression tree node},
    language={c++}
]{code/cpp/node.hpp}

This interface is then implemented for each operation that is used in the
model. Listing~\ref{code:basic_add_and_constant} shows an example of a node for
performing addition as well as a node which represents a constant.

\lstinputlisting[
    label={code:basic_add_and_constant},
    caption={Example of an addition and a constant node},
    language={c++}
]{code/cpp/simple_add_constant.hpp}

In order to make usage of this API more convenient, custom operators are also
defined for the Node class which allows them to be used as
drop-in-replacements for normal C++ expressions as shown in
Listing~\ref{code:operator_overloading}. Further, in order to simplify memory
management and the definition of the overloaded operators, all nodes are
wrapped in shared pointers.

\lstinputlisting[
    label={code:operator_overloading},
    caption={Example of definition and usage of the addition operator for nodes},
    language={c++}
]{code/cpp/operator_overloading.hpp}



\section{Converting nodes to \ac{HDL}}\label{sub:node_to_hdl}

In order to convert an expression from the expression graph representation into
a \ac{HDL}, two functions were added to the \textit{Node} interface:
\textit{variable\_name} and \textit{get\_code}. The former simply returns a
string which will be the name of the \ac{HDL} variable where the result of the
calculation associated with the node will be stored. The exact names returned
by this function are not important, they could be chosen at random or generated
based on the inputs to the node as long as they are always unique and constant
for each node.

The \textit{get\_code} function returns \ac{HDL} code which creates the
corresponding variable and gives it its value. Listing~\ref{code:get_code}
shows an example of such a function which generates verilog code for an
addition node. The size of the resulting register is left out for now but will
be discussed in Section~\ref{sec:word_length_selection}.

\lstinputlisting[
    label={code:get_code},
    caption={Function for generating \ac{HDL} code for an addition node},
    language={c++}
]{code/cpp/get_code.cpp}


\section{Pipelining}

As explained in Section~\ref{sub:pipelining_introduction}, pipelining can be
used to increase the throughput of calculated values without adding more
computation units but it does require adding extra delay registers when two operands
are not available simultaneously.

As was mentioned in Section~\ref{sec:theory:pipeline_depth}, the depth of a pipeline node
is 0 for all nodes which have no operands, and

\[
    d_n = \max_{i \in \text{operands}(n)}(d_i + t_i)
\]

when the depth is known. The computation time of node $i$ in clock cycles is
denoted by $t_i$. The required delay slots between a node and one of its
operands can be calculated as $d_n - d_i - 1$ where $d_n$ is the depth of the
node and $d_i$ is the depth of the operand.

The code in Listing~\ref{code:get_code} can then be modified to use the
pipeline registers as shown in Listing~\ref{code:get_code_pipeline}. Where the
\textit{register\_name(n)} returns the nth delay register of the node.

\lstinputlisting[
    label={code:get_code_pipeline},
    caption={Listing~\ref{code:get_code} modified to use pipeline registers},
    language={c++}
]{code/cpp/get_code_pipeline.cpp}



\subsubsection{Constants}

Some nodes, primarily constants do not need pipeline registers as their values
never change between iterations. For these nodes, the \textit{register\_name}
function just returns the default name, and the function that generates
pipeline code is overridden to return an empty string.


\section{Boolean values and if-expressions}

So far, the discussion has been centered around nodes with numerical values.
However, most programs also contain control logic which requires boolean values
and expressions. In order to accommodate boolean nodes, operations that are
specific to numerical nodes, such as \textit{calculate\_value} were moved into
a subclass of nodes, and boolean nodes were added as a separate subclass with
similar functionality.

Most algorithms require some choice based on a boolean value which is most
easily done through if-statements. However, converting a C++ if-statement to
\ac{HDL} code from inside C++ is difficult, if not impossible. Therefore, a
special node representing an \textit{if-expression} was introduced.

Like an if-statement, the behaviour of an if-expression depends on a condition, however,
unlike an if-statement which performs different computations based on the condition,
an if-expression selects between two values depending on the condition.

The if expression node has three operands: the condition, the node containing
the value to select if the condition is true and the node to select if the
value is false. This means that both the true and false branch are computed and
based on the value of the condition node, the true or false value is selected
as the value of the node.




\section{Special nodes}

Most nodes that were implemented in this project perform standard arithmetic
calculations and were mapped directly to the corresponding verilog version.
However, some nodes required extra thought and are documented in this section.

\subsection{Estimating square roots}

The square root of a fixed point value can not easily be calculated in hardware. Instead,
it was estimated using the method described in this section.

The square root estimation is based on the following equation:

\[
    \sqrt{x} = \sqrt{\frac{2^n x}{2^n}}
             = \sqrt{2^n} \sqrt{\frac{x}{2^n}}
             = 2^\frac{n}{2} \sqrt{\frac{x}{2^n}}
\]

If $\alpha$ is within a known bound, $\sqrt{\alpha}$ can be estimated using a
lookup table. This means that $\sqrt{\frac{x}{2^n}}$ can be approximated if $n$
is chosen such that $a \leq \frac{x}{2^n} \leq b$. This can be achieved by
selecting $n$ as $n=k-m$ where $k$ is the position of the most significant $1$
in x and where $m$ is the amount of fractional bits in $x$. This bounds the
value of $\frac{x}{2^n}$ between 0 and 1.

If $n$ is an integer, $\frac{x}{2^n}$ can easily be computed by shifting $x$
$n$ steps to the right. Similarly, if $n$ is an even integer, $2^{\frac{n}{2}}
\cdot a$ can easily be computed by shifting $a$ $\frac{n}{2}$ steps to the
left. However, this does not work when n is odd. This in turn can be fixed by
using $m=2n$ as follows:

\[
    \sqrt{x} = 2^\frac{m}{2} \sqrt{\frac{x}{2^m}}
\]

which bounds the value if $\frac{x}{2^n}$ between 0 and 2 and ensures that $m$ is
always even.

This can efficiently be computed in hardware by calculating
$\frac{x}{2^{m}}$ using bit shifts, using the result to look up the square
root, and then shifting the result back $\frac{m}{2}$ steps.


\subsection{Generating lookup tables}

Another common expression which needed extra thought was lookup tables. Lookup
tables currently only support integer inputs which are directly mapped to an
output fractional value. They were implemented as individual verilog modules in
order to make re-use of their values in multiple places easier.

Lookup tables were implemented as two separate classes, one which contains the
value mapping and is used to generate the verilog module. The second class
is a node subclass which implementation is shown in Listing~\ref{code:lut_result}.

\lstinputlisting[
    label={code:lut_result},
    caption={Lookup table node class},
    language={c++}
]{code/cpp/lut_result.cpp}


The lookup table module itself consists of a register for storing the result,
along with a large case statement which assigns the correct value to that result.
Initially, this case statement contained the full fractional values with a default
branch to catch any non-integer values. However, this caused the synthesis tool to
generate multiplexers instead of block RAMs. To avoid this problem, the fractional
bits are truncated before being passed to the case statement.




\section{Generating \ac{HDL} modules}\label{sec:module_generation}

The discussion so far has been focused on generating code for individual
computations, but in order to output useful \ac{HDL} code, a few more things
have to happen. The delay registers for each node have to be added, code for
each node has to be generated exactly once, and the inputs and outputs for the
module have to be
specified.

\subsection{Adding pipeline registers}

Each node can be used multiple times, and the amount of pipeline registers to
add depends on the nodes for which that node is an operand. In order to determine
the amount of required pipeline registers for each node, each node computes the
amount of delay required for each of its operands. The required delay is passed
to the operand node which stores the largest amount of delay it receives. When
generating code, that amount of pipeline registers is generated.

\subsection{Only generating each node once}

The code for each node should only be inserted into the resulting module once,
but since each node can be the operand of multiple other nodes, simply traversing the
graph and inserting the code for each node will not work. Instead, the graph is
traversed once in a depth first manner and each node gets added to a vector if
it is not already in there. Once the vector has been created, the code for each
node is generated and inserted in the order that it appears in the vector.

\subsection{Specifying inputs and outputs}

In order to generate a complete and working verilog module, the inputs and
outputs to that model must be specified. The model which was used has several
parallel outputs for both cost and new states, so multiple outputs must be
supported. Luckily, the output nodes are the root nodes of their expression
trees which means that returning them suffices to output all required
information. Then, the code which generates the model can simply go through
all the output nodes and mark them as outputs.

Inputs were slightly more tricky as they are embedded in the expression tree as
leaf nodes along with constants which makes it non-trivial to find them. They
could have been specified as a list to the function which generates the code,
but that would have required extra effort for the user. Instead, the tree is
traversed, and all nodes with the input class are added to the module header.








\section{Word length selection}\label{sec:word_length_selection}

The value computed in each node must be stored in registers and propagated
through the circuit, and because most values in the model are fractional, this
could not be done at full precision. Instead, the word length of each value
must be selected to produce results that are good enough while not using too
much hardware.

All values were stored using a fix point representation since floating point
values use much more resources. A fix point number consists of an integer part
and a fractional part where the fractional part influences the precision of the
stored number while the integer part controls the minimum and maximum value
that can be stored.

In this project, some attempts were made to optimize the size of the fractional
part to achieve results with a specified precision. However, those attempts
were unsuccessful and will be discussed in
Section~\ref{sec:fractional_word_length_optimisation}. Instead of optimizing the
fractional word length individually, the same amount of fractional bits was
used for all values in the model, and the amount of fractional bits required
was determined by simulation.

Unlike the fractional word length, the integer word length was determined per
variable in order to guarantee that no overflow would occur while keeping
resource usage low. This was done by calculating the minimum and maximum value
of each variable and selecting the smallest integer word length that would fit
all those values.

Like many other computations done on the expression graph, the minimum and
maximum value in each node can be calculated recursively. To do so, the bounds
on values without operands must first be known beforehand. The bounds on
constants are trivial to determine, it is simply the constant value, while the
bounds on input variables must be specified by a designer.

Then, the bounds on other nodes can be determined recursively from the
operation performed in the node and the bounds on the operands. For example,
the maximum value of an addition node is the sum of the maximum values of the
operands.

This works well for most basic operations, but in some cases, it gives a very
pessimistic estimate which can then propagate further through the design. To
see why this is a problem, consider the code snippet shown in
Listing~\ref{code:pessimistic_minimum}.

\lstinputlisting[
    label={code:pessimistic_minimum},
    caption={Example of an expression tree node},
    language={c++}
]{code/cpp/pessimistic_minimum.cpp}

In this snippet, a large value called \textit{some\_value} is capped from above
and below before being used to calculate \textit{result1} and \textit{result2}. It is
clear when reading the code that the capped value will be bounded between $0.3$
and $1$, but there is no general way to determine the effect on variable bounds
of an arbitrary if-statement. Therefore, the program will still think that the capped
value is between $0$ and $100$.

This leads to $result1$ being stored with way more bytes than is required which
is inefficient. However, it leads to much worse problems when calculating the
bounds on \textit{result2} where the calculation of the maximum value will
attempt to divide by zero.

In order to solve this, an extra function was added to if-statements which
allows a programmer to manually override the bounds on the node. While this
works, it does introduce potential overflow errors if a programmer isn't
careful when specifying the bounds. In order to avoid that problem, additional
functions were added for common operations which use if-statements to cap
variables. These functions return if-nodes with the bounds already applied.












\section{Common patterns}

While the \ac{HDL} converter can convert basic C++ code to verilog, some
programming constructs can not currently be directly converted and must be
worked around manually. This section presents examples of this, along with
workarounds for those problems.


\subsection{Early returns}

One very common pattern in the example model is one where a value is calculated
and compared with some minimum or maximum value. If the calculated value exceeds
the bounds, the specified input is infeasible and model evaluation is aborted. In the
original code, this was implemented using early returns, as shown below.

\lstinputlisting[language={c++}]{code/cpp/early_return_example.cpp}

In the \ac{HDL} converter, this leads to a few problems. First, the C++
function has to return the whole expression containing all branches which means
that an early return is not possible. Second, an early return would not be
easily implemented in a pipeline. In order to work around that, a boolean flag
called failed was added. Each time an early return would have happen in C++,
that flag is updated to hold \textit{or(old\_value, condition)}. If this flag
is true at the end of the computation, the calculated values can be discarded.

However, the early returns also often prevent future calculations involving out
of bounds values, which with this system propagate through to those
calculations. This problem is similar to the out of bounds values that occur
when if-statements are used to cap values. However, in this case, since there
is no early return node, the programmer manually has to insert additional code
to cap the values.


\subsection{Interpolation}

As mentioned in Section~\ref{sec:vehicle_model}, the model contains several
instances of functions being approximated by interpolation of values from a
lookup table. The interpolation consists of regular arithmetic operations with
some array lookups. As all those operations were already implemented in the HDL
converter and the interpolation functions were already written in C++, a
special interpolation node is not used.


\section{Simulating the expression tree}\label{sec:tree_simulation}

In order to enable testing of various things like bit lengths of variables, and
to ease debugging of the generated expression graph, some functions were added
to execute the expression graph on a normal CPU.

In order to do so, a function to calculate the value of a node was added, this function
recursively calls itself on all the operands before performing some local
computation based on the operand values. An example of such a function for addition
nodes is shown in Listing~\ref{code:calculate_value} where
\textit{ExecutionType} is the type used to represent values. When verifying the
translation of a model into an expression tree, this can be set to the same
type as what is used in the original model. On the other hand, when evaluating
various data types for hardware implementation, it can be changed to whichever
type is being evaluated.


\lstinputlisting[
    label={code:calculate_value},
    caption={Example of the \textit{calculate\_value} for an addition node},
    language={c++}
]{code/cpp/calculate_value.cpp}


When simulating using fix point values, the \textit{FpF} class from the
\textit{MFixedPoint}\footnote{\url{https://github.com/gbmhunter/MFixedPoint}}
library was used with 128 bit integers as the base value.

\subsection{Performance issues and caching}

The above methods work well for small expressions, but as the size of the
expression grows, a lot of nodes will be recomputed multiple times which in the
case of the sample model lead to infeasible computation times. To see why,
consider the graph shown in Figure~\ref{fig:expression_recomputation_example}.


\begin{figure}[h]
    \centering
    \begin{tikzpicture}
        [scale=1,auto=left,every node/.style={circle,draw=black}]
        \node (na) at (3,10) {$a$};
        \node (nb1) at (1,8) {$b_1$};
        \node (nb2) at (3,8) {$b_2$};
        \node (nb3) at (5,8) {$b_3$};
        \node (nc) at (3,6) {$c$};

        \foreach \from/\to in {na/nb1, na/nb2, na/nb3, nb1/nc, nb2/nc, nb3/nc}
          \draw[->] (\from) -- (\to);
    \end{tikzpicture}
    \caption{
        Example of expression graph where one node is recomputed multiple times
        when calculating node values recursively
    }\label{fig:expression_recomputation_example}
\end{figure}

Here, the value of node $a$ will be computed once when calculating $b_1$,
once when calculating $b_2$ and once when calculating $b_3$. For larger graphs
with more branches, this leads to a lot of recomputations. The sample vehicle
model has roughly 500 unique nodes but in total, those nodes get visited over
$100\ 000$ times when traversing the tree recursively.

In order to solve this problem, the \textit{calculate\_value} function was
extended with a cache. If the value of this node has been calculated before,
the old value is returned, otherwise it is recomputed. Additionally, a function
to clear the cache was added. The implementation can be seen in
Listing~\ref{code:value_cache}

\lstinputlisting[
    label={code:value_cache},
    caption={Node class augmented with a cache for runtime values},
    language={c++}
]{code/cpp/value_cache.cpp}

The downside to this approach is that the cache has to be cleared when input
values change which itself requires traversal of the whole tree. The solution
to this was to flatten the expression graph after it is generated by
recursively traversing it and inserting all unique nodes into a set data
structure. This set can then be iterated over in order to reset the cache for
each node.

\subsection{Debugging and debugging tools}

A result of the described structure when simulating expression execution is
that the execution does not happen in the source code of the function being
executed, it happens when the \textit{calculate\_value} function is executed.
This makes debugging difficult as print statements or debugger breakpoints
can't be placed between calculations, only inside the \textit{calculate\_value}
function for a specific node class.

In order to aid debugging, a method to output the expression
graph of all the ancestors of a node was added. This method outputs the name of
a node, along with values from the node in a
graphwiz\footnote{\url{https://graphviz.org/}} graph.




\section{Performance evaluation}\label{sec:performance_evaluation}

In this section, the methods used for evaluating the performance of the
\ac{HDL} converter will be presented. 

When the output quality is determined, the original CPU version of the model
using double precision floating point values is used as ground truth.

\subsection{Synthesis}

In order to evaluate the maximum clock frequency of the design, as well as the
amount of FPGA resources it uses, the model was synthesised using tools
included in Xilinx ISE 14.7 which were executed using a makefile written by
\textit{GitHub} user \textit{duskwuff}
\footnote{\url{https://github.com/duskwuff/Xilinx-ISE-Makefile}}.  The target
hardware was set to xc6vlx760-FF1760. The hardware usage was recorded after the
synthesis step while the maximum frequency was recorded after the place and
route step.

The bulk of FPGA hardware consists of a lookup table paired with a flip-flop
which makes the number of such pairs used by a design a useful measure of FPGA
resource usage. Additionally, when large chunks of data is to be stored,
discrete block RAMs are used. These are not constructed from LUT-flip-flops
which means that the amount of RAMs used is reported separately.

\subsection{Output quality}

As all values are represented as fixpoint numbers with the same amount of
fractional bits, the quality of the output depends on the amount of bits used.
The simulation process described in Section~\ref{sec:tree_simulation} was used
to determine the quality as a function of the number of fractional bits.

The simulation was run on two different road profiles which both contain a mix
of urban and highway driving. The first is the same profile as was used by Olin
et.al.\ in~\cite{road_profile_paper}. The second profile was generated for this
project. For convenience, they will be referred to as the ``American'' and
``Swedish'' road profile because of the speed limits used.

In order to determine where to start searching for the required amount of
fractional bits, the original model was first executed using double precision
floats with truncation to a certain amount of fractional bits at the output.
